# MOB eFuse Application Software

## Software States

The software shall implement four different states:
- **Booting**: Performs a POST, checks Flash memory correctness and loads the application.
- **Ambient Monitoring**: Cyclically requests values from the environmental sensors.
- **eFuse Experiment**: Performs a single verification run of the experiment.
- **Safe Mode**: Keeos the system in a safe, incative state and waits for ground intervention.

The application keeps the CAN interface towards the host platform active in all states except **Booting**, so communication between the platform controller and the system is possible.

## Software ICD

The application will define the following telecommand and telemetry messages. All statements in this section are suggestions and need to be finalized during the Design Phase. The final implementation will take the to be provided protocol used on the CAN bus into account. SeeSat and Ororatech need to define an operational concept, too, before the software ICD will be finalized.

### Telecommands (TC)

- Reset
- Set Time (time) 
- Configure (config: [key, value])
- Start eFuse Experiment
- Enable/Disable Telemetry (packet: {hk, ambient}, interval)
- Dump Ambient Data (id: {0,1,2})
- Dump Experiment Data (id: {0,1,2})
- Clear Ambient Data (id: {0,1,2}) 
- Clear Experiment Data (id: {0,1,2})

### Telemetry (TM)

- HK report
- Ambient report
- Ambient Data dump
- Experiment Data dump

The _reports_ will be transmitted sporadically, while the _dumps_ will be one time downloads of the specific data block.

## Data Memory Estimation

The following subsections estimate the memory requirements for the housekeeping data pool, the ambient monitoring data pool and the experiment result data pool. Each position lists an identifier, the bit length and a shot description of the content.

### Housekeeping Data

- `v_in` [16] (externally measured input voltage)
- `temp_uc` [16] (internally measured MCU temperature)
- `v_ref_int` [16] (internally measured MCU reference voltage)
- `ts_boot` [64] (timestamp of system start in CDS format; computed after "Set Time" and otherwise 0)
- `tid_last` [16] (last TID value read from dosimeter)
- `temp_1_last` [16] (last value read from temperature sensor 1)
- `temp_2_last` [16] (last value read from temperature sensor 2)
- `dut_1_fault` [1] (flag indicating whether or not DUT 1 is in fault mode)
- `dut_2_fault` [1] (flag indicating whether or not DUT 2 is in fault mode)
- `dut_3_fault` [1] (flag indicating whether or not DUT 3  is in fault mode)
- `spare` [13] (spare bits to align memory to complete bytes)

The housekeeping data block has a complete size of **22 bytes**.

### Ambient Moitoring Data

- `ts` [64] (timestamp of measurement in CDS format)
- `tid` [16] (TID value read from dosimeter)
- `temp_1` [16] (value read from temperature sensor 1)
- `temp_2` [16] (value read from temperature sensor 2)

The ambient monitoring data block has a complete size of **14 bytes** per measurement. Assuming an ambient measurement every 16 seconds and an orbit duration of about 90 minutes (5400 secons), the ambitent monitoring results in 338 measurements that will consume **3192 bytes** of data per orbit.

### Experiment Result Data

- `ts_start` [64] (timestamp of experiment start in CDS format; will be collected only once per experiment run)
- `t_offset` [12] (time offset from experiment start in ms; collected per test cycle)
- `test_select` [2] (selected test; can be "_OVP_", "_UVP_", "_OCP 3.3V_" or "_OCP 5V_")
- `test_cnt` [4] (test counter; each test will be repeated 10 times)
- `dut_v_in` [16] (input voltage to DUT)
- `dut_select` [2] (selected DUT; can be "_SOURCE LCL_", "_TPS25947_" or "_NIS5150_")
- `dut_i_sense` [16] (current measured by selected DUT)
- `dut_v_out` [16] (output voltage out of DUT)
- `dut_fault` [1] (fault state of selected DUT)
- `load_select` [2] (selected load; can be "_R1_", "_R2_", "_R3_" or "_R4_")
- `load_i_sense` [16] (current measured after load)
- `t_trip` [10] (tripping time of fuse in ms)
- `spare` [7] (spare bits to align memory to complete bytes)

The experiemnt result data block has a complete size of **13 bytes** per test cycle (exculding the timestamp). A complete experiment run consists of 10 tests with 10 repetitions each for all three DUTs resulting in 300 test cycles per run. Therefore, the required memory size for keeping the all results for a experiemnt run is **3908 bytes** (3900 bytes result data and 8 bytes timestamp).

### Buffering of gathered data

The system will store the ambient monitoring data of approx. three orbits. This means **14196 bytes** (or 14.2 KB) are requied to store the corresponding data.

The system will store the experiment result data of three runs. This means **11724 bytes** (or 11.7 KB) are requied to store the corresponding data.
