# Chanelog

## EGSEv1.0 - 2024-02-29
- Initial release


## PCBv2.1 - 2024-01-24
### Changed
- Replace RADFET instrumentation amplifier with precision amplifier INA190A5

### Fixed
- Fix MP3630 footprint
- Fix misc. footprints


## PCBv2.0 - 2024-01-08
### Changed
- Add VDD_ISO power converter
- Add earth GND
- Cleanup testpoints
- Change VDD to 3V3
- Assign PCB border and mounting hole pads to earth
- Add large or sensitive components to backside of PCB
- Use larger SMD components

### Fixed
- Fix PMN30XPA p-channel MOSFET connections
- Fix LM134H current source footprint pin assignment


## PCBv1.0 - 2023-11-16
- Initial release