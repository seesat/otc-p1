/**
 * @file adc.c
 *
 * @brief Control functions for Analog-Digial-Converter
 *
 * @author Stefan Wertheimer
 * @date 2024-06-14
 */

#ifndef INC_ADC_H_
#define INC_ADC_H_

#include "stdint.h"

#include "stm32g4xx_hal.h"

#include "error_codes.h"

#include "main.h"

// Defines to enable/disable ADCs by name
#define ADC_DUT1_I_SENSE ADC_1
#define ADC_DUT2_I_SENSE ADC_1
#define ADC_DUT3_I_SENSE ADC_1
#define ADC_RAD_I_SENSE ADC_1
#define ADC_LS_I_SENSE ADC_1
#define ADC_VCC_SENSE ADC_2
#define ADC_DUT_V_OUT_SENSE ADC_2
#define ADC_RAD_OUT_SENSE ADC_2

#define ADC_DUT1_I_SENSE_CHANNEL ADC_CHANNEL_1
#define ADC_DUT2_I_SENSE_CHANNEL ADC_CHANNEL_2
#define ADC_DUT3_I_SENSE_CHANNEL ADC_CHANNEL_3
#define ADC_DUT_V_OUT_SENSE_CHANNEL ADC_CHANNEL_12
#define ADC_RAD_I_SENSE_CHANNEL ADC_CHANNEL_15
#define ADC_RAD_OUT_SENSE_CHANNEL ADC_CHANNEL_15
#define ADC_VCC_SENSE_CHANNEL ADC_CHANNEL_17

// Configured/used channels of ADC1
#define ADC_1_CHANNELS 6

// Configured/used channels of ADC2
#define ADC_2_CHANNELS 3

// Offset of ADC2 in parameter enumeration
#define OFFSET_ADC_2 6

/**
 * @brief Enumerations for used ADCs.
 */
typedef enum adc {
	ADC_1,
	ADC_2
} adc_t;

/**
 * @brief Enumeration of ADC parameters.
 */
typedef enum adc_param {
	// ADC1
	DUT1_I_SENSE = 0,
	DUT2_I_SENSE = 1,
	DUT3_I_SENSE = 2,
	LS_I_SENSE   = 3,
	RAD_I_SENSE  = 4,
	VREF         = 5,
	// ADC2
	DUT_V_OUT_SENSE = 6,  // 0 for ADC2
	RAD_OUT_SENSE   = 7,  // 1 for ADC2
	VCC_SENSE       = 8   // 2 for ADC2
} adc_param_t;

/**
 * @brief Initialize ADC control.
 */
void adc_init(ADC_HandleTypeDef adc1, ADC_HandleTypeDef adc2);

/**
 * @brief Initialize comparator for DUT1_I_SENSE
 */
void comp3_init();

/**
 * @brief Start comparator for DUT1_I_SENSE
 */
void comp3_start();

/**
 * @brief Initialize selected ADC for slow but accurate conversion of all defined channels
 * @param  adc  ADC to configure
 * @return OK, PARAMETER_ERROR or ADC_INIT_ERROR
 */
error_code_t adc_init_all_slow(adc_t adc);

/**
 * @brief Initialize selected ADC for slow but accurate conversion of one given channel
 * @param  adc  ADC to configure
 * @param  adc_channel the adc channel to use, only pass a HAL defined channel like ADC_CHANNEL_1
 * @return OK, PARAMETER_ERROR or ADC_INIT_ERROR
 */
error_code_t adc_init_one_slow(adc_t adc_num, uint32_t adc_channel);

/**
 * @brief Initialize selected ADC for fast conversion of one given channel
 * @param  adc  ADC to configure
 * @param  adc_channel the adc channel to use, only pass a HAL defined channel like ADC_CHANNEL_1
 * @return OK, PARAMETER_ERROR or ADC_INIT_ERROR
 */
error_code_t adc_init_one_fast(adc_t adc_num, uint32_t adc_channel);

/**
 * @brief Start read out through DMA for given ADC.
 *
 * @param  adc  ADC to start DMA read out
 * @return OK, PARAMETER_ERROR or ADC_INIT_ERROR
 */
error_code_t adc_start_dma(adc_t adc);

/**
 * @brief Start single channel read out through DMA for given ADC.
 *
 * @param  adc  ADC to start DMA read out
 * @return OK, PARAMETER_ERROR or ADC_INIT_ERROR
 */
error_code_t adc_start_single_dma(adc_t adc);

/**
 * @brief Stop read out through DMA for given ADC.
 *
 * @param  adc  ADC to stop DMA read out
 * @return OK or PARAMETER_ERROR
 */
error_code_t adc_stop_dma(adc_t adc);

/**
 * @brief Get value of given ADC parameter read by DMA.
 *
 * @param  param  ADC parameter name
 * @return Value of the parameter
 */
uint32_t adc_get_value_dma(adc_param_t param);

/**
 * @brief Get last value measured by given ADC read by DMA.
 *
 * @param  param  adc
 * @return Value of the parameter
 */
uint32_t adc_get_single_value_dma(adc_t adc);

/**
 * @brief Get current reference voltage of ADC.
 *
 * @return Current reference voltage of ADC
 */
uint32_t adc_get_vref();

/**
 * @brief Calibrate ADC.
 *
 * @param  adc  ADC to calibrate
 * @return OK or PARAMETER_ERROR
 */
error_code_t adc_calibrate(adc_t adc);

#endif /* INC_ADC_H_ */
