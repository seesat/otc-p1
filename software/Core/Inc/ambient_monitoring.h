/**
 * @file ambient_monitoring.h
 *
 * @brief Ambient monitoring (radiation & temperature)
 *
 * @author Stefan Wertheimer
 * @date 2023-12-24
 */

#ifndef INC_AMBIENT_MONITORING_H_
#define INC_AMBIENT_MONITORING_H_

#include "stdbool.h"
#include "stdint.h"
#include "stdlib.h"

#include "main.h"

#include "adc.h"
#include "circular_buffer.h"
#include "config.h"
#include "messages.h"
#include "timer.h"
#include "tm.h"
#include "tmp75bq1_config.h"
#include "mp28167.h"

/**
 * Orbit number.
 */
typedef enum orbit_no {
	ORBIT_LAST = 1, //< Last orbit
	ORBIT_PREV = 2, //< Previous orbit (before last)
	ORBIT_PENU = 3  //< Penultimate orbit (before previous)
} orbit_no_t;

/**
 * @brief Enumeration of sensor IDs and corresponding I2C addresses.
 */
enum sensor {
	sensor1 = (0x48 << 1), // 0b1001000
	sensor2 = (0x49 << 1)  // 0b1001001
};

/** @brief Flag indicating whether or not ambient monitoring data shall be distributes as TM report */
static volatile _Bool ambient_monitoring_tm_enabled;


/**
 * @brief Initialize ambient monitoring.
 *
 * @param  hi2c  Handle to I2C bus to access sensors
 */
void ambient_monitoring_init(I2C_HandleTypeDef hi2c, mp28167* ssc);

/**
 * @brief Free (delete) storage memory of ambient monitoring.
 */
void ambient_monitoring_free();

/**
 * @brief Read current ambient monitoring data from sensors.
 */
void ambient_monitoring_read();

/**
 * @brief Get all data stored in circular buffer for given orbit and send TM packets.
 *
 * @param  orbit  Orbit to get data for
 */
void ambient_monitoring_get_all_data(orbit_no_t orbit);

/**
 * @brief Get last recorded ambient monitoring data.
 *
 * @return Last ambient monitoring data recorded
 */
am_data_t ambient_monitoring_get_last_data();

#endif /* INC_AMBIENT_MONITORING_H_ */
