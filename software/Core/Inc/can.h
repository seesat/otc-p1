/**
 * @file can.h
 *
 * @brief Everything regarding CAN communication
 *
 * @author Stefan Wertheimer
 * @date 2024-01-23
 */

#ifndef INC_CAN_H_
#define INC_CAN_H_

// required to use auto-generated serializer with asserting
#define NUNAVUT_ASSERT(x) assert(x)

#include "stdint.h"

#include "stm32g4xx_hal.h"

#include "canard.h"
#include "o1heap.h"

#include "uavcan/node/ExecuteCommand_1_3.h"
#include "uavcan/node/Heartbeat_1_0.h"
#include "uavcan/primitive/array/Real64_1_0.h"

#include "ororatech/otc-p1/UTCTimeAtLastPPS_0_1.h"

#include "constants.h"
#include "error_codes.h"
#include "tc.h"
#include "timer.h"

/** Representation of a CAN FD frame. */
typedef struct canfd_frame {
	uint32_t can_id;  //< 32 bit CAN_ID including EFF/RTR/ERR flags
	uint8_t  len;     //< Frame payload length in byte
	uint8_t  flags;   //< Additional flags for CAN FD
	uint8_t  __res0;  //< Reserved (padding)
	uint8_t  __res1;  //< Reserved (padding)
	uint8_t  data[CANFD_MAX_DATA_LENGTH] __attribute__((aligned(8))); //< Frame payload data
} canfd_frame_t;

/**
 * @brief Initialize and configure CAN communication stack including hardware and OpenCyphal.
 *
 * @param  hfdcan  Handle to FDCAN bus
 *
 * @return OK on success, CAN_CONIFG_ERROR or CAN_HEAP_INIT_ERROR otherwise
 */
enum error_code can_init(FDCAN_HandleTypeDef* hfdcan);

/**
 * @brief Interrupt handler for CAN RX requests.
 *
 * @param  header  CAN header received from bus
 * @param  data    CAN data received from bus
 * @param  bytes   Bytes received
 *
 * @return OK on success, CAN_INVALID_TRANSFER_FRAME or CAN_DESERIALIZATION_ERROR otherwise
 */
enum error_code can_receive(FDCAN_RxHeaderTypeDef header, uint8_t* const data, const size_t bytes);

/**
 * @brief Send a Cyphal transfer object via CAN.
 *
 * @param  buffer       Pointer to buffer holding Cyphal transfer object
 * @param  buffer_size  Size of the buffer
 *
 * @return OK on success.
 */
enum error_code can_send(uint8_t* const buffer, size_t buffer_size, CanardPortID port_id);

void can_transmit(void);

void can_rx_subscribe();

#endif /* INC_CAN_H_ */
