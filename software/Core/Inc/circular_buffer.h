/**
 * @file circular_buffer.h
 *
 * @brief Circular buffer for data storage.
 *
 * @author Stefan Wertheimer
 * @date 2023-12-22
 */

#ifndef INC_CIRCULAR_BUFFER_H_
#define INC_CIRCULAR_BUFFER_H_

#include "stdbool.h"
#include "stddef.h"
#include "stdint.h"
#include "stdlib.h"
#include "string.h"

/** Data type for circular buffer */
typedef struct circular_buffer circular_buffer_t;

/** Handle type for circular buffer */
typedef circular_buffer_t* cb_handle_t;

/**
 * Initialize circular buffer structure for data storage.
 *
 * @param  buffer  Data buffer
 * @param  size    Size of the circular buffer
 * @return Handle to the created circular buffer
 */
cb_handle_t circular_buffer_init(uint8_t* buffer, size_t size);

/**
 * Free circular buffer structure.
 * Dose not free data buffer itself; owner is responsible for that.
 *
 * @param  me  Handle of buffer to free
 */
void circular_buffer_free(cb_handle_t me);

/**
 * Reset circular buffer to empty (head == tail).
 *
 * @param  me  Handle of buffer to free
 */
void circular_buffer_reset(cb_handle_t me);

/**
 * Add data to circular buffer.
 * Since it is a circular buffer, new data will overwrite oldest data if buffer is full.
 *
 * @param  me    Handle of buffer
 * @param  data  Data to add
 * @param  size  Size of data to add
 */
void circular_buffer_put(cb_handle_t me, const void * data, size_t size);

/**
 * Retrieve data from circular buffer.
 *
 * @param  me    Handle of buffer
 * @param  data  Retrieved data [out]
 * @param  size  Size of data to retrieve
 * @return True in case of success, false if buffer is empty.
 */
bool circular_buffer_get(cb_handle_t me, void * data, size_t size);

/**
 * Retrieve data from circular buffer without manipulating pointers.
 *
 * @param  me    Handle of buffer
 * @param  data  Retrieved data [out]
 * @param  size  Size of data to retrieve
 * @return True in case of success, false if buffer is empty.
 */
bool circular_buffer_peek(cb_handle_t me, void * data, size_t size);

/**
 * Retrieve data from circular buffer at a specific index without manipulating pointers.
 *
 * @param  me    Handle of buffer
 * @param  data  Retrieved data [out]
 * @param  size  Size of data to retrieve
 * @param  at    Index to start peeking at
 * @return True in case of success, false if buffer is empty.
 */
bool circular_buffer_peek_at(cb_handle_t me, void * data, size_t size, size_t at);

/**
 * Check if circular buffer is empty.
 *
 * @param  me   Handle of buffer
 * @return True if buffer is empty, false otherwise.
 */
bool circular_buffer_empty(cb_handle_t me);

/**
 * Check if circular buffer is full.
 *
 * @param  me   Handle of buffer
 * @return True if buffer is full, false otherwise.
 */
bool circular_buffer_full(cb_handle_t me);

/**
 * Get maximum capacity of circular buffer.
 *
 * @param  me   Handle of buffer
 * @return Maximum capacity of circular buffer
 */
size_t circular_buffer_capacity(cb_handle_t me);

/**
 * Get current number of elements in circular buffer.
 *
 * @param  me   Handle of buffer
 * @return Current number of elements in circular buffer
 */
size_t circular_buffer_size(cb_handle_t me);

/**
 * Get current position of head pointer in circular buffer.
 *
 * @param  me   Handle of buffer
 * @return Current position of head pointer  in circular buffer
 */
size_t circular_buffer_head(cb_handle_t me);

/**
 * Get current position of tail pointer in circular buffer.
 *
 * @param  me   Handle of buffer
 * @return Current position of tail pointer  in circular buffer
 */
size_t circular_buffer_tail(cb_handle_t me);

#endif /* INC_CIRCULAR_BUFFER_H_ */
