/**
 * @file config.h
 *
 * @brief Management of runtime configuration
 *
 * @author Stefan Wertheimer
 * @date 2024-01-20
 */

#ifndef INC_CONFIG_H_
#define INC_CONFIG_H_

#include "stdint.h"
#include "stdlib.h"

#include "config_ids.h"
#include "messages.h"
#include "timer.h"
#include "tm.h"

/** Flag indicating whether main loop shall be skipped or not */
static volatile _Bool config_skip_main_loop;

/**
 * @brief Initialize configuration parameter management and load defaults.
 */
void config_init();

/**
 * @brief Delete configuration parameter management.
 */
void config_delete();

/**
 * @brief Set a configuration parameter to the given value.
 *
 * @info Configuration parameters all are uint32.
 * @param   parameter   ID of the parameter (see constants)
 * @param   value       New value to set
 */
void config_set_value(config_id_t parameter, uint32_t value);

/**
 * @brief Get the value of a configuration parameter.
 *
 * @info Configuration parameters all are uint32.
 * @param   parameter   ID of the parameter (see constants)
 * @return Current value of the configuration parameter
 */
uint32_t config_get_value(config_id_t parameter);

/**
 * @brief Create a configuration parameter telemetry report.
 */
void config_create_parameter_report(void);

#endif /* INC_CONFIG_H_ */
