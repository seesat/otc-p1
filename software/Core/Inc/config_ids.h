/**
 * @file config_ids.h
 *
 * @brief Definition of configuration parameter identifiers
 *
 * @author Stefan Wertheimer
 * @date 2024-05-01
 */

#ifndef INC_CONFIG_IDS_H_
#define INC_CONFIG_IDS_H_

/** Identifier for all SeeSat MOBeFuse configuration parameter identifiers */
#define MOBEFUSE_CONFIG_ID 0x5300

/**
 * @brief List of all configuration parameter identifiers.
 */
typedef enum config_id {
	CONFIG_ID_EXP_INTERVAL            = MOBEFUSE_CONFIG_ID | 0x0001, //< Sampling interval for experiment data (in seconds)
	CONFIG_ID_AM_INTERVAL             = MOBEFUSE_CONFIG_ID | 0x0002, //< Sampling interval for ambient monitoring data (in seconds)
	CONFIG_ID_HK_INTERVAL             = MOBEFUSE_CONFIG_ID | 0x0003, //< Sampling interval for housekeeping data (in seconds)

	CONFIG_ID_ORBIT_DURATION          = MOBEFUSE_CONFIG_ID | 0x0004, //< Orbit duration (in seconds)

	CONFIG_ID_NUMBER_OF_ORBITS_FOR_AM = MOBEFUSE_CONFIG_ID | 0x0005, //< Number of ambient measurements per orbit
	CONFIG_ID_NUMBER_OF_EXP_RUNS      = MOBEFUSE_CONFIG_ID | 0x0006, //< Number of storable complete experiment runs
	CONFIG_ID_NUMBER_OF_SINGLE_EXP    = MOBEFUSE_CONFIG_ID | 0x0007, //< Number of storable single experiments
	CONFIG_ID_NUMBER_OF_EXP_SUITES    = MOBEFUSE_CONFIG_ID | 0x0008, //< Number of storable experiment suites

	CONFIG_ID_EXP_RUN_LENGTH          = MOBEFUSE_CONFIG_ID | 0x0009, //< Length of experiment runs
	CONFIG_ID_EXP_ENABLE              = MOBEFUSE_CONFIG_ID | 0x000A, //< Bit mask of enabled experiments
	CONFIG_ID_NUMBER_OF_EXP_ENABLED   = MOBEFUSE_CONFIG_ID | 0x000B, //< Number of enabled experiments

	CONFIG_ID_NUM = 11
} config_id_t;

#endif /* INC_CONFIG_IDS_H_ */
