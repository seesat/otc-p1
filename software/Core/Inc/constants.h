/**
 * @file constants.h
 *
 * @brief Definition of constants used in MOBeFuse.
 *
 * @author Stefan Wertheimer
 * @date 2023-12-22
 */

#ifndef INC_CONSTANTS_H_
#define INC_CONSTANTS_H_

// Standard definitions to avoid magic numbers
#define KILO 1000
#define MEGA 1000000

#define KIBI 1024
#define MEBI 1048576

// Flag indicating Bit Rate Switch (second bit rate for payload data)
#define CANFD_FLAG_BRS 0x01
// Error State Indicator of the transmitting node
#define CANFD_FLAG_ESI 0x02

// Flag indicating use of Extended Frame Format
#define CAN_FLAG_EFF 0x80000000U
// Flag indicating a remote transmission request
#define CAN_FLAG_RTR 0x40000000U
// Flag indicating an error message frame
#define CAN_FLAG_ERR 0x20000000U

// Maximum payload data length of a CAN-FD frame
#define CANFD_MAX_DATA_LENGTH 8
// Canard node id
#define CAN_NODE_ID 101

/* ------------------------------------------------------------------------ */

// Configuration for CAN interface
#define CAN_HEAP_SIZE 16
#define CAN_REDUNDANCY_FACTOR 1
#define CAN_TX_QUEUE_CAPACITY 100
#define CAN_FRAME_TIMEOUT 1000000000

#define MBFS_FDCAN_FILTER_ID_1 0x321
#define MBFS_FDCAN_FILTER_ID_2 0x7FF
#define MBFS_FDCAN_RX_BUFFER_SIZE 8
#define MBFS_FDCAN_TX_HEADER_ID 0x321

/* ------------------------------------------------------------------------ */

/* +++ Default values for configuration data +++ */

// Default experiment interval in seconds
#define DEFAULT_EXP_INTERVAL 4
// Default ambient monitoring interval in seconds
#define DEFAULT_AM_INTERVAL 16
// Default housekeeping interval in seconds
#define DEFAULT_HK_INTERVAL 60

// Default value for orbit duration in seconds
#define DEFAULT_ORBIT_DURATION 5400

// Default amount of storable orbit cycles for ambient monitoring data
#define DEFAULT_NUM_ORBITS_AM 3
// Default amount of storable complete experiment runs
#define DEFAULT_NUM_RUNS_EXP 3
// Default amount of storable single experiments
#define DEFAULT_NUM_SINGLE_EXP 3
// Default amount of storable experiment suites
#define DEFAULT_NUM_SUITES_EXP 3

// Default length of experiment run
#define DEFAULT_EXP_RUN_LENGTH 10

// Default bit mask of enabled experiments
#define DEFAULT_EXP_ENABLE 0x0703
// Default number of enabled experiments
#define DEFAULT_NUM_OF_EXP_ENABLE 19

/* ------------------------------------------------------------------------ */

// Maximum length of experiment run (due to memory allocation)
#define MAX_EXP_RUN_LENGTH 10

#endif /* INC_CONSTANTS_H_ */
