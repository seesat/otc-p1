// This is an AUTO-GENERATED Cyphal DSDL data type implementation. Curious? See https://opencyphal.org.
// You shouldn't attempt to edit this file.
//
// Checking this file under version control is not recommended unless it is used as part of a high-SIL
// safety-critical codebase. The typical usage scenario is to generate it as part of the build process.
//
// To avoid conflicts with definitions given in the source DSDL file, all entities created by the code generator
// are named with an underscore at the end, like foo_bar_().
//
// Generator:     nunavut-2.3.1 (serialization was enabled)
// Source file:   /Users/stefan/Documents/SeeSat/MOBeFuse/git/software/Config/cyphal/custom_data_types/seesat/mobefuse/110.AmbientMonitoringReport.1.0.dsdl
// Generated at:  2024-08-09 19:57:24.926564 UTC
// Is deprecated: no
// Fixed port-ID: 110
// Full name:     seesat.mobefuse.AmbientMonitoringReport
// Version:       1.0
//
// Platform
//     python_implementation:  CPython
//     python_version:  3.12.0
//     python_release_level:  final
//     python_build:  ('v3.12.0:0fb18b02c8', 'Oct  2 2023 09:45:56')
//     python_compiler:  Clang 13.0.0 (clang-1300.0.29.30)
//     python_revision:  0fb18b02c8
//     python_xoptions:  {}
//     runtime_platform:  macOS-14.5-arm64-arm-64bit
//
// Language Options
//     target_endianness:  any
//     omit_float_serialization_support:  False
//     enable_serialization_asserts:  True
//     enable_override_variable_array_capacity:  False
//     cast_format:  (({type}) {value})

#ifndef SEESAT_MOBEFUSE_AMBIENT_MONITORING_REPORT_1_0_INCLUDED_
#define SEESAT_MOBEFUSE_AMBIENT_MONITORING_REPORT_1_0_INCLUDED_

#include <nunavut/support/serialization.h>
#include <stdint.h>
#include <stdlib.h>

static_assert( NUNAVUT_SUPPORT_LANGUAGE_OPTION_TARGET_ENDIANNESS == 1693710260,
              "/Users/stefan/Documents/SeeSat/MOBeFuse/git/software/Config/cyphal/custom_data_types/seesat/mobefuse/110.AmbientMonitoringReport.1.0.dsdl is trying to use a serialization library that was compiled with "
              "different language options. This is dangerous and therefore not allowed." );
static_assert( NUNAVUT_SUPPORT_LANGUAGE_OPTION_OMIT_FLOAT_SERIALIZATION_SUPPORT == 0,
              "/Users/stefan/Documents/SeeSat/MOBeFuse/git/software/Config/cyphal/custom_data_types/seesat/mobefuse/110.AmbientMonitoringReport.1.0.dsdl is trying to use a serialization library that was compiled with "
              "different language options. This is dangerous and therefore not allowed." );
static_assert( NUNAVUT_SUPPORT_LANGUAGE_OPTION_ENABLE_SERIALIZATION_ASSERTS == 1,
              "/Users/stefan/Documents/SeeSat/MOBeFuse/git/software/Config/cyphal/custom_data_types/seesat/mobefuse/110.AmbientMonitoringReport.1.0.dsdl is trying to use a serialization library that was compiled with "
              "different language options. This is dangerous and therefore not allowed." );
static_assert( NUNAVUT_SUPPORT_LANGUAGE_OPTION_ENABLE_OVERRIDE_VARIABLE_ARRAY_CAPACITY == 0,
              "/Users/stefan/Documents/SeeSat/MOBeFuse/git/software/Config/cyphal/custom_data_types/seesat/mobefuse/110.AmbientMonitoringReport.1.0.dsdl is trying to use a serialization library that was compiled with "
              "different language options. This is dangerous and therefore not allowed." );
static_assert( NUNAVUT_SUPPORT_LANGUAGE_OPTION_CAST_FORMAT == 2368206204,
              "/Users/stefan/Documents/SeeSat/MOBeFuse/git/software/Config/cyphal/custom_data_types/seesat/mobefuse/110.AmbientMonitoringReport.1.0.dsdl is trying to use a serialization library that was compiled with "
              "different language options. This is dangerous and therefore not allowed." );

#ifdef __cplusplus
extern "C" {
#endif

#define seesat_mobefuse_AmbientMonitoringReport_1_0_HAS_FIXED_PORT_ID_ true
#define seesat_mobefuse_AmbientMonitoringReport_1_0_FIXED_PORT_ID_     110U

// +-------------------------------------------------------------------------------------------------------------------+
// | seesat.mobefuse.AmbientMonitoringReport.1.0
// +-------------------------------------------------------------------------------------------------------------------+
#define seesat_mobefuse_AmbientMonitoringReport_1_0_FULL_NAME_             "seesat.mobefuse.AmbientMonitoringReport"
#define seesat_mobefuse_AmbientMonitoringReport_1_0_FULL_NAME_AND_VERSION_ "seesat.mobefuse.AmbientMonitoringReport.1.0"

/// Extent is the minimum amount of memory required to hold any serialized representation of any compatible
/// version of the data type; or, on other words, it is the the maximum possible size of received objects of this type.
/// The size is specified in bytes (rather than bits) because by definition, extent is an integer number of bytes long.
/// When allocating a deserialization (RX) buffer for this data type, it should be at least extent bytes large.
/// When allocating a serialization (TX) buffer, it is safe to use the size of the largest serialized representation
/// instead of the extent because it provides a tighter bound of the object size; it is safe because the concrete type
/// is always known during serialization (unlike deserialization). If not sure, use extent everywhere.
#define seesat_mobefuse_AmbientMonitoringReport_1_0_EXTENT_BYTES_                    24UL
#define seesat_mobefuse_AmbientMonitoringReport_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_ 24UL
static_assert(seesat_mobefuse_AmbientMonitoringReport_1_0_EXTENT_BYTES_ >= seesat_mobefuse_AmbientMonitoringReport_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_,
              "Internal constraint violation");

typedef struct
{
    /// saturated uint32 ts
    uint32_t ts;

    /// saturated uint16 rad0_v
    uint16_t rad0_v;

    /// saturated uint16 rad0_i
    uint16_t rad0_i;

    /// saturated uint16 rad1_v
    uint16_t rad1_v;

    /// saturated uint16 rad1_i
    uint16_t rad1_i;

    /// saturated uint16 rad2_v
    uint16_t rad2_v;

    /// saturated uint16 rad2_i
    uint16_t rad2_i;

    /// saturated uint16 rad12_v
    uint16_t rad12_v;

    /// saturated uint16 rad12_i
    uint16_t rad12_i;

    /// saturated uint16 temp_1
    uint16_t temp_1;

    /// saturated uint16 temp_2
    uint16_t temp_2;
} seesat_mobefuse_AmbientMonitoringReport_1_0;

/// Serialize an instance into the provided buffer.
/// The lifetime of the resulting serialized representation is independent of the original instance.
/// This method may be slow for large objects (e.g., images, point clouds, radar samples), so in a later revision
/// we may define a zero-copy alternative that keeps references to the original object where possible.
///
/// @param obj      The object to serialize.
///
/// @param buffer   The destination buffer. There are no alignment requirements.
///                 @see seesat_mobefuse_AmbientMonitoringReport_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_
///
/// @param inout_buffer_size_bytes  When calling, this is a pointer to the size of the buffer in bytes.
///                                 Upon return this value will be updated with the size of the constructed serialized
///                                 representation (in bytes); this value is then to be passed over to the transport
///                                 layer. In case of error this value is undefined.
///
/// @returns Negative on error, zero on success.
static inline int8_t seesat_mobefuse_AmbientMonitoringReport_1_0_serialize_(
    const seesat_mobefuse_AmbientMonitoringReport_1_0* const obj, uint8_t* const buffer,  size_t* const inout_buffer_size_bytes)
{
    if ((obj == NULL) || (buffer == NULL) || (inout_buffer_size_bytes == NULL))
    {
        return -NUNAVUT_ERROR_INVALID_ARGUMENT;
    }
    const size_t capacity_bytes = *inout_buffer_size_bytes;
    if ((8U * (size_t) capacity_bytes) < 192UL)
    {
        return -NUNAVUT_ERROR_SERIALIZATION_BUFFER_TOO_SMALL;
    }
    // Notice that fields that are not an integer number of bytes long may overrun the space allocated for them
    // in the serialization buffer up to the next byte boundary. This is by design and is guaranteed to be safe.
    size_t offset_bits = 0U;
    {   // saturated uint32 ts
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits + 32ULL) <= (capacity_bytes * 8U));
        // Saturation code not emitted -- native representation matches the serialized representation.
        const int8_t _err0_ = nunavutSetUxx(&buffer[0], capacity_bytes, offset_bits, obj->ts, 32U);
        if (_err0_ < 0)
        {
            return _err0_;
        }
        offset_bits += 32U;
    }
    {   // saturated uint16 rad0_v
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits + 16ULL) <= (capacity_bytes * 8U));
        // Saturation code not emitted -- native representation matches the serialized representation.
        const int8_t _err1_ = nunavutSetUxx(&buffer[0], capacity_bytes, offset_bits, obj->rad0_v, 16U);
        if (_err1_ < 0)
        {
            return _err1_;
        }
        offset_bits += 16U;
    }
    {   // saturated uint16 rad0_i
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits + 16ULL) <= (capacity_bytes * 8U));
        // Saturation code not emitted -- native representation matches the serialized representation.
        const int8_t _err2_ = nunavutSetUxx(&buffer[0], capacity_bytes, offset_bits, obj->rad0_i, 16U);
        if (_err2_ < 0)
        {
            return _err2_;
        }
        offset_bits += 16U;
    }
    {   // saturated uint16 rad1_v
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits + 16ULL) <= (capacity_bytes * 8U));
        // Saturation code not emitted -- native representation matches the serialized representation.
        const int8_t _err3_ = nunavutSetUxx(&buffer[0], capacity_bytes, offset_bits, obj->rad1_v, 16U);
        if (_err3_ < 0)
        {
            return _err3_;
        }
        offset_bits += 16U;
    }
    {   // saturated uint16 rad1_i
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits + 16ULL) <= (capacity_bytes * 8U));
        // Saturation code not emitted -- native representation matches the serialized representation.
        const int8_t _err4_ = nunavutSetUxx(&buffer[0], capacity_bytes, offset_bits, obj->rad1_i, 16U);
        if (_err4_ < 0)
        {
            return _err4_;
        }
        offset_bits += 16U;
    }
    {   // saturated uint16 rad2_v
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits + 16ULL) <= (capacity_bytes * 8U));
        // Saturation code not emitted -- native representation matches the serialized representation.
        const int8_t _err5_ = nunavutSetUxx(&buffer[0], capacity_bytes, offset_bits, obj->rad2_v, 16U);
        if (_err5_ < 0)
        {
            return _err5_;
        }
        offset_bits += 16U;
    }
    {   // saturated uint16 rad2_i
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits + 16ULL) <= (capacity_bytes * 8U));
        // Saturation code not emitted -- native representation matches the serialized representation.
        const int8_t _err6_ = nunavutSetUxx(&buffer[0], capacity_bytes, offset_bits, obj->rad2_i, 16U);
        if (_err6_ < 0)
        {
            return _err6_;
        }
        offset_bits += 16U;
    }
    {   // saturated uint16 rad12_v
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits + 16ULL) <= (capacity_bytes * 8U));
        // Saturation code not emitted -- native representation matches the serialized representation.
        const int8_t _err7_ = nunavutSetUxx(&buffer[0], capacity_bytes, offset_bits, obj->rad12_v, 16U);
        if (_err7_ < 0)
        {
            return _err7_;
        }
        offset_bits += 16U;
    }
    {   // saturated uint16 rad12_i
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits + 16ULL) <= (capacity_bytes * 8U));
        // Saturation code not emitted -- native representation matches the serialized representation.
        const int8_t _err8_ = nunavutSetUxx(&buffer[0], capacity_bytes, offset_bits, obj->rad12_i, 16U);
        if (_err8_ < 0)
        {
            return _err8_;
        }
        offset_bits += 16U;
    }
    {   // saturated uint16 temp_1
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits + 16ULL) <= (capacity_bytes * 8U));
        // Saturation code not emitted -- native representation matches the serialized representation.
        const int8_t _err9_ = nunavutSetUxx(&buffer[0], capacity_bytes, offset_bits, obj->temp_1, 16U);
        if (_err9_ < 0)
        {
            return _err9_;
        }
        offset_bits += 16U;
    }
    {   // saturated uint16 temp_2
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits + 16ULL) <= (capacity_bytes * 8U));
        // Saturation code not emitted -- native representation matches the serialized representation.
        const int8_t _err10_ = nunavutSetUxx(&buffer[0], capacity_bytes, offset_bits, obj->temp_2, 16U);
        if (_err10_ < 0)
        {
            return _err10_;
        }
        offset_bits += 16U;
    }
    if (offset_bits % 8U != 0U)  // Pad to 8 bits. TODO: Eliminate redundant padding checks.
    {
        const uint8_t _pad0_ = (uint8_t)(8U - offset_bits % 8U);
        NUNAVUT_ASSERT(_pad0_ > 0);
        const int8_t _err11_ = nunavutSetUxx(&buffer[0], capacity_bytes, offset_bits, 0U, _pad0_);  // Optimize?
        if (_err11_ < 0)
        {
            return _err11_;
        }
        offset_bits += _pad0_;
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    }
    // It is assumed that we know the exact type of the serialized entity, hence we expect the size to match.
    NUNAVUT_ASSERT(offset_bits == 192ULL);
    NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    *inout_buffer_size_bytes = (size_t) (offset_bits / 8U);
    return NUNAVUT_SUCCESS;
}

/// Deserialize an instance from the provided buffer.
/// The lifetime of the resulting object is independent of the original buffer.
/// This method may be slow for large objects (e.g., images, point clouds, radar samples), so in a later revision
/// we may define a zero-copy alternative that keeps references to the original buffer where possible.
///
/// @param obj      The object to update from the provided serialized representation.
///
/// @param buffer   The source buffer containing the serialized representation. There are no alignment requirements.
///                 If the buffer is shorter or longer than expected, it will be implicitly zero-extended or truncated,
///                 respectively; see Specification for "implicit zero extension" and "implicit truncation" rules.
///
/// @param inout_buffer_size_bytes  When calling, this is a pointer to the size of the supplied serialized
///                                 representation, in bytes. Upon return this value will be updated with the
///                                 size of the consumed fragment of the serialized representation (in bytes),
///                                 which may be smaller due to the implicit truncation rule, but it is guaranteed
///                                 to never exceed the original buffer size even if the implicit zero extension rule
///                                 was activated. In case of error this value is undefined.
///
/// @returns Negative on error, zero on success.
static inline int8_t seesat_mobefuse_AmbientMonitoringReport_1_0_deserialize_(
    seesat_mobefuse_AmbientMonitoringReport_1_0* const out_obj, const uint8_t* buffer, size_t* const inout_buffer_size_bytes)
{
    if ((out_obj == NULL) || (inout_buffer_size_bytes == NULL) || ((buffer == NULL) && (0 != *inout_buffer_size_bytes)))
    {
        return -NUNAVUT_ERROR_INVALID_ARGUMENT;
    }
    if (buffer == NULL)
    {
        buffer = (const uint8_t*)"";
    }
    const size_t capacity_bytes = *inout_buffer_size_bytes;
    const size_t capacity_bits = capacity_bytes * (size_t) 8U;
    size_t offset_bits = 0U;
    // saturated uint32 ts
    NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    out_obj->ts = nunavutGetU32(&buffer[0], capacity_bytes, offset_bits, 32);
    offset_bits += 32U;
    // saturated uint16 rad0_v
    NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    out_obj->rad0_v = nunavutGetU16(&buffer[0], capacity_bytes, offset_bits, 16);
    offset_bits += 16U;
    // saturated uint16 rad0_i
    NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    out_obj->rad0_i = nunavutGetU16(&buffer[0], capacity_bytes, offset_bits, 16);
    offset_bits += 16U;
    // saturated uint16 rad1_v
    NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    out_obj->rad1_v = nunavutGetU16(&buffer[0], capacity_bytes, offset_bits, 16);
    offset_bits += 16U;
    // saturated uint16 rad1_i
    NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    out_obj->rad1_i = nunavutGetU16(&buffer[0], capacity_bytes, offset_bits, 16);
    offset_bits += 16U;
    // saturated uint16 rad2_v
    NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    out_obj->rad2_v = nunavutGetU16(&buffer[0], capacity_bytes, offset_bits, 16);
    offset_bits += 16U;
    // saturated uint16 rad2_i
    NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    out_obj->rad2_i = nunavutGetU16(&buffer[0], capacity_bytes, offset_bits, 16);
    offset_bits += 16U;
    // saturated uint16 rad12_v
    NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    out_obj->rad12_v = nunavutGetU16(&buffer[0], capacity_bytes, offset_bits, 16);
    offset_bits += 16U;
    // saturated uint16 rad12_i
    NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    out_obj->rad12_i = nunavutGetU16(&buffer[0], capacity_bytes, offset_bits, 16);
    offset_bits += 16U;
    // saturated uint16 temp_1
    NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    out_obj->temp_1 = nunavutGetU16(&buffer[0], capacity_bytes, offset_bits, 16);
    offset_bits += 16U;
    // saturated uint16 temp_2
    NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    out_obj->temp_2 = nunavutGetU16(&buffer[0], capacity_bytes, offset_bits, 16);
    offset_bits += 16U;
    offset_bits = (offset_bits + 7U) & ~(size_t) 7U;  // Align on 8 bits.
    NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    *inout_buffer_size_bytes = (size_t) (nunavutChooseMin(offset_bits, capacity_bits) / 8U);
    NUNAVUT_ASSERT(capacity_bytes >= *inout_buffer_size_bytes);
    return NUNAVUT_SUCCESS;
}

/// Initialize an instance to default values. Does nothing if @param out_obj is NULL.
/// This function intentionally leaves inactive elements uninitialized; for example, members of a variable-length
/// array beyond its length are left uninitialized; aliased union memory that is not used by the first union field
/// is left uninitialized, etc. If full zero-initialization is desired, just use memset(&obj, 0, sizeof(obj)).
static inline void seesat_mobefuse_AmbientMonitoringReport_1_0_initialize_(seesat_mobefuse_AmbientMonitoringReport_1_0* const out_obj)
{
    if (out_obj != NULL)
    {
        size_t size_bytes = 0;
        const uint8_t buf = 0;
        const int8_t err = seesat_mobefuse_AmbientMonitoringReport_1_0_deserialize_(out_obj, &buf, &size_bytes);
        NUNAVUT_ASSERT(err >= 0);
        (void) err;
    }
}

#ifdef __cplusplus
}
#endif
#endif // SEESAT_MOBEFUSE_AMBIENT_MONITORING_REPORT_1_0_INCLUDED_
