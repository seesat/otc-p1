// This is an AUTO-GENERATED Cyphal DSDL data type implementation. Curious? See https://opencyphal.org.
// You shouldn't attempt to edit this file.
//
// Checking this file under version control is not recommended unless it is used as part of a high-SIL
// safety-critical codebase. The typical usage scenario is to generate it as part of the build process.
//
// To avoid conflicts with definitions given in the source DSDL file, all entities created by the code generator
// are named with an underscore at the end, like foo_bar_().
//
// Generator:     nunavut-2.3.1 (serialization was enabled)
// Source file:   /Users/stefan/Documents/SeeSat/MOBeFuse/git/software/Config/cyphal/custom_data_types/seesat/mobefuse/100.MOBeFuseTM.1.0.dsdl
// Generated at:  2024-08-09 19:57:25.059194 UTC
// Is deprecated: no
// Fixed port-ID: 100
// Full name:     seesat.mobefuse.MOBeFuseTM
// Version:       1.0
//
// Platform
//     python_implementation:  CPython
//     python_version:  3.12.0
//     python_release_level:  final
//     python_build:  ('v3.12.0:0fb18b02c8', 'Oct  2 2023 09:45:56')
//     python_compiler:  Clang 13.0.0 (clang-1300.0.29.30)
//     python_revision:  0fb18b02c8
//     python_xoptions:  {}
//     runtime_platform:  macOS-14.5-arm64-arm-64bit
//
// Language Options
//     target_endianness:  any
//     omit_float_serialization_support:  False
//     enable_serialization_asserts:  True
//     enable_override_variable_array_capacity:  False
//     cast_format:  (({type}) {value})

#ifndef SEESAT_MOBEFUSE_MO_BE_FUSE_TM_1_0_INCLUDED_
#define SEESAT_MOBEFUSE_MO_BE_FUSE_TM_1_0_INCLUDED_

#include <nunavut/support/serialization.h>
#include <seesat/mobefuse/AmbientMonitoringReport_1_0.h>
#include <seesat/mobefuse/ConfigurationParameterReport_1_0.h>
#include <seesat/mobefuse/HousekeepingReport_1_0.h>
#include <seesat/mobefuse/SingleExperimentReport_1_0.h>
#include <stdint.h>
#include <stdlib.h>

static_assert( NUNAVUT_SUPPORT_LANGUAGE_OPTION_TARGET_ENDIANNESS == 1693710260,
              "/Users/stefan/Documents/SeeSat/MOBeFuse/git/software/Config/cyphal/custom_data_types/seesat/mobefuse/100.MOBeFuseTM.1.0.dsdl is trying to use a serialization library that was compiled with "
              "different language options. This is dangerous and therefore not allowed." );
static_assert( NUNAVUT_SUPPORT_LANGUAGE_OPTION_OMIT_FLOAT_SERIALIZATION_SUPPORT == 0,
              "/Users/stefan/Documents/SeeSat/MOBeFuse/git/software/Config/cyphal/custom_data_types/seesat/mobefuse/100.MOBeFuseTM.1.0.dsdl is trying to use a serialization library that was compiled with "
              "different language options. This is dangerous and therefore not allowed." );
static_assert( NUNAVUT_SUPPORT_LANGUAGE_OPTION_ENABLE_SERIALIZATION_ASSERTS == 1,
              "/Users/stefan/Documents/SeeSat/MOBeFuse/git/software/Config/cyphal/custom_data_types/seesat/mobefuse/100.MOBeFuseTM.1.0.dsdl is trying to use a serialization library that was compiled with "
              "different language options. This is dangerous and therefore not allowed." );
static_assert( NUNAVUT_SUPPORT_LANGUAGE_OPTION_ENABLE_OVERRIDE_VARIABLE_ARRAY_CAPACITY == 0,
              "/Users/stefan/Documents/SeeSat/MOBeFuse/git/software/Config/cyphal/custom_data_types/seesat/mobefuse/100.MOBeFuseTM.1.0.dsdl is trying to use a serialization library that was compiled with "
              "different language options. This is dangerous and therefore not allowed." );
static_assert( NUNAVUT_SUPPORT_LANGUAGE_OPTION_CAST_FORMAT == 2368206204,
              "/Users/stefan/Documents/SeeSat/MOBeFuse/git/software/Config/cyphal/custom_data_types/seesat/mobefuse/100.MOBeFuseTM.1.0.dsdl is trying to use a serialization library that was compiled with "
              "different language options. This is dangerous and therefore not allowed." );

#ifdef __cplusplus
extern "C" {
#endif

#define seesat_mobefuse_MOBeFuseTM_1_0_HAS_FIXED_PORT_ID_ true
#define seesat_mobefuse_MOBeFuseTM_1_0_FIXED_PORT_ID_     100U

// +-------------------------------------------------------------------------------------------------------------------+
// | seesat.mobefuse.MOBeFuseTM.1.0
// +-------------------------------------------------------------------------------------------------------------------+
#define seesat_mobefuse_MOBeFuseTM_1_0_FULL_NAME_             "seesat.mobefuse.MOBeFuseTM"
#define seesat_mobefuse_MOBeFuseTM_1_0_FULL_NAME_AND_VERSION_ "seesat.mobefuse.MOBeFuseTM.1.0"

/// Extent is the minimum amount of memory required to hold any serialized representation of any compatible
/// version of the data type; or, on other words, it is the the maximum possible size of received objects of this type.
/// The size is specified in bytes (rather than bits) because by definition, extent is an integer number of bytes long.
/// When allocating a deserialization (RX) buffer for this data type, it should be at least extent bytes large.
/// When allocating a serialization (TX) buffer, it is safe to use the size of the largest serialized representation
/// instead of the extent because it provides a tighter bound of the object size; it is safe because the concrete type
/// is always known during serialization (unlike deserialization). If not sure, use extent everywhere.
#define seesat_mobefuse_MOBeFuseTM_1_0_EXTENT_BYTES_                    49UL
#define seesat_mobefuse_MOBeFuseTM_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_ 49UL
static_assert(seesat_mobefuse_MOBeFuseTM_1_0_EXTENT_BYTES_ >= seesat_mobefuse_MOBeFuseTM_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_,
              "Internal constraint violation");

typedef struct
{
    union  /// The union is placed first to ensure that the active element address equals the struct address.
    {
        /// seesat.mobefuse.AmbientMonitoringReport.1.0 am_report
        seesat_mobefuse_AmbientMonitoringReport_1_0 am_report;

        /// seesat.mobefuse.HousekeepingReport.1.0 hk_report
        seesat_mobefuse_HousekeepingReport_1_0 hk_report;

        /// seesat.mobefuse.SingleExperimentReport.1.0 exp_report
        seesat_mobefuse_SingleExperimentReport_1_0 exp_report;

        /// seesat.mobefuse.ConfigurationParameterReport.1.0 config_report
        seesat_mobefuse_ConfigurationParameterReport_1_0 config_report;
    };
    uint8_t _tag_;
} seesat_mobefuse_MOBeFuseTM_1_0;

/// The number of fields in the union. Valid tag values range from zero to this value minus one, inclusive.
#define seesat_mobefuse_MOBeFuseTM_1_0_UNION_OPTION_COUNT_ 4U

/// Serialize an instance into the provided buffer.
/// The lifetime of the resulting serialized representation is independent of the original instance.
/// This method may be slow for large objects (e.g., images, point clouds, radar samples), so in a later revision
/// we may define a zero-copy alternative that keeps references to the original object where possible.
///
/// @param obj      The object to serialize.
///
/// @param buffer   The destination buffer. There are no alignment requirements.
///                 @see seesat_mobefuse_MOBeFuseTM_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_
///
/// @param inout_buffer_size_bytes  When calling, this is a pointer to the size of the buffer in bytes.
///                                 Upon return this value will be updated with the size of the constructed serialized
///                                 representation (in bytes); this value is then to be passed over to the transport
///                                 layer. In case of error this value is undefined.
///
/// @returns Negative on error, zero on success.
static inline int8_t seesat_mobefuse_MOBeFuseTM_1_0_serialize_(
    const seesat_mobefuse_MOBeFuseTM_1_0* const obj, uint8_t* const buffer,  size_t* const inout_buffer_size_bytes)
{
    if ((obj == NULL) || (buffer == NULL) || (inout_buffer_size_bytes == NULL))
    {
        return -NUNAVUT_ERROR_INVALID_ARGUMENT;
    }
    const size_t capacity_bytes = *inout_buffer_size_bytes;
    if ((8U * (size_t) capacity_bytes) < 392UL)
    {
        return -NUNAVUT_ERROR_SERIALIZATION_BUFFER_TOO_SMALL;
    }
    // Notice that fields that are not an integer number of bytes long may overrun the space allocated for them
    // in the serialization buffer up to the next byte boundary. This is by design and is guaranteed to be safe.
    size_t offset_bits = 0U;
    {   // Union tag field: truncated uint8
        buffer[offset_bits / 8U] = (uint8_t)(obj->_tag_);  // C std, 6.3.1.3 Signed and unsigned integers
        offset_bits += 8U;
    }
    if (0U == obj->_tag_)  // seesat.mobefuse.AmbientMonitoringReport.1.0 am_report
    {
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits + 192ULL) <= (capacity_bytes * 8U));
        size_t _size_bytes0_ = 24UL;  // Nested object (max) size, in bytes.
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits / 8U + _size_bytes0_) <= capacity_bytes);
        int8_t _err0_ = seesat_mobefuse_AmbientMonitoringReport_1_0_serialize_(
            &obj->am_report, &buffer[offset_bits / 8U], &_size_bytes0_);
        if (_err0_ < 0)
        {
            return _err0_;
        }
        // It is assumed that we know the exact type of the serialized entity, hence we expect the size to match.
        NUNAVUT_ASSERT((_size_bytes0_ * 8U) == 192ULL);
        offset_bits += _size_bytes0_ * 8U;  // Advance by the size of the nested object.
        NUNAVUT_ASSERT(offset_bits <= (capacity_bytes * 8U));
    }
    else if (1U == obj->_tag_)  // seesat.mobefuse.HousekeepingReport.1.0 hk_report
    {
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits + 312ULL) <= (capacity_bytes * 8U));
        size_t _size_bytes1_ = 39UL;  // Nested object (max) size, in bytes.
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits / 8U + _size_bytes1_) <= capacity_bytes);
        int8_t _err1_ = seesat_mobefuse_HousekeepingReport_1_0_serialize_(
            &obj->hk_report, &buffer[offset_bits / 8U], &_size_bytes1_);
        if (_err1_ < 0)
        {
            return _err1_;
        }
        // It is assumed that we know the exact type of the serialized entity, hence we expect the size to match.
        NUNAVUT_ASSERT((_size_bytes1_ * 8U) == 312ULL);
        offset_bits += _size_bytes1_ * 8U;  // Advance by the size of the nested object.
        NUNAVUT_ASSERT(offset_bits <= (capacity_bytes * 8U));
    }
    else if (2U == obj->_tag_)  // seesat.mobefuse.SingleExperimentReport.1.0 exp_report
    {
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits + 112ULL) <= (capacity_bytes * 8U));
        size_t _size_bytes2_ = 14UL;  // Nested object (max) size, in bytes.
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits / 8U + _size_bytes2_) <= capacity_bytes);
        int8_t _err2_ = seesat_mobefuse_SingleExperimentReport_1_0_serialize_(
            &obj->exp_report, &buffer[offset_bits / 8U], &_size_bytes2_);
        if (_err2_ < 0)
        {
            return _err2_;
        }
        // It is assumed that we know the exact type of the serialized entity, hence we expect the size to match.
        NUNAVUT_ASSERT((_size_bytes2_ * 8U) == 112ULL);
        offset_bits += _size_bytes2_ * 8U;  // Advance by the size of the nested object.
        NUNAVUT_ASSERT(offset_bits <= (capacity_bytes * 8U));
    }
    else if (3U == obj->_tag_)  // seesat.mobefuse.ConfigurationParameterReport.1.0 config_report
    {
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits + 384ULL) <= (capacity_bytes * 8U));
        size_t _size_bytes3_ = 48UL;  // Nested object (max) size, in bytes.
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT((offset_bits / 8U + _size_bytes3_) <= capacity_bytes);
        int8_t _err3_ = seesat_mobefuse_ConfigurationParameterReport_1_0_serialize_(
            &obj->config_report, &buffer[offset_bits / 8U], &_size_bytes3_);
        if (_err3_ < 0)
        {
            return _err3_;
        }
        // It is assumed that we know the exact type of the serialized entity, hence we expect the size to match.
        NUNAVUT_ASSERT((_size_bytes3_ * 8U) == 384ULL);
        offset_bits += _size_bytes3_ * 8U;  // Advance by the size of the nested object.
        NUNAVUT_ASSERT(offset_bits <= (capacity_bytes * 8U));
    }
    else
    {
        return -NUNAVUT_ERROR_REPRESENTATION_BAD_UNION_TAG;
    }
    if (offset_bits % 8U != 0U)  // Pad to 8 bits. TODO: Eliminate redundant padding checks.
    {
        const uint8_t _pad0_ = (uint8_t)(8U - offset_bits % 8U);
        NUNAVUT_ASSERT(_pad0_ > 0);
        const int8_t _err4_ = nunavutSetUxx(&buffer[0], capacity_bytes, offset_bits, 0U, _pad0_);  // Optimize?
        if (_err4_ < 0)
        {
            return _err4_;
        }
        offset_bits += _pad0_;
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    }
    // It is assumed that we know the exact type of the serialized entity, hence we expect the size to match.
    NUNAVUT_ASSERT(offset_bits >= 120ULL);
    NUNAVUT_ASSERT(offset_bits <= 392ULL);
    NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    *inout_buffer_size_bytes = (size_t) (offset_bits / 8U);
    return NUNAVUT_SUCCESS;
}

/// Deserialize an instance from the provided buffer.
/// The lifetime of the resulting object is independent of the original buffer.
/// This method may be slow for large objects (e.g., images, point clouds, radar samples), so in a later revision
/// we may define a zero-copy alternative that keeps references to the original buffer where possible.
///
/// @param obj      The object to update from the provided serialized representation.
///
/// @param buffer   The source buffer containing the serialized representation. There are no alignment requirements.
///                 If the buffer is shorter or longer than expected, it will be implicitly zero-extended or truncated,
///                 respectively; see Specification for "implicit zero extension" and "implicit truncation" rules.
///
/// @param inout_buffer_size_bytes  When calling, this is a pointer to the size of the supplied serialized
///                                 representation, in bytes. Upon return this value will be updated with the
///                                 size of the consumed fragment of the serialized representation (in bytes),
///                                 which may be smaller due to the implicit truncation rule, but it is guaranteed
///                                 to never exceed the original buffer size even if the implicit zero extension rule
///                                 was activated. In case of error this value is undefined.
///
/// @returns Negative on error, zero on success.
static inline int8_t seesat_mobefuse_MOBeFuseTM_1_0_deserialize_(
    seesat_mobefuse_MOBeFuseTM_1_0* const out_obj, const uint8_t* buffer, size_t* const inout_buffer_size_bytes)
{
    if ((out_obj == NULL) || (inout_buffer_size_bytes == NULL) || ((buffer == NULL) && (0 != *inout_buffer_size_bytes)))
    {
        return -NUNAVUT_ERROR_INVALID_ARGUMENT;
    }
    if (buffer == NULL)
    {
        buffer = (const uint8_t*)"";
    }
    const size_t capacity_bytes = *inout_buffer_size_bytes;
    const size_t capacity_bits = capacity_bytes * (size_t) 8U;
    size_t offset_bits = 0U;
    // Union tag field: truncated uint8
    if ((offset_bits + 8U) <= capacity_bits)
    {
        out_obj->_tag_ = buffer[offset_bits / 8U] & 255U;
    }
    else
    {
        out_obj->_tag_ = 0U;
    }
    offset_bits += 8U;
    if (0U == out_obj->_tag_)  // seesat.mobefuse.AmbientMonitoringReport.1.0 am_report
    {
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        {
            size_t _size_bytes4_ = (size_t)(capacity_bytes - nunavutChooseMin((offset_bits / 8U), capacity_bytes));
            NUNAVUT_ASSERT(offset_bits % 8U == 0U);
            const int8_t _err5_ = seesat_mobefuse_AmbientMonitoringReport_1_0_deserialize_(
                &out_obj->am_report, &buffer[offset_bits / 8U], &_size_bytes4_);
            if (_err5_ < 0)
            {
                return _err5_;
            }
            offset_bits += _size_bytes4_ * 8U;  // Advance by the size of the nested serialized representation.
        }
    }
    else if (1U == out_obj->_tag_)  // seesat.mobefuse.HousekeepingReport.1.0 hk_report
    {
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        {
            size_t _size_bytes5_ = (size_t)(capacity_bytes - nunavutChooseMin((offset_bits / 8U), capacity_bytes));
            NUNAVUT_ASSERT(offset_bits % 8U == 0U);
            const int8_t _err6_ = seesat_mobefuse_HousekeepingReport_1_0_deserialize_(
                &out_obj->hk_report, &buffer[offset_bits / 8U], &_size_bytes5_);
            if (_err6_ < 0)
            {
                return _err6_;
            }
            offset_bits += _size_bytes5_ * 8U;  // Advance by the size of the nested serialized representation.
        }
    }
    else if (2U == out_obj->_tag_)  // seesat.mobefuse.SingleExperimentReport.1.0 exp_report
    {
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        {
            size_t _size_bytes6_ = (size_t)(capacity_bytes - nunavutChooseMin((offset_bits / 8U), capacity_bytes));
            NUNAVUT_ASSERT(offset_bits % 8U == 0U);
            const int8_t _err7_ = seesat_mobefuse_SingleExperimentReport_1_0_deserialize_(
                &out_obj->exp_report, &buffer[offset_bits / 8U], &_size_bytes6_);
            if (_err7_ < 0)
            {
                return _err7_;
            }
            offset_bits += _size_bytes6_ * 8U;  // Advance by the size of the nested serialized representation.
        }
    }
    else if (3U == out_obj->_tag_)  // seesat.mobefuse.ConfigurationParameterReport.1.0 config_report
    {
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        NUNAVUT_ASSERT(offset_bits % 8U == 0U);
        {
            size_t _size_bytes7_ = (size_t)(capacity_bytes - nunavutChooseMin((offset_bits / 8U), capacity_bytes));
            NUNAVUT_ASSERT(offset_bits % 8U == 0U);
            const int8_t _err8_ = seesat_mobefuse_ConfigurationParameterReport_1_0_deserialize_(
                &out_obj->config_report, &buffer[offset_bits / 8U], &_size_bytes7_);
            if (_err8_ < 0)
            {
                return _err8_;
            }
            offset_bits += _size_bytes7_ * 8U;  // Advance by the size of the nested serialized representation.
        }
    }
    else
    {
        return -NUNAVUT_ERROR_REPRESENTATION_BAD_UNION_TAG;
    }
    offset_bits = (offset_bits + 7U) & ~(size_t) 7U;  // Align on 8 bits.
    NUNAVUT_ASSERT(offset_bits % 8U == 0U);
    *inout_buffer_size_bytes = (size_t) (nunavutChooseMin(offset_bits, capacity_bits) / 8U);
    NUNAVUT_ASSERT(capacity_bytes >= *inout_buffer_size_bytes);
    return NUNAVUT_SUCCESS;
}

/// Initialize an instance to default values. Does nothing if @param out_obj is NULL.
/// This function intentionally leaves inactive elements uninitialized; for example, members of a variable-length
/// array beyond its length are left uninitialized; aliased union memory that is not used by the first union field
/// is left uninitialized, etc. If full zero-initialization is desired, just use memset(&obj, 0, sizeof(obj)).
static inline void seesat_mobefuse_MOBeFuseTM_1_0_initialize_(seesat_mobefuse_MOBeFuseTM_1_0* const out_obj)
{
    if (out_obj != NULL)
    {
        size_t size_bytes = 0;
        const uint8_t buf = 0;
        const int8_t err = seesat_mobefuse_MOBeFuseTM_1_0_deserialize_(out_obj, &buf, &size_bytes);
        NUNAVUT_ASSERT(err >= 0);
        (void) err;
    }
}
/// Mark option "am_report" active without initializing it. Does nothing if @param obj is NULL.
static inline void seesat_mobefuse_MOBeFuseTM_1_0_select_am_report_(seesat_mobefuse_MOBeFuseTM_1_0* const obj)
{
    if (obj != NULL)
    {
        obj->_tag_ = 0;
    }
}

/// Check if option "am_report" is active. Returns false if @param obj is NULL.
static inline bool seesat_mobefuse_MOBeFuseTM_1_0_is_am_report_(const seesat_mobefuse_MOBeFuseTM_1_0* const obj)
{
    return ((obj != NULL) && (obj->_tag_ == 0));
}

/// Mark option "hk_report" active without initializing it. Does nothing if @param obj is NULL.
static inline void seesat_mobefuse_MOBeFuseTM_1_0_select_hk_report_(seesat_mobefuse_MOBeFuseTM_1_0* const obj)
{
    if (obj != NULL)
    {
        obj->_tag_ = 1;
    }
}

/// Check if option "hk_report" is active. Returns false if @param obj is NULL.
static inline bool seesat_mobefuse_MOBeFuseTM_1_0_is_hk_report_(const seesat_mobefuse_MOBeFuseTM_1_0* const obj)
{
    return ((obj != NULL) && (obj->_tag_ == 1));
}

/// Mark option "exp_report" active without initializing it. Does nothing if @param obj is NULL.
static inline void seesat_mobefuse_MOBeFuseTM_1_0_select_exp_report_(seesat_mobefuse_MOBeFuseTM_1_0* const obj)
{
    if (obj != NULL)
    {
        obj->_tag_ = 2;
    }
}

/// Check if option "exp_report" is active. Returns false if @param obj is NULL.
static inline bool seesat_mobefuse_MOBeFuseTM_1_0_is_exp_report_(const seesat_mobefuse_MOBeFuseTM_1_0* const obj)
{
    return ((obj != NULL) && (obj->_tag_ == 2));
}

/// Mark option "config_report" active without initializing it. Does nothing if @param obj is NULL.
static inline void seesat_mobefuse_MOBeFuseTM_1_0_select_config_report_(seesat_mobefuse_MOBeFuseTM_1_0* const obj)
{
    if (obj != NULL)
    {
        obj->_tag_ = 3;
    }
}

/// Check if option "config_report" is active. Returns false if @param obj is NULL.
static inline bool seesat_mobefuse_MOBeFuseTM_1_0_is_config_report_(const seesat_mobefuse_MOBeFuseTM_1_0* const obj)
{
    return ((obj != NULL) && (obj->_tag_ == 3));
}

#ifdef __cplusplus
}
#endif
#endif // SEESAT_MOBEFUSE_MO_BE_FUSE_TM_1_0_INCLUDED_

