/**
 * @file data_types.h
 *
 * @brief Definition of data types for MOBeFuse.
 *
 * @author Stefan Wertheimer
 * @date 2023-12-22
 */

#ifndef INC_DATA_TYPES_H_
#define INC_DATA_TYPES_H_

/** Enumeration containing the bit values for enabled experiments*/
typedef enum exp_enable {
	DUT_3_OCP_02 = 0x0001,
	DUT_3_OCP_01 = 0x0002,
	DUT_3_OVP_01 = 0x0004,
	DUT_3_UVP_01 = 0x0008,
	DUT_2_OCP_02 = 0x0010,
	DUT_2_OCP_01 = 0x0020,
	DUT_2_OVP_01 = 0x0040,
	DUT_2_UVP_01 = 0x0080,
	DUT_1_OCP_02 = 0x0100,
	DUT_1_OCP_01 = 0x0200,
	DUT_1_OVP_01 = 0x0400,
	DUT_1_UVP_01 = 0x0800
} exp_enable_t;

/** @brief Device Under Test identifier */
typedef enum dut {
	DUT_1 = 0b01, //< DUT-1 (TI TPS25947)
	DUT_2 = 0b10, //< DUT-2 (ONS NIS6150)
	DUT_3 = 0b11  //< DUT-3 (KSat SOURCE LCL)
} dut_t;

/** @brief Experiment identifier */
typedef enum experiment {
	EXP_OVP     = 0b00, //< Over-Voltage Protection Experiment
	EXP_UVP     = 0b01, //< Under-Voltage Protection Experiment
	EXP_OCP_3_3 = 0b10, //< Over-Current Protection at 3.3V Experiment
	EXP_OCP_5_0 = 0b11  //< Over-Current Protection at 5.0V Experiment
} experiment_t;

/** @brief Load Simulator configuration */
typedef enum load_sim_config {
	LSC_2700_0 = 0b000, //<                R4 (2700.0 ohm)
	LSC_79_6   = 0b001, //<           R3 + R4 (79.6 ohm)
	LSC_38_4   = 0b010, //<      R2 +      R4 (38.4 ohm)
	LSC_26_2   = 0b011, //<      R2 + R3 + R4 (26.2 ohm)
	LSC_14_9   = 0b100, //< R1 +           R4 (14.9 ohm)
	LSC_12_6   = 0b101, //< R1 +      R3 + R4 (12.6 ohm)
	LSC_10_8   = 0b110, //< R1 + R2 +      R4 (10.8 ohm)
	LSC_9_4    = 0b111  //< R1 + R2 + R3 + R4 (9.4 ohm)
} load_sim_config_t;

/** @brief Data storage sections */
typedef enum storage_section {
	LAST = 1, //< Last data storage sections
	PREV = 2, //< Previous data storage sections (before last)
	PENU = 3  //< Penultimate data storage sections (before previous)
} storage_section_t;

#endif /* INC_DATA_TYPES_H_ */
