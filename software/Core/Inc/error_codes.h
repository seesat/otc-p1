/**
 * @file error_codes.h
 *
 * @brief Definition of system wide error codes
 *
 * @author Stefan Wertheimer
 * @date 2024-01-22
 */

#ifndef INC_ERROR_CODES_H_
#define INC_ERROR_CODES_H_

typedef enum error_code {
	OK,
	PARAMETER_ERROR,
	I2C_TX_ERROR,
	I2C_RX_ERROR,
	CAN_CONFIG_ERROR,
	CAN_HEAP_INIT_ERRROR,
	CAN_INVALID_TRANSFER_FRAME,
	CAN_DESERIALIZATION_ERROR,
	CAN_SERIALIZATION_ERROR,
	CAN_SEND_ERROR,
	TC_FAILURE,
	TC_BAD_COMMAND,
	TC_BAD_PARAMETER,
	TC_NOT_IMPLEMENTED,
	ADC_INIT_ERROR,
	ADC_ERROR
} error_code_t;

#endif /* INC_ERROR_CODES_H_ */
