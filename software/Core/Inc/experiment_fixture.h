/*
 * @file experiment_fixture.h
 *
 * @brief Fixture to run eFuse experiments
 *
 * @author Stefan Wertheimer
 * @date 2023-12-29
 */

#ifndef INC_EXPERIMENT_FIXTURE_H_
#define INC_EXPERIMENT_FIXTURE_H_

#include "stdbool.h"
#include "stdint.h"

#include "main.h"

#include "circular_buffer.h"
#include "config.h"
#include "mp28167.h"

#include "data_types.h"
#include "messages.h"


/** @brief Flag indicating whether or not experiment data shall be distributes as TM report */
static volatile _Bool experiment_tm_enabled;

/** @brief Experiment storage areas. */
typedef enum exp_storage
{
	SINGLE,
	SUITE,
	RUN
} exp_storage_t;

/**
 * @brief Initialize experiment execution.
 *
 * @param  hi2c  Handle to I2C bus to access sensors
 */
void experiment_fixture_init(I2C_HandleTypeDef hi2c, mp28167* ssc);

/**
 * @brief Free (delete) storage memory for experiment data.
 */
void experiment_fixture_free();

/**
 * @brief Run all configured experiments in a row.
 */
void experiment_fixture_start(void);

/**
 * @brief Run all configured experiments for a single time.
 */
void experiment_fixture_one_shot(void);

/**
 * @brief Get the current fault state of selected DUT.
 *
 * @param  dut  Identifier of DUT in question
 * @return True if DUT is in fault state, false otherwise.
 */
_Bool experiment_fixture_get_fault_state(dut_t dut);

/**
 * @brief Check whether or not an experiment is running.
 *
 * @return True if an experiment is running, false otherwise.
 */
_Bool experiment_fixture_is_running();

/**
 * @brief Get experiment data stored in circular buffer and send TM packets.
 *
 * @param  storage  Storage location to get experiment data from (single / suite / run).
 * @param  section  Storage section to get experiment data from (last / previous / penultimate).
 */
void experiment_fixture_get_exp_data(exp_storage_t storage, storage_section_t section);

/**
 * @brief Start a single experiement.
 *
 * @param  dut  DUT to run experiement on.
 * @param  exp  Experiment to execute on DUT.
 * @return True if experiment has been conducted, false otherwiese.
 */
_Bool experiment_fixture_start_single_experiment(dut_t dut, experiment_t exp);

#endif /* INC_EXPERIMENT_FIXTURE_H_ */
