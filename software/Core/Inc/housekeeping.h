/**
 * @file housekeeping.h
 *
 * @brief Gather data for housekeeping
 *
 * @author Stefan Wertheimer
 * @date 2024-07-17
 */

#ifndef INC_HOUSEKEEPING_H_
#define INC_HOUSEKEEPING_H_

#include "stdbool.h"
#include "stdint.h"

#include "adc.h"
#include "ambient_monitoring.h"
#include "data_types.h"
#include "experiment_fixture.h"
#include "timer.h"
#include "tm.h"

/** @brief Flag indicating whether or not housekeeping data shall be distributes as TM report */
static volatile _Bool housekeeping_tm_enabled;

/**
 * @brief Sets the boot time stamp.
 *
 * @param   ts   The boot time stamp
 */
void housekeeping_set_ts_boot(uint32_t ts);

/**
 * @brief Gathers required housekeeping data and sends a TM report.
 */
void housekeeping_prepare_report();

#endif /* INC_HOUSEKEEPING_H_ */
