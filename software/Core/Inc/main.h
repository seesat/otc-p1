/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED1_Pin GPIO_PIN_2
#define LED1_GPIO_Port GPIOE
#define LED2_Pin GPIO_PIN_3
#define LED2_GPIO_Port GPIOE
#define LED3_Pin GPIO_PIN_4
#define LED3_GPIO_Port GPIOE
#define LS1_SEL_Pin GPIO_PIN_0
#define LS1_SEL_GPIO_Port GPIOC
#define LS2_SEL_Pin GPIO_PIN_1
#define LS2_SEL_GPIO_Port GPIOC
#define LS3_SEL_Pin GPIO_PIN_2
#define LS3_SEL_GPIO_Port GPIOC
#define TEMP1_ALT_Pin GPIO_PIN_3
#define TEMP1_ALT_GPIO_Port GPIOC
#define DUT1_I_SENSE_Pin GPIO_PIN_0
#define DUT1_I_SENSE_GPIO_Port GPIOA
#define DUT2_I_SENE_Pin GPIO_PIN_1
#define DUT2_I_SENE_GPIO_Port GPIOA
#define DUT3_I_SENSE_Pin GPIO_PIN_2
#define DUT3_I_SENSE_GPIO_Port GPIOA
#define VCC_SENSE_Pin GPIO_PIN_4
#define VCC_SENSE_GPIO_Port GPIOA
#define DUT1_PG_Pin GPIO_PIN_5
#define DUT1_PG_GPIO_Port GPIOA
#define DUT2_FLAG_Pin GPIO_PIN_6
#define DUT2_FLAG_GPIO_Port GPIOA
#define TEMP2_ALT_Pin GPIO_PIN_4
#define TEMP2_ALT_GPIO_Port GPIOC
#define RAD_READ_Pin GPIO_PIN_5
#define RAD_READ_GPIO_Port GPIOC
#define RAD_I_SENSE_Pin GPIO_PIN_0
#define RAD_I_SENSE_GPIO_Port GPIOB
#define LS_I_SENSE_Pin GPIO_PIN_1
#define LS_I_SENSE_GPIO_Port GPIOB
#define DUT_V_OUT_SENSE_Pin GPIO_PIN_2
#define DUT_V_OUT_SENSE_GPIO_Port GPIOB
#define DUT2_SEL_Pin GPIO_PIN_10
#define DUT2_SEL_GPIO_Port GPIOB
#define DUT3_SEL_Pin GPIO_PIN_11
#define DUT3_SEL_GPIO_Port GPIOB
#define DUT1_EN_Pin GPIO_PIN_12
#define DUT1_EN_GPIO_Port GPIOB
#define DUT2_EN_Pin GPIO_PIN_13
#define DUT2_EN_GPIO_Port GPIOB
#define DUT3_EN_Pin GPIO_PIN_14
#define DUT3_EN_GPIO_Port GPIOB
#define RAD_OUT_Pin GPIO_PIN_15
#define RAD_OUT_GPIO_Port GPIOB
#define RAD_PG_Pin GPIO_PIN_6
#define RAD_PG_GPIO_Port GPIOC
#define RAD_R1_Pin GPIO_PIN_7
#define RAD_R1_GPIO_Port GPIOC
#define RAD_R2_Pin GPIO_PIN_8
#define RAD_R2_GPIO_Port GPIOC
#define SDA2_Pin GPIO_PIN_8
#define SDA2_GPIO_Port GPIOA
#define SCL2_Pin GPIO_PIN_9
#define SCL2_GPIO_Port GPIOA
#define CAN_RXD_Pin GPIO_PIN_11
#define CAN_RXD_GPIO_Port GPIOA
#define CAN_TXD_Pin GPIO_PIN_12
#define CAN_TXD_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define JTDI_Pin GPIO_PIN_15
#define JTDI_GPIO_Port GPIOA
#define SSC_EN_Pin GPIO_PIN_5
#define SSC_EN_GPIO_Port GPIOB
#define SCC_ALT_Pin GPIO_PIN_6
#define SCC_ALT_GPIO_Port GPIOB
#define DUT1_SEL_Pin GPIO_PIN_9
#define DUT1_SEL_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
