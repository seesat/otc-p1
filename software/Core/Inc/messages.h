/**
 * @file messages.h
 *
 * @brief Declaration of TC and TM messages
 *
 * @author Stefan Wertheimer
 * @date 2024-01-23
 */

#ifndef INC_MESSAGES_H_
#define INC_MESSAGES_H_

#include "stdint.h"

#include "constants.h"
#include "data_types.h"

/**
 * @brief Housekeeping data of MOBeFuse.
 */
typedef struct hk_data
{
	uint32_t ts_now;           //< Current timestamp in UTC
	uint32_t ts_boot;          //< Timestamp of system start in UTC
	uint16_t v_in;             //< Supply voltage of MOBeFuse
	uint16_t temp_uc;          //< MCU temperature
	uint16_t v_ref_int;        //< MCU reference voltage
	uint32_t v_ref_adc;        //< Reference voltage of ADC
	uint16_t rad0_v_last; 	   //< Last idle voltage read from dosimeter
	uint16_t rad0_i_last; 	   //< Last idle current read from dosimeter
	uint16_t rad1_v_last;      //< Last voltage of RADFET 1 read from dosimeter
	uint16_t rad1_i_last;      //< Last current of RADFET 1 read from dosimeter
	uint16_t rad2_v_last;      //< Last voltage of RADFET 2 read from dosimeter
	uint16_t rad2_i_last;      //< Last current of RADFET 2 read from dosimeter
	uint16_t rad12_v_last;     //< Last combined voltage of RADFETs 1 + 2 read from dosimeter
	uint16_t rad12_i_last;     //< Last combined current of RADFETs 1 + 2 read from dosimeter
	uint16_t temp_1_last;      //< Last temperature value read from sensor 1
	uint16_t temp_2_last;      //< Last temperature value read from sensor 2
	uint16_t dut_1_fault :  1; //< Flag indicating whether or not DUT 1 (TPS25947) is in fault mode
	uint16_t dut_2_fault :  1; //< Flag indicating whether or not DUT 2 (NIS5150) is in fault mode
	uint16_t dut_3_fault :  1; //< Flag indicating whether or not DUT 3 (SOURCE LCL) is in fault mode
	uint16_t experiment  :  1; //< Flag indicating whether or not an experiment is running
	uint16_t             : 12; //< Spare bits to align memory to complete bytes
} hk_data_t;

/** @brief Real size in bytes of housekeeping monitoring data (no alignment) */
static const uint16_t hk_data_real_size = 40;

/**
 * @brief Ambient Monitoring data
 */
typedef struct am_data
{
	uint32_t ts;      //< Current timestamp in UTC
	uint16_t rad0_v;  //< Current idle voltage read from dosimeter
	uint16_t rad0_i;  //< Current idle current read from dosimeter
	uint16_t rad1_v;  //< Current voltage of RADFET 1 read from dosimeter
	uint16_t rad1_i;  //< Current current of RADFET 1 read from dosimeter
	uint16_t rad2_v;  //< Current voltage of RADFET 2 read from dosimeter
	uint16_t rad2_i;  //< Current current of RADFET 2 read from dosimeter
	uint16_t rad12_v; //< Current combined voltage of RADFETs 1 + 2 read from dosimeter
	uint16_t rad12_i; //< Current combined current of RADFETs 1 + 2 read from dosimeter
	uint16_t temp_1;  //< Current temperature value read from sensor 1
	uint16_t temp_2;  //< Current temperature value read from sensor 2
} am_data_t;

/** @brief Real size in bytes of ambient monitoring data (no alignment) */
static const uint16_t am_data_real_size = 24;

/**
 * @brief Experiment data from a single fuse experiment
 */
typedef struct exp_data
{
	uint32_t          ts_start;         //< Current timestamp in UTC
	experiment_t      experiment;       //< Experiment ID
	dut_t             dut_select;       //< DUT selection
	load_sim_config_t load_config;      //< Load configuration
	uint8_t           rep_cnt;          //< Repetition counter
	_Bool             dut_fault;        //< Fault indicator of DUT
	uint16_t          dut_v_in;         //< Input voltage to DUT at tripping
	uint16_t          t_trip;			//< Tripping time of DUT (us)
	uint16_t          dut_v_out_trip;   //< Output voltage after tripping
	uint16_t          dut_i_sense_trip; //< Measured output current after tripping
} exp_data_t;

/** @brief Real size in bytes of single experiment data (no alignment) */
static const uint16_t exp_data_real_size = 17;


/**
 * @brief Complete data set of a full experiment run.
 */
typedef struct run_data
{
	exp_data_t dut_1_uvp_01[MAX_EXP_RUN_LENGTH]; //< Result data of all UVP01 experiment runs at DUT1
	exp_data_t dut_1_ovp_01[MAX_EXP_RUN_LENGTH]; //< Result data of all OVP01 experiment runs at DUT1
	exp_data_t dut_1_ocp_01[MAX_EXP_RUN_LENGTH]; //< Result data of all OCP01 experiment runs at DUT1
	exp_data_t dut_1_ocp_02[MAX_EXP_RUN_LENGTH]; //< Result data of all OCP02 experiment runs at DUT1
	exp_data_t dut_2_uvp_01[MAX_EXP_RUN_LENGTH]; //< Result data of all UVP01 experiment runs at DUT2
	exp_data_t dut_2_ovp_01[MAX_EXP_RUN_LENGTH]; //< Result data of all OVP01 experiment runs at DUT2
	exp_data_t dut_2_ocp_01[MAX_EXP_RUN_LENGTH]; //< Result data of all OCP01 experiment runs at DUT2
	exp_data_t dut_2_ocp_02[MAX_EXP_RUN_LENGTH]; //< Result data of all OCP02 experiment runs at DUT2
	exp_data_t dut_3_ocp_01[MAX_EXP_RUN_LENGTH]; //< Result data of all OCP01 experiment runs at DUT3
	exp_data_t dut_3_ocp_02[MAX_EXP_RUN_LENGTH]; //< Result data of all OCP02 experiment runs at DUT3
} run_data_t;

/**
 * @brief Get real size in bytes of full experiment run data (no alignment)
 *
 * @return Real size in bytes of full experiment run data (no alignment)
 */
uint16_t run_data_real_size();

/**
 * @brief Get real size in bytes of full experiment suite data (no alignment)
 *
 * @return Real size in bytes of full experiment suite data (no alignment)
 */
uint16_t suite_data_real_size();

/**
 * @brief Configuration data of MOBeFuse.
 */
typedef struct config_data
{
	uint32_t ts;                      //< Current timestamp in UTC
	uint32_t exp_interval;            //< Execution interval for experiment suites
	uint32_t am_interval;             //< Sampling interval for ambient monitoring data
	uint32_t hk_interval;             //< Sampling interval for housekeeping data
	uint32_t orbit_duration;          //< Orbit duration in seconds
	uint32_t number_of_orbits_for_am; //< Number of ambient measurements per orbit
	uint32_t number_of_exp_runs;      //< Number of storable complete experiment runs
	uint32_t number_of_single_exp;    //< Number of storable single experiments
	uint32_t number_of_exp_suites;    //< Number of storable experiment suites
	uint32_t exp_run_length;          //< Length of experiment runs
	uint32_t exp_enable;              //< Bit mask of enabled experiments
	uint32_t number_of_exp_enabled;   //< Number of enabled experiments
} config_data_t;

/** @brief Real size in bytes of configuration data (no alignment) */
static const uint16_t config_data_real_size = 48;

#endif /* INC_MESSAGES_H_ */
