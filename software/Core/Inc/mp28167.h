/**
 * @file mp28167.h
 *
 * @brief Data definitions regarding MP28167-A Integrated Buck-Boost Converter with I2C interface
 *
 * @author Marian Keller
 * @date 2024-04-28
 */

#ifndef INC_MP28167_H_
#define INC_MP28167_H_

#include "stdint.h"
#include <stdbool.h>
#include "stm32g4xx_hal.h"
#include "error_codes.h"

typedef struct {
	I2C_HandleTypeDef *i2c_handle;
	GPIO_TypeDef *gpio_x;
	uint16_t gpio_pin;
} mp28167;

// I2C Bus address
#define MP28167_ADDR 0x60

// I2C Registers
#define MP28167_REGISTER_VREF_L      0x00
#define MP28167_REGISTER_VREF_H      0x01
#define MP28167_REGISTER_VREF_GO     0x02
#define MP28167_REGISTER_IOUT_LIM    0x03
#define MP28167_REGISTER_CTL1        0x04
#define MP28167_REGISTER_CTL2        0x05
#define MP28167_REGISTER_Status      0x09
#define MP28167_REGISTER_INTERRUPT   0x0A
#define MP28167_REGISTER_MASK        0x0B
#define MP28167_REGISTER_ID1         0x0C
#define MP28167_REGISTER_MFR_ID      0x27
#define MP28167_REGISTER_DEV_ID      0x28
#define MP28167_REGISTER_IC_REV      0x29

// VREF_GO Go bit, set before changing VREF [bit 1]
#define MP28167_VREF_GO_GO_BIT       0x01
// VREF_GO PG delay bit, set to add 100 us delay to PG signal [bit 2]
#define MP28167_VREF_GO_PG_DELAY_BIT 0x02
// CTL1 EN bit, to turn the part on and off. When EN pin is high, the EN bit takes over. [bit 7]
#define MP28167_CTL1_EN_BIT 0x40
// CTL1 HICCUP_OCP_OVP bit, Over-current (OC) and over-voltage protection (OVP) mode selection. 1: Hiccup mode 0: Latch-off mode [bit 6]
#define MP28167_CTL1_HICCUP_OCP_OVP_BIT 0x20
// CTL1 DISCHG_EN bit, Output discharge enable bit [bit 5]
#define MP28167_CTL1_DISCHG_EN_BIT 0x10
// CTL1 MODE bit, Enable PFM/PWM mode bit [bit 4]
#define MP28167_CTL1_MODE_BIT 0x08


/**
 * @brief Turn MP28167 on or off
 * @param state true = on, false = off
 * @return OK on success, I2C_TX_ERROR or I2C_RX_ERROR otherwise
 */
void mp28167_set_enable(mp28167 *ref, bool state);


/**
 * @brief Set VREF voltage of MP28167 in mV from 0 to 1637
 * @param hi2c  Handle to I2C bus
 * @param vref  voltage in mV
 * @return OK on success, I2C_TX_ERROR or I2C_RX_ERROR otherwise
 */
enum error_code mp28167_set_vref(mp28167 *ref, uint16_t vref);

uint16_t mp28167_calculate_vref(float r1, float r2, float vout);


#endif /* INC_MP28167_H_ */
