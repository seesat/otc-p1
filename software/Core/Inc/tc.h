/**
 * @file tc.h
 *
 * @brief Handling of Telecommands (TC)
 *
 * @author Stefan Wertheimer
 * @date 2024-03-27
 */

#ifndef INC_TC_H_
#define INC_TC_H_

#include "stdbool.h"
#include "stddef.h"
#include "stdint.h"

#include "error_codes.h"
#include "mp28167.h"
#include "tc_ids.h"

/**
 * @brief  Initialize TC handler
 *
 * @param  hi2c  Handle to I2C
 * @param  ssc   Handle to SSC
 */
void tc_init(I2C_HandleTypeDef hi2c, mp28167* ssc);

/**
 * @brief  Execute a received TC if it is known.
 *
 * @param  command_id  Command identifier
 * @param  parameter   Byte array containing command parameters
 * @param  param_len   Length of cmmand parameter array
 *
 * @return OK in case of success, otherwise TC_FAILURE, TC_BAD_COMMAND or TC_BAD_PARAMETER.
 */
error_code_t tc_execute(uint16_t command_id, uint8_t* parameter, size_t param_len);

#endif /* INC_TC_H_ */
