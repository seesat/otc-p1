/**
 * @file tc_ids.h
 *
 * @brief Definition of Telecommand (TC) identifiers
 *
 * @author Stefan Wertheimer
 * @date 2024-03-29
 */

#ifndef INC_TC_IDS_H_
#define INC_TC_IDS_H_

/** Identifier for all SeeSat MOBeFuse TCs */
#define MOBEFUSE_TC_ID 0x4D00

/** Enumeration of all allowed TC identifiers. */
typedef enum tc_id {
	TC_ID_START_EXP_RUN        = MOBEFUSE_TC_ID | 0x0001, //< Start experiment run (TC)
	TC_ID_ENABLE_HK_REPORT     = MOBEFUSE_TC_ID | 0x0002, //< Enable cyclic report of housekeeping data (TC)
	TC_ID_DISABLE_HK_REPORT    = MOBEFUSE_TC_ID | 0x0003, //< Disable cyclic report of housekeeping data (TC)
	TC_ID_ENABLE_AM_REPORT     = MOBEFUSE_TC_ID | 0x0004, //< Enable cyclic report of ambient monitoring data (TC)
	TC_ID_DISABLE_AM_REPORT    = MOBEFUSE_TC_ID | 0x0005, //< Disable cyclic report of ambient monitoring data (TC)
	TC_ID_DUMP_AM_DATA_LAST    = MOBEFUSE_TC_ID | 0x0006, //< Dump ambient monitoring data from last orbit (TC)
	TC_ID_DUMP_AM_DATA_PREV    = MOBEFUSE_TC_ID | 0x0007, //< Dump ambient monitoring data from previous orbit (TC)
	TC_ID_DUMP_AM_DATA_PENU    = MOBEFUSE_TC_ID | 0x0008, //< Dump ambient monitoring data from penultimate orbit (TC)
	TC_ID_DUMP_EXP_DATA_LAST   = MOBEFUSE_TC_ID | 0x0009, //< Dump experiment data from last run (TC)
	TC_ID_DUMP_EXP_DATA_PREV   = MOBEFUSE_TC_ID | 0x000A, //< Dump experiment data from previous run (TC)
	TC_ID_DUMP_EXP_DATA_PENU   = MOBEFUSE_TC_ID | 0x000B, //< Dump experiment data from penultimate run (TC)
	TC_ID_DUMP_DETAILS         = MOBEFUSE_TC_ID | 0x000C, //< Dump detailed experiment data from last run (TC)
	TC_ID_EXE_SINGLE_TEST      = MOBEFUSE_TC_ID | 0x000D, //< Execute a single test case (parameter contains test case ID) (TC)
	TC_ID_SET_CONFIG_PARAM     = MOBEFUSE_TC_ID | 0x000E, //< Set value of single configuration parameter (TC)
	TC_ID_GET_ALL_CONFIG       = MOBEFUSE_TC_ID | 0x000F, //< Get values of all configuration parameters (TC)
	TC_ID_DUMP_SUITE_DATA_LAST = MOBEFUSE_TC_ID | 0x0010, //< Dump experiment data from last suite (TC)
	TC_ID_DUMP_SUITE_DATA_PREV = MOBEFUSE_TC_ID | 0x0011, //< Dump experiment data from previous suite (TC)
	TC_ID_DUMP_SUITE_DATA_PENU = MOBEFUSE_TC_ID | 0x0012, //< Dump experiment data from penultimate suite (TC)
	TC_ID_DUMP_RUN_DATA_LAST   = MOBEFUSE_TC_ID | 0x0013, //< Dump experiment data from last run (TC)
	TC_ID_DUMP_RUN_DATA_PREV   = MOBEFUSE_TC_ID | 0x0014, //< Dump experiment data from previous run (TC)
	TC_ID_DUMP_RUN_DATA_PENU   = MOBEFUSE_TC_ID | 0x0015, //< Dump experiment data from penultimate run (TC)
	TC_ID_ENABLE_EXP_REPORT    = MOBEFUSE_TC_ID | 0x0016, //< Enable report of experiment data on occurrence (TC)
	TC_ID_DISABLE_EXP_REPORT   = MOBEFUSE_TC_ID | 0x0017, //< Disable report of experiment data on occurrence (TC)
	TC_ID_REINIT               = MOBEFUSE_TC_ID | 0x00FF, //< Re-initialize software (TC)
	TC_ID_MAX //< maximum TC ID
} tc_id_t;

/** Total number of defined TC IDs */
#define TC_ID_NUM 24

#endif /* INC_TC_IDS_H_ */
