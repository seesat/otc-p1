/**
 * @file timer.h
 *
 * @brief Additional timer functions.
 *
 * @author Stefan Wertheimer
 * @date 2024-02-25
 */

#ifndef INC_TIMER_H_
#define INC_TIMER_H_

#include "stdint.h"

#include "stm32g4xx_hal.h"

/** UTC timestamp at last 1-PPS signal */
extern uint32_t utc_ts_pps;

/**
 * @brief  Initialize timer helper
 */
void timer_init(TIM_HandleTypeDef* htim);

/**
 * @brief  Get a microsecond timer tick.
 *
 * @return 64-bit microsecond count
 *
 * @note   1us timer 'timer_us' must be set before usage!
 */
uint64_t get_micros();

/**
 * @brief  Save current micros
 */
void save_micros();

/**
 * @brief  Get last saved microseconds
 * @return 64-bit microsecond count
 */
uint64_t get_last_micros();

/**
 * @brief  Register microsecond timer overflow.
 */
void timer_us_overflow();

#endif /* INC_TIMER_H_ */
