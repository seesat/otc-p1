/**
 * @file tm.h
 *
 * @brief Handling of Telemetry (TM)
 *
 * @author Stefan Wertheimer
 * @date 2024-03-27
 */

#ifndef INC_TM_H_
#define INC_TM_H_

#include "can.h"
#include "error_codes.h"
#include "messages.h"

#include "seesat/mobefuse/AmbientMonitoringReport_1_0.h"
#include "seesat/mobefuse/ConfigurationParameterReport_1_0.h"
#include "seesat/mobefuse/HousekeepingReport_1_0.h"
#include "seesat/mobefuse/MOBeFuseTM_1_0.h"
#include "seesat/mobefuse/SingleExperimentReport_1_0.h"

/**
 * @brief Send a single housekeeping data report.
 *
 * @param  data  Housekeeping data to pack into the report.
 * @return OK on success.
 */
enum error_code tm_send_hk_report(hk_data_t data);

/**
 * @brief Send a single ambient monitoring data report.
 *
 * @param  data  Ambient monitoring data to pack into the report.
 * @return OK on success.
 */
enum error_code tm_send_am_report(am_data_t data);

/**
 * @brief Send a single experiment data report.
 *
 * @param  data  Experiment data to pack into the report.
 * @return OK on success.
 */
enum error_code tm_send_exp_report(exp_data_t data);

/**
 * @brief Send a configuration data report.
 *
 * @param  data  Configuration data to pack into the report.
 * @return OK on success.
 */
enum error_code tm_send_config_report(config_data_t data);


#endif /* INC_TM_H_ */
