/**
 * @file tmp75bq1_config.h
 *
 * @brief Data definitions regarding TMP75B-Q1 temperature sensor.
 *
 * @author Stefan Wertheimer
 * @date 2024-01-20
 */

#ifndef INC_TMP75BQ1_CONFIG_H_
#define INC_TMP75BQ1_CONFIG_H_

#include "stdint.h"

#include "stm32g4xx_hal.h"

#include "error_codes.h"

#define TMP75BQ1_REGISTER_TEMP 0
#define TMP75BQ1_REGISTER_CONFIG 1
#define TMP75BQ1_REGISTER_T_LOW 2
#define TMP75BQ1_REGISTER_T_HIGH 3

// Temperature result [bits 15:4]
#define TMP75BQ1_TEMP_MASK 0xFFF0
// Temperature result (resolution 1degC) [bits 15:8]
#define TMP75BQ1_TEMP_MSB_MASK 0xFF00
// Temperature result (resolution of 0.0625degC) [bits 7:4]
#define TMP75BQ1_TEMP_LSB_MASK 0x00F0

// One-shot mode [bit 15]
#define TMP75BQ1_CONFIG_OS_MASK   0x8000
#define TMP75BQ1_CONFIG_OS_RESET  0x7FFF
// Conversion rate control [bits 14:13]
#define TMP75BQ1_CONFIG_CR_MASK   0x6000
#define TMP75BQ1_CONFIG_CR_RESET  0x9FFF
// Fault queue to trigger the ALERT pin [bits 12:11]
#define TMP75BQ1_CONFIG_FQ_MASK   0x1800
#define TMP75BQ1_CONFIG_FQ_RESET  0xE7FF
// ALERT polarity control [bit 10]
#define TMP75BQ1_CONFIG_POL_MASK  0x0400
#define TMP75BQ1_CONFIG_POL_RESET 0xFBFF
// ALERT thermostat mode control [bit 9]
#define TMP75BQ1_CONFIG_TM_MASK   0x0200
#define TMP75BQ1_CONFIG_TM_RESET  0xFDFF
// Shutdown control bit [bit 8]
#define TMP75BQ1_CONFIG_SD_MASK   0x0100
#define TMP75BQ1_CONFIG_SD_RESET  0xFEFF

// Temperature low limit [bits 15:4]
#define TMP75BQ1_T_LOW_MASK 0xFFF0
// Temperature low limit (resolution 1degC) [bits 15:8]
#define TMP75BQ1_T_LOW_MSB_MASK 0xFF00
// Temperature low limit (resolution of 0.0625degC) [bits 7:4]
#define TMP75BQ1_T_LOW_LSB_MASK 0x00F0

// Temperature high limit [bits 15:4]
#define TMP75BQ1_T_HIGH_MASK 0xFFF0
// Temperature high limit (resolution 1degC) [bits 15:8]
#define TMP75BQ1_T_HIGH_MSB_MASK 0xFF00
// Temperature high limit (resolution of 0.0625degC) [bits 7:4]
#define TMP75BQ1_T_HIGH_LSB_MASK 0x00F0

#define TMP75BQ1_CONFIG_OS_ON 0x80 // 0b10000000

/** Conversion Rates for TMP75B-Q1 */
enum tmp75bq1_cr
{
	/** 37 Hz Conversion Rate */
	cr_37Hz = 0x00, // 0b00000000
	/** 18 Hz Conversion Rate */
	cr_18Hz = 0x20, // 0b00100000
	/** 9 Hz Conversion Rate */
	cr_9Hz  = 0x40, // 0b01000000
	/** 4 Hz Conversion Rate */
	cr_4Hz  = 0x60  // 0b01100000
};

/** Fault Queue values for TMP75B-Q1 */
enum tmp75bq1_fq
{
	/** Fault Queue length 1 */
	fq_1 = 0x00, // 0b00000000
	/** Fault Queue length 2 */
	fq_2 = 0x08, // 0b00001000
	/** Fault Queue length 4 */
	fq_4 = 0x10, // 0b00010000
	/** Fault Queue length 6 */
	fq_6 = 0x18  // 0b00011000
};

/** Alert Polarities for TMP75B-Q1 */
enum tmp75bq1_pol
{
	/** Low Polarity */
	pol_low  = 0x00, // 0b00000000
	/** High Polarity */
	pol_high = 0x04  // 0b00000100
};

/** Alert Modes for TMP75B-Q1 */
enum tmp75bq1_tm
{
	/** Comparator Mode */
	tm_comp = 0x00, // 0b00000000
	/** Interrupt Mode */
	tm_intr = 0x02  // 0b00000000
};

/** Operation Modes for TMP75B-Q1 */
enum tmp75bq1_sd
{
	/** Continuous Conversion Mode */
	sd_cc = 0x00, // 0b00000000
	/** Shutdown Mode */
	sd_sd = 0x01  // 0b00000001
};

/**
 * @brief Read configuration register of TMP75B-Q1.
 *
 * @param      hi2c     Handle to I2C bus
 * @param      address  I2C address of target TMP75B-Q1
 * @param[out] config   Buffer for receiving the configuration data
 * @return OK on success, I2C_TX_ERROR or I2C_RX_ERROR otherwise
 */
enum error_code tmp75bq1_read_config(I2C_HandleTypeDef hi2c, const uint8_t address, uint16_t* config);

/**
 * @brief Read temperature register of TMP75B-Q1 and returns 12 bit raw value.
 *
 * @param      hi2c         Handle to I2C bus
 * @param      address      I2C address of target TMP75B-Q1
 * @param[out] temperature  Variable for temperature data
 * @return OK on success, I2C_TX_ERROR or I2C_RX_ERROR otherwise
 */
enum error_code tmp75bq1_read_temperature_raw(I2C_HandleTypeDef hi2c, const uint8_t address, uint16_t* temperature);

/**
 * @brief Read temperature register of TMP75B-Q1 and returns calibrated value.
 *
 * @param      hi2c         Handle to I2C bus
 * @param      address      I2C address of target TMP75B-Q1
 * @param[out] temperature  Variable for temperature data
 * @return OK on success, I2C_TX_ERROR or I2C_RX_ERROR otherwise
 */
enum error_code tmp75bq1_read_temperature(I2C_HandleTypeDef hi2c, const uint8_t address, float* temperature);

/**
 * @brief Set low temperature limit register of TMP75B-Q1.
 *
 * @param hi2c     Handle to I2C bus
 * @param address  I2C address of target TMP75B-Q1
 * @param limit    Low limit to set
 * @return OK on success, I2C_TX_ERROR or I2C_RX_ERROR otherwise
 */
enum error_code tmp75bq1_set_low_limit(I2C_HandleTypeDef hi2c, const uint8_t address, const uint16_t limit);

/**
 * @brief Set high temperature limit register of TMP75B-Q1.
 *
 * @param hi2c     Handle to I2C bus
 * @param address  I2C address of target TMP75B-Q1
 * @param limit    High limit to set
 * @return OK on success, I2C_TX_ERROR or I2C_RX_ERROR otherwise
 */
enum error_code tmp75bq1_set_high_limit(I2C_HandleTypeDef hi2c, const uint8_t address, const uint16_t limit);

/**
 * @brief Set one-shot-mode in configuration register of TMP75B-Q1.
 *
 * @param hi2c     Handle to I2C bus
 * @param address  I2C address of target TMP75B-Q1
 * @return OK on success, I2C_TX_ERROR or I2C_RX_ERROR otherwise
 */
enum error_code tmp75bq1_set_one_shot_mode(I2C_HandleTypeDef hi2c, const uint8_t address);

/**
 * @brief Set conversion rate in configuration register of TMP75B-Q1.
 *
 * @param hi2c     Handle to I2C bus
 * @param address  I2C address of target TMP75B-Q1
 * @param cr       Conversion rate to set
 * @return OK on success, I2C_TX_ERROR or I2C_RX_ERROR otherwise
 */
enum error_code tmp75bq1_set_conversion_rate(I2C_HandleTypeDef hi2c, const uint8_t address, enum tmp75bq1_cr cr);

/**
 * @brief Set fault queue value in configuration register of TMP75B-Q1.
 *
 * @param hi2c     Handle to I2C bus
 * @param address  I2C address of target TMP75B-Q1
 * @param fq       Fault queue value to set
 * @return OK on success, I2C_TX_ERROR or I2C_RX_ERROR otherwise
 */
enum error_code tmp75bq1_set_fault_queue(I2C_HandleTypeDef hi2c, const uint8_t address, enum tmp75bq1_fq fq);

/**
 * @brief Set alert polarity in configuration register of TMP75B-Q1.
 *
 * @param hi2c     Handle to I2C bus
 * @param address  I2C address of target TMP75B-Q1
 * @param pol      Polarity to set
 * @return OK on success, I2C_TX_ERROR or I2C_RX_ERROR otherwise
 */
enum error_code tmp75bq1_set_alert_polarity(I2C_HandleTypeDef hi2c, const uint8_t address, enum tmp75bq1_pol pol);

/**
 * @brief Set alert mode in configuration register of TMP75B-Q1.
 *
 * @param hi2c     Handle to I2C bus
 * @param address  I2C address of target TMP75B-Q1
 * @param tm       Alert mode to set
 * @return OK on success, I2C_TX_ERROR or I2C_RX_ERROR otherwise
 */
enum error_code tmp75bq1_set_alert_mode(I2C_HandleTypeDef hi2c, const uint8_t address, enum tmp75bq1_tm tm);

/**
 * @brief Set operation mode in configuration register of TMP75B-Q1.
 *
 * @param hi2c     Handle to I2C bus
 * @param address  I2C address of target TMP75B-Q1
 * @param sd       Operation mode to set
 * @return OK on success, I2C_TX_ERROR or I2C_RX_ERROR otherwise
 */
enum error_code tmp75bq1_set_shutdown_mode(I2C_HandleTypeDef hi2c, const uint8_t address, enum tmp75bq1_sd);

#endif /* INC_TMP75BQ1_CONFIG_H_ */
