/**
 * @file adc.c
 *
 * @brief Control functions for Analog-Digial-Converter
 *
 * @author Stefan Wertheimer
 * @date 2024-06-14
 */

#include "adc.h"

/** Handle for ADC1 */
ADC_HandleTypeDef hadc_1;

/** Handle for ADC2 */
ADC_HandleTypeDef hadc_2;

extern COMP_HandleTypeDef hcomp3;

/** Data buffer for ADC1 */
uint32_t value_adc1[ADC_1_CHANNELS];

/** Data buffer for ADC2 */
uint32_t value_adc2[ADC_2_CHANNELS];

/* ------------------------------------------------------------------------ */
void adc_init(ADC_HandleTypeDef adc1, ADC_HandleTypeDef adc2)
{
	hadc_1 = adc1;
	hadc_2 = adc2;
}

/* ------------------------------------------------------------------------ */
error_code_t adc_init_all_slow(adc_t adc_num)
{
	error_code_t status = OK;
	ADC_MultiModeTypeDef multimode = {0};
	ADC_ChannelConfTypeDef sConfig = {0};
	ADC_HandleTypeDef adc;


	if (ADC_1 == adc_num)
	{
		adc = hadc_1;

		adc.Init.ScanConvMode = ADC_SCAN_ENABLE;
		adc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
		adc.Init.ContinuousConvMode = ENABLE;
		adc.Init.NbrOfConversion = 6;
		adc.Init.DiscontinuousConvMode = DISABLE;
		adc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
		adc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
		adc.Init.DMAContinuousRequests = DISABLE;
		adc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
		adc.Init.OversamplingMode = ENABLE;
		adc.Init.Oversampling.Ratio = ADC_OVERSAMPLING_RATIO_256;
		adc.Init.Oversampling.RightBitShift = ADC_RIGHTBITSHIFT_8;

		if (HAL_ADC_Init(&adc) != HAL_OK)
		{
			status = ADC_INIT_ERROR;
		}

		multimode.Mode = ADC_MODE_INDEPENDENT;
		if (HAL_ADCEx_MultiModeConfigChannel(&adc, &multimode) != HAL_OK)
		{
			status = ADC_INIT_ERROR;
		}
		sConfig.Channel = ADC_CHANNEL_1;
		sConfig.Rank = ADC_REGULAR_RANK_1;
		sConfig.SamplingTime = ADC_SAMPLETIME_247CYCLES_5;
		sConfig.SingleDiff = ADC_SINGLE_ENDED;
		sConfig.OffsetNumber = ADC_OFFSET_NONE;
		sConfig.Offset = 0;
		if (HAL_ADC_ConfigChannel(&adc, &sConfig) != HAL_OK)
		{
			status = ADC_INIT_ERROR;
		}

		sConfig.Channel = ADC_CHANNEL_2;
		sConfig.Rank = ADC_REGULAR_RANK_2;
		if (HAL_ADC_ConfigChannel(&adc, &sConfig) != HAL_OK)
		{
			status = ADC_INIT_ERROR;
		}

		sConfig.Channel = ADC_CHANNEL_3;
		sConfig.Rank = ADC_REGULAR_RANK_3;
		if (HAL_ADC_ConfigChannel(&adc, &sConfig) != HAL_OK)
		{
			status = ADC_INIT_ERROR;
		}

		sConfig.Channel = ADC_CHANNEL_12;
		sConfig.Rank = ADC_REGULAR_RANK_4;
		if (HAL_ADC_ConfigChannel(&adc, &sConfig) != HAL_OK)
		{
			status = ADC_INIT_ERROR;
		}

		sConfig.Channel = ADC_CHANNEL_15;
		sConfig.Rank = ADC_REGULAR_RANK_5;
		if (HAL_ADC_ConfigChannel(&adc, &sConfig) != HAL_OK)
		{
			status = ADC_INIT_ERROR;
		}

		sConfig.Channel = ADC_CHANNEL_VREFINT;
		sConfig.Rank = ADC_REGULAR_RANK_6;
		if (HAL_ADC_ConfigChannel(&adc, &sConfig) != HAL_OK)
		{
			status = ADC_INIT_ERROR;
		}
	} else if (ADC_2 == adc_num){
		adc = hadc_2;

		adc.Init.ScanConvMode = ADC_SCAN_ENABLE;
		adc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
		adc.Init.ContinuousConvMode = ENABLE;
		adc.Init.NbrOfConversion = 3;
		adc.Init.DiscontinuousConvMode = DISABLE;
		adc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
		adc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
		adc.Init.DMAContinuousRequests = DISABLE;
		adc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
		adc.Init.OversamplingMode = ENABLE;
		adc.Init.Oversampling.Ratio = ADC_OVERSAMPLING_RATIO_256;
		adc.Init.Oversampling.RightBitShift = ADC_RIGHTBITSHIFT_8;

		sConfig.Channel = ADC_CHANNEL_12;
		sConfig.Rank = ADC_REGULAR_RANK_1;
		sConfig.SamplingTime = ADC_SAMPLETIME_247CYCLES_5;
		sConfig.SingleDiff = ADC_SINGLE_ENDED;
		sConfig.OffsetNumber = ADC_OFFSET_NONE;
		sConfig.Offset = 0;
		if (HAL_ADC_ConfigChannel(&adc, &sConfig) != HAL_OK)
		{
			status = ADC_INIT_ERROR;
		}

		sConfig.Channel = ADC_CHANNEL_15;
		sConfig.Rank = ADC_REGULAR_RANK_2;
		if (HAL_ADC_ConfigChannel(&adc, &sConfig) != HAL_OK)
		{
			status = ADC_INIT_ERROR;
		}

		/** Configure Regular Channel
		 */
		sConfig.Channel = ADC_CHANNEL_17;
		sConfig.Rank = ADC_REGULAR_RANK_3;
		if (HAL_ADC_ConfigChannel(&adc, &sConfig) != HAL_OK)
		{
			status = ADC_INIT_ERROR;
		}
	} else {
		status = PARAMETER_ERROR;
	}

	return status;
}

/* ------------------------------------------------------------------------ */
error_code_t adc_init_one_slow(adc_t adc_num, uint32_t adc_channel)
{
	error_code_t status = OK;
	ADC_ChannelConfTypeDef sConfig = {0};
	ADC_HandleTypeDef adc;

	if (ADC_1 == adc_num)
	{
		adc = hadc_1;
	} else if (ADC_2 == adc_num) {
		adc = hadc_2;
	} else {
		return PARAMETER_ERROR;
	}

	adc.Init.ScanConvMode = ADC_SCAN_DISABLE;
	adc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	adc.Init.ContinuousConvMode = ENABLE;
	adc.Init.NbrOfConversion = 1;
	adc.Init.DiscontinuousConvMode = DISABLE;
	adc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	adc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	adc.Init.DMAContinuousRequests = DISABLE;
	adc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
	adc.Init.OversamplingMode = ENABLE;
	adc.Init.Oversampling.Ratio = ADC_OVERSAMPLING_RATIO_256;
	adc.Init.Oversampling.RightBitShift = ADC_RIGHTBITSHIFT_8;

	if (HAL_ADC_Init(&adc) != HAL_OK)
	{
		status = ADC_INIT_ERROR;
	}

	sConfig.Channel = adc_channel;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLETIME_247CYCLES_5;
	sConfig.SingleDiff = ADC_SINGLE_ENDED;
	sConfig.OffsetNumber = ADC_OFFSET_NONE;
	sConfig.Offset = 0;
	if (HAL_ADC_ConfigChannel(&adc, &sConfig) != HAL_OK)
	{
		status = ADC_INIT_ERROR;
	}
	return status;
}

/* ------------------------------------------------------------------------ */
error_code_t adc_init_one_fast(adc_t adc_num, uint32_t adc_channel){
	error_code_t status = OK;
		ADC_MultiModeTypeDef multimode = {0};
		ADC_ChannelConfTypeDef sConfig = {0};
		ADC_HandleTypeDef adc;

		if (ADC_1 == adc_num)
		{
			adc = hadc_1;
		} else if (ADC_2 == adc_num) {
			adc = hadc_2;
		} else {
			return PARAMETER_ERROR;
		}

		adc.Init.ScanConvMode = ADC_SCAN_DISABLE;
		adc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
		adc.Init.ContinuousConvMode = ENABLE;
		adc.Init.NbrOfConversion = 1;
		adc.Init.DiscontinuousConvMode = DISABLE;
		adc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
		adc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
		adc.Init.DMAContinuousRequests = DISABLE;
		adc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
		adc.Init.OversamplingMode = DISABLE;
		adc.Init.Oversampling.RightBitShift = ADC_RIGHTBITSHIFT_NONE;

		if (HAL_ADC_Init(&adc) != HAL_OK)
		{
			status = ADC_INIT_ERROR;
		}

		multimode.Mode = ADC_MODE_INDEPENDENT;
		if (HAL_ADCEx_MultiModeConfigChannel(&adc, &multimode) != HAL_OK)
		{
			status = ADC_INIT_ERROR;
		}
		sConfig.Channel = adc_channel;
		sConfig.Rank = ADC_REGULAR_RANK_1;
		sConfig.SamplingTime = ADC_SAMPLETIME_92CYCLES_5;
		sConfig.SingleDiff = ADC_SINGLE_ENDED;
		sConfig.OffsetNumber = ADC_OFFSET_NONE;
		sConfig.Offset = 0;
		if (HAL_ADC_ConfigChannel(&adc, &sConfig) != HAL_OK)
		{
			status = ADC_INIT_ERROR;
		}
		return status;
}

/* ------------------------------------------------------------------------ */
error_code_t adc_start_dma(adc_t adc)
{
	error_code_t rc = OK;

	if (adc == ADC_1)
	{
		HAL_ADC_Start_DMA(&hadc_1, value_adc1, ADC_1_CHANNELS);
	}
	else if (adc == ADC_2)
	{
		HAL_ADC_Start_DMA(&hadc_2, value_adc2, ADC_2_CHANNELS);
	}
	else
	{
		rc = PARAMETER_ERROR;
	}

	return rc;
}

/* ------------------------------------------------------------------------ */
error_code_t adc_start_single_dma(adc_t adc)
{
	error_code_t rc = OK;

	if (adc == ADC_1)
	{
		HAL_ADC_Start_DMA(&hadc_1, value_adc1, 1);
	}
	else if (adc == ADC_2)
	{
		HAL_ADC_Start_DMA(&hadc_2, value_adc2, 1);
	}
	else
	{
		rc = PARAMETER_ERROR;
	}

	return rc;
}

/* ------------------------------------------------------------------------ */
error_code_t adc_stop_dma(adc_t adc)
{
	error_code_t rc = OK;

	if (adc == ADC_1)
	{
		HAL_ADC_Stop_DMA(&hadc_1);
	}
	else if (adc == ADC_2)
	{
		HAL_ADC_Stop_DMA(&hadc_2);
	}
	else
	{
		rc = PARAMETER_ERROR;
	}

	return rc;
}

/* ------------------------------------------------------------------------ */
uint32_t adc_get_value_dma(adc_param_t param)
{
	uint32_t value;

	if (param < OFFSET_ADC_2) // ADC1
	{
		value = value_adc1[param];
	}
	else // ADC2
	{
		value = value_adc2[param - OFFSET_ADC_2];
	}

	return value;
}

/* ------------------------------------------------------------------------ */
uint32_t adc_get_single_value_dma(adc_t adc)
{
	uint32_t value;

	if (ADC_1 == adc) 		// ADC1
	{
		value = value_adc1[0];
	}
	else if (ADC_2 == adc) // ADC2
	{
		value = value_adc2[0];
	}

	return value;
}


/* ------------------------------------------------------------------------ */
uint32_t adc_get_vref()
{
	return adc_get_value_dma(VREF);
}

/* ------------------------------------------------------------------------ */
error_code_t adc_calibrate(adc_t adc)
{
	error_code_t rc = OK;
	//*vref = 0;

	ADC_HandleTypeDef *hadc;
	if (adc == ADC_1)
	{
		hadc = &hadc_1;
	}
	else if (adc == ADC_2)
	{
		hadc = &hadc_2;
	}
	else
	{
		rc = PARAMETER_ERROR;
	}

	if (rc == OK)
	{
		HAL_ADCEx_Calibration_Start(hadc, ADC_SINGLE_ENDED);
		//*vref = adc_get_vref();
	}

	return rc;
}

/* ------------------------------------------------------------------------ */
void comp3_init() {
	hcomp3.Instance = COMP3;
	hcomp3.Init.InputPlus = COMP_INPUT_PLUS_IO1;
	hcomp3.Init.InputMinus = COMP_INPUT_MINUS_1_2VREFINT; // :TODO: use DAC
	hcomp3.Init.OutputPol = COMP_OUTPUTPOL_NONINVERTED;
	hcomp3.Init.Hysteresis = COMP_HYSTERESIS_10MV;
	hcomp3.Init.BlankingSrce = COMP_BLANKINGSRC_NONE;
	hcomp3.Init.TriggerMode = COMP_TRIGGERMODE_IT_FALLING;
	if (HAL_COMP_Init(&hcomp3) != HAL_OK)
	{
		Error_Handler();
	}
	hcomp3.Instance->CSR |= 1 << 1; // Set reserved bit #1
	NVIC_EnableIRQ(COMP1_2_3_IRQn);
	HAL_NVIC_EnableIRQ(COMP1_2_3_IRQn);
	HAL_NVIC_SetPriority(COMP1_2_3_IRQn, 0, 1);
}

void comp3_start() {
	HAL_COMP_Start(&hcomp3);
}
