/**
 * @file ambient_monitoring.c
 *
 * @brief Ambient monitoring (radiation & temperature)
 *
 * @author Stefan Wertheimer
 * @date 2023-12-24
 */

#include "ambient_monitoring.h"


/* ##### PUBLIC VARIABLES ################################################ */
static volatile _Bool ambient_monitoring_tm_enabled = true;

/* ##### PRIVATE VARIABLES ################################################ */

I2C_HandleTypeDef am_hi2c;

mp28167* am_ssc;


uint8_t* am_buffer;

cb_handle_t am_circular_buffer;

am_data_t current_am_data;

/**
 * @brief Enumeration of RADFET sensors.
 */
typedef enum radfet {
	R_0   = 0x00,
	R_1   = 0x01,
	R_2   = 0x10,
	R_12  = 0x11
} radfet_t;

/* ##### PRIVATE FUNCTIONS ################################################ */

/**
 * @brief Read out RADFETs.
 *
 * @param  r     RADFET to read out [IN]
 * @param  rad_v Voltage of RADFET  [OUT]
 * @param  rad_i Current of RADFET  [OUT]
 * @return OK or an error code.
 */
error_code_t radfet_read(radfet_t r, uint16_t* rad_v, uint16_t* rad_i)
{
	// enable RAD_R1 and/or RAD_R2
	HAL_GPIO_WritePin(RAD_R1_GPIO_Port, RAD_R1_Pin, (r & R_1));
	HAL_GPIO_WritePin(RAD_R2_GPIO_Port, RAD_R2_Pin, (r & R_2));

	HAL_Delay(5);

	// read RAD_OUT_SENSE and RAD_I_SENSE
	adc_init_one_slow(ADC_RAD_OUT_SENSE, ADC_RAD_OUT_SENSE_CHANNEL);
	adc_init_one_slow(ADC_RAD_I_SENSE, ADC_RAD_I_SENSE_CHANNEL);
	adc_start_dma(ADC_RAD_OUT_SENSE);
	adc_start_dma(ADC_RAD_I_SENSE);
    HAL_Delay(10);
	*rad_v = adc_get_single_value_dma(ADC_RAD_OUT_SENSE);
	*rad_i = adc_get_single_value_dma(ADC_RAD_I_SENSE);

	// disable RAD_R1 and_RAD_R2
	HAL_GPIO_WritePin(RAD_R1_GPIO_Port, RAD_R1_Pin, 0);
	HAL_GPIO_WritePin(RAD_R2_GPIO_Port, RAD_R2_Pin, 0);

	return OK;
}

/**
 * @brief Read current Total Ionization Dose (TID) from RADFET.
 *
 * @param  rad0_v  Voltage without RADFET [OUT]
 * @param  rad0_i  Current without RADFET [OUT]
 * @param  rad1_v  Voltage of RADFET 2 [OUT]
 * @param  rad1_i  Current of RADFET 2 [OUT]
 * @param  rad2_v  Voltage of RADFET 2 [OUT]
 * @param  rad2_i  Current of RADFET 2 [OUT]
 * @param  rad12_v Combined voltage of RADFET 1 + 2 [OUT]
 * @param  rad12_i Combined current of RADFET 1 + 2 [OUT]
 * @return OK or an error code.
 */
error_code_t radfet_get_tid(uint16_t* rad0_v, uint16_t* rad0_i, uint16_t* rad1_v, uint16_t* rad1_i, uint16_t* rad2_v, uint16_t* rad2_i, uint16_t* rad12_v, uint16_t* rad12_i)
{
	error_code_t ret_val = OK;
	*rad0_v  = 0, *rad0_i  = 0;
	*rad1_v  = 0, *rad1_i  = 0;
	*rad2_v  = 0, *rad2_i  = 0;
	*rad12_v = 0, *rad12_i = 0;

	// set VCC to 12V
	mp28167_set_enable(am_ssc, true);
	uint16_t new_vref = mp28167_calculate_vref(820.0, 100.0, 12000.0);
	mp28167_set_vref(am_ssc, new_vref);

	// enable RAD_READ
	HAL_GPIO_WritePin(RAD_READ_GPIO_Port, RAD_READ_Pin, 1);

	HAL_Delay(10);

	// read out without RADFET
	ret_val = radfet_read(R_0, rad0_v, rad0_i);

	// read out RADFET R1
	if (OK == ret_val)
		ret_val = radfet_read(R_1, rad1_v, rad1_i);

	// read out RADFET R2
	if (OK == ret_val)
		ret_val = radfet_read(R_2, rad2_v, rad2_i);

	// read out both RADFETs R1 + R2
	if (OK == ret_val)
		ret_val = radfet_read(R_12, rad12_v, rad12_i);

	// disable RAD_READ
	HAL_GPIO_WritePin(RAD_READ_GPIO_Port, RAD_READ_Pin, 0);

	// reset and disable VCC
	new_vref = mp28167_calculate_vref(820.0, 100.0, 5000.0);
	mp28167_set_vref(am_ssc, new_vref);
	mp28167_set_enable(am_ssc, false);

	return ret_val;
}

/**
 * @brief Read current temperature from given sensor.
 *
 * @param   id   Sensor ID
 * @return Temperature read from sensor as 12 bit raw value.
 */
uint16_t read_temperature(enum sensor id)
{
	uint16_t temperature;
	tmp75bq1_read_temperature_raw(am_hi2c, id, &temperature);
	return temperature;
}

/* ##### PUBLIC FUNCTIONS ################################################# */

/* ------------------------------------------------------------------------ */
void ambient_monitoring_init(I2C_HandleTypeDef hi2c, mp28167* ssc)
{
	// initialize circular buffer for ambient monitoring data
	uint32_t meas_per_orbit = config_get_value(CONFIG_ID_ORBIT_DURATION) / config_get_value(CONFIG_ID_AM_INTERVAL);
	uint16_t am_buffer_size = am_data_real_size * meas_per_orbit * config_get_value(CONFIG_ID_NUMBER_OF_ORBITS_FOR_AM);
	am_buffer = malloc(am_buffer_size);
	am_circular_buffer = circular_buffer_init(am_buffer, am_buffer_size);

	// initialize communication with sensors
	am_hi2c = hi2c;

	// initialize reference to SSC
	am_ssc = ssc;
}

/* ------------------------------------------------------------------------ */
void ambient_monitoring_free()
{
	free(am_circular_buffer);
	free(am_buffer);
}

/* ------------------------------------------------------------------------ */
void ambient_monitoring_read()
{
	// get current time stamp
	current_am_data.ts = utc_ts_pps;

	// read TID from RAD-FET
	radfet_get_tid(&current_am_data.rad0_v,  &current_am_data.rad0_i,
				   &current_am_data.rad1_v,  &current_am_data.rad1_i,
			       &current_am_data.rad2_v,  &current_am_data.rad2_i,
				   &current_am_data.rad12_v, &current_am_data.rad12_i);

	// read temperature sensor 1
	current_am_data.temp_1 = read_temperature(sensor1);
	// read temperature sensor 2
	current_am_data.temp_2 = read_temperature(sensor2);

	// pack data into circular buffer
	circular_buffer_put(am_circular_buffer, &current_am_data.ts, sizeof(current_am_data.ts));
	circular_buffer_put(am_circular_buffer, &current_am_data.rad0_v,  sizeof(current_am_data.rad0_v));
	circular_buffer_put(am_circular_buffer, &current_am_data.rad0_i,  sizeof(current_am_data.rad0_i));
	circular_buffer_put(am_circular_buffer, &current_am_data.rad1_v,  sizeof(current_am_data.rad1_v));
	circular_buffer_put(am_circular_buffer, &current_am_data.rad1_i,  sizeof(current_am_data.rad1_i));
	circular_buffer_put(am_circular_buffer, &current_am_data.rad2_v,  sizeof(current_am_data.rad2_v));
	circular_buffer_put(am_circular_buffer, &current_am_data.rad2_i,  sizeof(current_am_data.rad2_i));
	circular_buffer_put(am_circular_buffer, &current_am_data.rad12_v, sizeof(current_am_data.rad12_v));
	circular_buffer_put(am_circular_buffer, &current_am_data.rad12_i, sizeof(current_am_data.rad12_i));
	circular_buffer_put(am_circular_buffer, &current_am_data.temp_1,  sizeof(current_am_data.temp_1));
	circular_buffer_put(am_circular_buffer, &current_am_data.temp_2,  sizeof(current_am_data.temp_2));

	// send TM packet
	if (ambient_monitoring_tm_enabled)
	{
		tm_send_am_report(current_am_data);
	}
}

/* ------------------------------------------------------------------------ */
void ambient_monitoring_get_all_data(orbit_no_t orbit)
{
	am_data_t msg;
	size_t    index;

	// determine start index in circular buffer:
	// for each orbit go back the number of messages per orbit multiplied with the message length
	uint16_t msg_per_orbit = config_get_value(CONFIG_ID_ORBIT_DURATION) / config_get_value(CONFIG_ID_AM_INTERVAL);
	size_t   steps_back    = orbit * msg_per_orbit * sizeof(am_data_t);
	if (steps_back <= circular_buffer_head(am_circular_buffer))
	{
		index = circular_buffer_head(am_circular_buffer) - steps_back;
	}
	else
	{
		// handle wrapping
		steps_back = steps_back - circular_buffer_head(am_circular_buffer);
		index = circular_buffer_capacity(am_circular_buffer) - steps_back;
	}

	for (int i = 0; i < msg_per_orbit; i++)
	{
		// get all data from circular buffer
		circular_buffer_peek_at(am_circular_buffer, &msg.ts, sizeof(msg.ts), index);
		index += sizeof(msg.ts);
		circular_buffer_peek_at(am_circular_buffer, &msg.rad0_v, sizeof(msg.rad0_v), index);
		index += sizeof(msg.rad1_v);
		circular_buffer_peek_at(am_circular_buffer, &msg.rad0_i, sizeof(msg.rad0_i), index);
		index += sizeof(msg.rad0_i);
		circular_buffer_peek_at(am_circular_buffer, &msg.rad1_v, sizeof(msg.rad1_v), index);
		index += sizeof(msg.rad1_v);
		circular_buffer_peek_at(am_circular_buffer, &msg.rad1_i, sizeof(msg.rad1_i), index);
		index += sizeof(msg.rad1_i);
		circular_buffer_peek_at(am_circular_buffer, &msg.rad2_v, sizeof(msg.rad2_v), index);
		index += sizeof(msg.rad2_v);
		circular_buffer_peek_at(am_circular_buffer, &msg.rad2_i, sizeof(msg.rad2_i), index);
		index += sizeof(msg.rad2_i);
		circular_buffer_peek_at(am_circular_buffer, &msg.rad12_v, sizeof(msg.rad12_v), index);
		index += sizeof(msg.rad12_v);
		circular_buffer_peek_at(am_circular_buffer, &msg.rad12_i, sizeof(msg.rad12_i), index);
		index += sizeof(msg.rad12_i);
		circular_buffer_peek_at(am_circular_buffer, &msg.temp_1, sizeof(msg.temp_1), index);
		index += sizeof(msg.temp_1);
		circular_buffer_peek_at(am_circular_buffer, &msg.temp_2, sizeof(msg.temp_2), index);
		index += sizeof(msg.temp_2);

		// send TM packet
		tm_send_am_report(msg);
	}
}

/* ------------------------------------------------------------------------ */
am_data_t ambient_monitoring_get_last_data()
{
	return current_am_data;
}
