/**
 * @file can.c
 *
 * @brief Everything regarding CAN communication
 *
 * @author Stefan Wertheimer
 * @date 2024-01-23
 */

#include "can.h"

/** Handle for FDCAN */
FDCAN_HandleTypeDef* hfdcan;

/** CAN RX header */
FDCAN_RxHeaderTypeDef rx_header;
/** CAN RX buffer */
uint8_t rx_data[CANFD_MAX_DATA_LENGTH];
/** CAN TX header */
FDCAN_TxHeaderTypeDef tx_header;
/** CAN TX buffer */
uint8_t tx_data[CANFD_MAX_DATA_LENGTH];

uint32_t utc_ts_pps;

/** Flag indicating whether CAN interface is initialized */
_Bool can_initialized = false;

/**
 * Keeping required stuff for Cyphal together.
 */
typedef struct cyphal_core
{
    CanardMicrosecond started_at;

    O1HeapInstance* heap;
    CanardInstance  canard;
    CanardTxQueue   canard_tx_queues[CAN_REDUNDANCY_FACTOR];

    struct
	{
    	CanardPortID mobefuse_hk_report;
    	CanardPortID mobefuse_ambient_monitoring_report;
    	CanardPortID mobefuse_single_experiment_report;
	} port_id;

    struct
	{
    	uint64_t uavcan_node_heartbeat;
        uint64_t uavcan_node_port_list;

        uint64_t mobefuse_hk_service;
        uint64_t mobefuse_ambient_monitoring_service;
        uint64_t mobefuse_experiment_service;
	} next_transfer_id;
} cyphal_t;

/** Central instance of Cyphal related parameters. */
static cyphal_t cyphal;

/** Cyphal transcription object for ExecuteCommand messages. */
CanardRxSubscription execute_command_subscription;

/** Cyphal transcription object for Heartbeat messages. */
CanardRxSubscription heartbeat_subscription;

/* ----- */

/**
  * @brief  Configure FDCAN in STM32 MCU.
  * @return OK on success, CAN_INIT_ERROR otherwise
  */
enum error_code fdcan_config()
{
	enum error_code result = OK;

	FDCAN_FilterTypeDef sFilterConfig;

	/* Configure Rx filter */
	sFilterConfig.IdType = FDCAN_EXTENDED_ID;
	sFilterConfig.FilterIndex = 0;
	sFilterConfig.FilterType = FDCAN_FILTER_MASK;
	sFilterConfig.FilterConfig = FDCAN_FILTER_TO_RXFIFO0;
	sFilterConfig.FilterID1 = MBFS_FDCAN_FILTER_ID_1;
	sFilterConfig.FilterID2 = MBFS_FDCAN_FILTER_ID_2;
	if (HAL_OK != HAL_FDCAN_ConfigFilter(hfdcan, &sFilterConfig))
	{
		result = CAN_CONFIG_ERROR;
	}

	/* Configure global filter:
	Filter all remote frames with STD and EXT ID
	Reject non matching frames with STD ID and EXT ID */
	/*if (OK == result && HAL_OK != HAL_FDCAN_ConfigGlobalFilter(hfdcan, FDCAN_REJECT, FDCAN_REJECT, FDCAN_FILTER_REMOTE, FDCAN_FILTER_REMOTE))
	{
		result = CAN_CONFIG_ERROR;
	}*/

	/* Start the FDCAN module */
	if (OK == result && HAL_OK != HAL_FDCAN_Start(hfdcan))
	{
		result = CAN_CONFIG_ERROR;
	}

	if (OK == result && HAL_OK != HAL_FDCAN_ActivateNotification(hfdcan, FDCAN_IT_RX_FIFO0_NEW_MESSAGE, 0))
	{
		result = CAN_CONFIG_ERROR;
	}

	HAL_NVIC_SetPriority(FDCAN1_IT0_IRQn, 0, 0);
	HAL_NVIC_SetPriority(FDCAN1_IT1_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(FDCAN1_IT0_IRQn);
	HAL_NVIC_EnableIRQ(FDCAN1_IT1_IRQn);

	/* Prepare Tx Header */
	tx_header.Identifier = MBFS_FDCAN_TX_HEADER_ID;
	tx_header.IdType = FDCAN_EXTENDED_ID;
	tx_header.TxFrameType = FDCAN_DATA_FRAME;
	tx_header.DataLength = FDCAN_DLC_BYTES_8;
	tx_header.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
	tx_header.BitRateSwitch = FDCAN_BRS_OFF;
	tx_header.FDFormat = FDCAN_CLASSIC_CAN;
	tx_header.TxEventFifoControl = FDCAN_NO_TX_EVENTS;
	tx_header.MessageMarker = 0;

	return result;
}

/**
 * @brief Allocate memory for CAN library.
 *
 * @param  ins     Pointer to CAN library instance
 * @param  amount  Amount of memory to allocate in bytes
 * @return Pointer to allocated memory
 */
static void* can_alloc(CanardInstance* ins, size_t amount)
{
	/*void* p_heap = NULL;
	O1HeapInstance* const heap = ((cyphal_t*) ins->user_reference)->heap;
	if (o1heapDoInvariantsHold(heap))
	{
		p_heap = o1heapAllocate(heap, amount);
	}
	return p_heap;*/
	(void) ins;
	return malloc(amount);
}

/**
 * @brief Free previously allocated memory for CAN library.
 *
 * @param  ins      Pointer to CAN library instance
 * @param  pointer  Pointer to previously allocated memory
 */
static void can_free(CanardInstance* ins, void* pointer)
{
	/*
	O1HeapInstance* const heap = ((cyphal_t*) ins->user_reference)->heap;
	o1heapFree(heap, pointer);
	*/
	(void) ins;
	free( pointer );
}

/**
 * @brief Subscribe to required Subject IDs on RX side.
 */
void can_rx_subscribe()
{
	(void) canardRxSubscribe(&(cyphal.canard),
	                         CanardTransferKindMessage,
							 uavcan_node_Heartbeat_1_0_FIXED_PORT_ID_,
							 uavcan_node_Heartbeat_1_0_EXTENT_BYTES_,
	                         CANARD_DEFAULT_TRANSFER_ID_TIMEOUT_USEC,
	                         &heartbeat_subscription);
	(void) canardRxSubscribe(&(cyphal.canard),
	                         CanardTransferKindMessage,
	                         uavcan_node_ExecuteCommand_1_3_FIXED_PORT_ID_,
	                         uavcan_node_ExecuteCommand_Request_1_3_EXTENT_BYTES_,
	                         CANARD_DEFAULT_TRANSFER_ID_TIMEOUT_USEC,
	                         &execute_command_subscription);
	(void) canardRxSubscribe(&(cyphal.canard),
		                     CanardTransferKindMessage,
		                     ororatech_otc_p1_UTCTimeAtLastPPS_0_1_FIXED_PORT_ID_,
							 ororatech_otc_p1_UTCTimeAtLastPPS_0_1_EXTENT_BYTES_,
		                     CANARD_DEFAULT_TRANSFER_ID_TIMEOUT_USEC,
		                     &execute_command_subscription);
}

void can_execute_response(error_code_t code)
{
	uavcan_node_ExecuteCommand_Response_1_3 msg = {0};
	uint8_t buffer[uavcan_node_ExecuteCommand_Response_1_3_SERIALIZATION_BUFFER_SIZE_BYTES_] = {0};
	size_t  size = sizeof(buffer);

	switch (code)
	{
		case OK:
			msg.status = uavcan_node_ExecuteCommand_Response_1_3_STATUS_SUCCESS;
			break;
		case TC_FAILURE:
			msg.status = uavcan_node_ExecuteCommand_Response_1_3_STATUS_FAILURE;
			break;
		case TC_BAD_COMMAND:
			msg.status = uavcan_node_ExecuteCommand_Response_1_3_STATUS_BAD_COMMAND;
			break;
		case TC_BAD_PARAMETER:
			msg.status = uavcan_node_ExecuteCommand_Response_1_3_STATUS_BAD_PARAMETER;
			break;
		default:
			msg.status = uavcan_node_ExecuteCommand_Response_1_3_STATUS_INTERNAL_ERROR;
			msg.output.count = sizeof(error_code_t);
			memcpy(&msg.output.elements, &code, msg.output.count);
			break;
	}

	uavcan_node_ExecuteCommand_Response_1_3_serialize_(&msg, &buffer[0], &size);
	can_send(&buffer[0], size, uavcan_node_ExecuteCommand_1_3_FIXED_PORT_ID_);
}

/**
 * @brief Process received CAN transfer object.
 *
 * @param  ins       Pointer to CAN library instance
 * @param  transfer  Pointer to received transfer object
 */
void can_process_transfer(CanardInstance* const ins, const CanardRxTransfer* const transfer)
{
	if (transfer->metadata.transfer_kind == CanardTransferKindMessage)
	{
		size_t size = transfer->payload_size;
		if (transfer->metadata.port_id == uavcan_node_ExecuteCommand_1_3_FIXED_PORT_ID_)
		{
			uavcan_node_ExecuteCommand_Request_1_3 msg = {0};
			if (uavcan_node_ExecuteCommand_Request_1_3_deserialize_(&msg, transfer->payload, &size) >= 0)
			{
				error_code_t rc;
				rc = tc_execute(msg.command, &msg.parameter.elements[0], msg.parameter.count);
				can_execute_response(rc);
			}
		}
		else if (transfer->metadata.port_id == ororatech_otc_p1_UTCTimeAtLastPPS_0_1_FIXED_PORT_ID_)
		{
			ororatech_otc_p1_UTCTimeAtLastPPS_0_1 msg = {0};
			if (ororatech_otc_p1_UTCTimeAtLastPPS_0_1_deserialize_(&msg, transfer->payload, &size) >= 0)
			{
				utc_ts_pps = msg.utc_time;
			}
		}
		else
		{
			// :TODO: error handling? port subscription without a handler
		}
	}
}

/* ------------------------------------------------------------------------ */
enum error_code can_init(FDCAN_HandleTypeDef* handle)
{
	enum error_code result = OK;

	if (!can_initialized)
	{
		hfdcan = handle;

		// configure basic CAN functionality in MCU
		result = fdcan_config();

		if (OK == result)
		{
			// initialize heap
			//_Alignas(O1HEAP_ALIGNMENT) static uint8_t heap_arena[KIBI * CAN_HEAP_SIZE] = {0};
			//cyphal.heap = o1heapInit(heap_arena, sizeof(heap_arena));
			//if (NULL == cyphal.heap)
			//{
			//	result = CAN_HEAP_INIT_ERRROR;
			//}

			// initialize libcanard
			cyphal.canard = canardInit(&can_alloc, &can_free);
			cyphal.canard.node_id = CAN_NODE_ID;
			cyphal.canard.user_reference = &cyphal;

			for (uint8_t ifidx = 0; ifidx < CAN_REDUNDANCY_FACTOR; ifidx++)
			{
				cyphal.canard_tx_queues[ifidx] = canardTxInit(CAN_TX_QUEUE_CAPACITY, CANFD_MAX_DATA_LENGTH);
			}
		}

		can_initialized = true;
	}

	return result;
}

/* ------------------------------------------------------------------------ */
enum error_code can_receive(FDCAN_RxHeaderTypeDef header, uint8_t* const data, const size_t bytes)
{
	CanardFrame rxf;
	rxf.extended_can_id = header.Identifier;
	if (rx_header.DataLength < MBFS_FDCAN_RX_BUFFER_SIZE)
	{
		rxf.payload_size = (size_t)header.DataLength;
	}
	else
	{
		rxf.payload_size = MBFS_FDCAN_RX_BUFFER_SIZE;
	}
	rxf.payload = (void*)data;

	CanardRxTransfer transfer;

	if(canardRxAccept((CanardInstance *const)&(cyphal.canard), get_micros(), &rxf, 0, &transfer, NULL) != 1)
	{
		return CAN_INVALID_TRANSFER_FRAME;
	}

	// deserialization/processing of received transfer data
	can_process_transfer((CanardInstance *const)&(cyphal.canard), &transfer);

	cyphal.canard.memory_free(&(cyphal.canard), transfer.payload);

	return OK;
}

/* ------------------------------------------------------------------------ */
static uint32_t transfer_id = 0;
enum error_code can_send(uint8_t* const buffer, size_t buffer_size, CanardPortID port_id)
{
	CanardTransferMetadata meta;
    meta.transfer_kind = CanardTransferKindMessage;
    meta.port_id = port_id;
    meta.priority = 7; // 0..7, 0 is highest
    meta.remote_node_id = CANARD_NODE_ID_UNSET;
    meta.transfer_id = transfer_id++;

    for (uint8_t ifidx = 0; ifidx < CAN_REDUNDANCY_FACTOR; ifidx++)
	{
		(void)canardTxPush(
				(CanardTxQueue* const)&(cyphal.canard_tx_queues[ifidx]),
				(CanardInstance* const)&(cyphal.canard),
				get_micros(),
				&meta,
				buffer_size,
				buffer);
	}

	return OK;
}

/* ------------------------------------------------------------------------ */
void can_transmit(void)
{
	// transmit pending frames from the prioritized TX queues managed by libcanard
	for (uint8_t ifidx = 0; ifidx < CAN_REDUNDANCY_FACTOR; ifidx++)
	{
		CanardTxQueue* const     que = &cyphal.canard_tx_queues[ifidx];
		const CanardTxQueueItem* tqi = canardTxPeek(que);  // Find the highest-priority frame.

		uint32_t frame_counter = 0;

		while (tqi)
		{
			canfd_frame_t can_frame;
			can_frame.can_id = tqi->frame.extended_can_id;
			can_frame.len    = (uint8_t)tqi->frame.payload_size;
			can_frame.flags = CANFD_FLAG_BRS;

			(void)memcpy(can_frame.data, tqi->frame.payload, tqi->frame.payload_size);
			tx_header.Identifier = tqi->frame.extended_can_id;

			if (HAL_FDCAN_AddMessageToTxFifoQ(hfdcan, &tx_header, (const uint8_t*)&can_frame.data) != HAL_OK)
			{
				if (HAL_FDCAN_ERROR_FIFO_FULL == hfdcan->ErrorCode)
				{
					continue; // low-level FIFO is full -> just wait to get empty
				}
				else
				{
					continue;
					// :TODO: error handling
				}
			} else {
				if (++frame_counter > 0)
				{
					// remove packet if TX success
					cyphal.canard.memory_free(&cyphal.canard, canardTxPop(que, tqi));
					tqi = canardTxPeek(que);
					frame_counter = 0;
				}
			}
		}
	}
}
