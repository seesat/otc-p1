/**
 * @file circular_buffer.h
 *
 * @brief Circular buffer for data storage.
 *
 * @author Stefan Wertheimer
 * @date 2023-12-22
 */

#include "circular_buffer.h"

struct circular_buffer {
	uint8_t* buffer;
	size_t head;
	size_t tail;
	size_t max;
	bool full;
};

/**
 * Advance head and tail pointers depending on circular buffer state if data is added.
 *
 * @param  me      Handle of circular buffer
 * @param  amount  Amount of bytes fore advancing pointer
 */
static void advance_pointer(cb_handle_t me, size_t amount)
{
	if (me->full)
   	{
		if ((me->tail += amount) == me->max)
		{
			me->tail = 0;
		}
	}

	if ((me->head += amount) == me->max)
	{
		me->head = 0;
	}
	me->full = (me->head == me->tail);
}

/**
 * Retreat tail pointer depending if data is removed.
 *
 * @param  me      Handle of circular buffer
 * @param  amount  Amount of bytes fore retreating pointer
 */
static void retreat_pointer(cb_handle_t me, size_t amount)
{
	me->full = false;
	if ((me->tail += amount) == me->max)
	{
		me->tail = 0;
	}
}

/* ------------------------------------------------------------------------ */
cb_handle_t circular_buffer_init(uint8_t* buffer, size_t size)
{
	// get memory for data structure
	cb_handle_t cbuf = malloc(sizeof(circular_buffer_t));
	// set memory for buffer
	cbuf->buffer = buffer;
	cbuf->max = size;

	circular_buffer_reset(cbuf);
	if (circular_buffer_empty(cbuf))
	{
		return cbuf;
	}
	else
	{
		return NULL;
	}
}

/* ------------------------------------------------------------------------ */
void circular_buffer_free(cb_handle_t me)
{
	free(me);
}

/* ------------------------------------------------------------------------ */
void circular_buffer_reset(cb_handle_t me)
{
	me->head = 0;
	me->tail = 0;
	me->full = false;
}

/* ------------------------------------------------------------------------ */
void circular_buffer_put(cb_handle_t me, const void * data, size_t size)
{
	memcpy(&me->buffer[me->head], data, size);
	advance_pointer(me, size);
}

/* ------------------------------------------------------------------------ */
bool circular_buffer_get(cb_handle_t me, void * data, size_t size)
{
	int result = false;
	if (!circular_buffer_empty(me))
	{
		memcpy(data, &me->buffer[me->tail], size);
		retreat_pointer(me, size);
		result = true;
	}
	return result;
}

/* ------------------------------------------------------------------------ */
bool circular_buffer_peek(cb_handle_t me, void * data, size_t size)
{
	int result = false;
	if (!circular_buffer_empty(me))
	{
		memcpy(data, &me->buffer[me->tail], size);
		result = true;
	}
	return result;
}

/* ------------------------------------------------------------------------ */
bool circular_buffer_peek_at(cb_handle_t me, void * data, size_t size, size_t at)
{
	int result = false;
	if (!circular_buffer_empty(me))
	{
		memcpy(data, &me->buffer[at], size);
		result = true;
	}
	return result;
}

/* ------------------------------------------------------------------------ */
bool circular_buffer_empty(cb_handle_t me)
{
	return (!me->full && (me->head == me->tail));
}

/* ------------------------------------------------------------------------ */
bool circular_buffer_full(cb_handle_t me)
{
	return me->full;
}

/* ------------------------------------------------------------------------ */
size_t circular_buffer_capacity(cb_handle_t me)
{
	return me->max;
}

/* ------------------------------------------------------------------------ */
size_t circular_buffer_size(cb_handle_t me)
{
	size_t size = me->max;

	if (!me->full)
	{
		if(me->head >= me->tail)
		{
			size = (me->head - me->tail);
		}
		else
		{
			size = (me->max + me->head - me->tail);
		}
	}

	return size;
}

/* ------------------------------------------------------------------------ */
size_t circular_buffer_head(cb_handle_t me)
{
	return me->head;
}

/* ------------------------------------------------------------------------ */
size_t circular_buffer_tail(cb_handle_t me)
{
	return me->tail;
}
