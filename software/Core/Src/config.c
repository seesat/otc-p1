/**
 * @file config.c
 *
 * @brief Management of runtime configuration
 *
 * @author Stefan Wertheimer
 * @date 2024-01-20
 */

#include "config.h"
#include "constants.h"


/* ##### PUBLIC VARIABLES ################################################# */
static volatile _Bool config_skip_main_loop = false;

/* ##### PRIVATE VARIABLES ################################################ */

/** Storage array for all manipulatable configuration parameters. */
uint32_t* params;

/* ##### PUBLIC FUNCTIONS ################################################# */

/* ------------------------------------------------------------------------ */
void config_init()
{
	params = malloc(sizeof(uint32_t) * CONFIG_ID_NUM);

	config_set_value(CONFIG_ID_EXP_INTERVAL, DEFAULT_EXP_INTERVAL);
	config_set_value(CONFIG_ID_AM_INTERVAL, DEFAULT_AM_INTERVAL);
	config_set_value(CONFIG_ID_HK_INTERVAL, DEFAULT_HK_INTERVAL);

	config_set_value(CONFIG_ID_ORBIT_DURATION, DEFAULT_ORBIT_DURATION);

	config_set_value(CONFIG_ID_NUMBER_OF_ORBITS_FOR_AM, DEFAULT_NUM_ORBITS_AM);
	config_set_value(CONFIG_ID_NUMBER_OF_EXP_RUNS, DEFAULT_NUM_RUNS_EXP);
	config_set_value(CONFIG_ID_NUMBER_OF_SINGLE_EXP, DEFAULT_NUM_SINGLE_EXP);
	config_set_value(CONFIG_ID_NUMBER_OF_EXP_SUITES, DEFAULT_NUM_SUITES_EXP);

	config_set_value(CONFIG_ID_EXP_RUN_LENGTH, DEFAULT_EXP_RUN_LENGTH);
	config_set_value(CONFIG_ID_EXP_ENABLE, DEFAULT_EXP_ENABLE);
	config_set_value(CONFIG_ID_NUMBER_OF_EXP_ENABLED, DEFAULT_NUM_OF_EXP_ENABLE);
}

/* ------------------------------------------------------------------------ */
void config_delete()
{
	free(params);
}

/* ------------------------------------------------------------------------ */
void config_set_value(config_id_t parameter, uint32_t value)
{
	params[parameter - MOBEFUSE_CONFIG_ID] = value;
}

/* ------------------------------------------------------------------------ */
uint32_t config_get_value(config_id_t parameter)
{
	return params[parameter - MOBEFUSE_CONFIG_ID];
}

/* ------------------------------------------------------------------------ */
void config_create_parameter_report(void)
{
	config_data_t data;

	data.ts                      = utc_ts_pps;
	data.exp_interval            = params[CONFIG_ID_EXP_INTERVAL];
	data.am_interval             = params[CONFIG_ID_AM_INTERVAL];
	data.hk_interval             = params[CONFIG_ID_HK_INTERVAL];
	data.orbit_duration          = params[CONFIG_ID_ORBIT_DURATION];
	data.number_of_orbits_for_am = params[CONFIG_ID_NUMBER_OF_ORBITS_FOR_AM];
	data.number_of_exp_runs      = params[CONFIG_ID_NUMBER_OF_EXP_RUNS];
	data.number_of_single_exp    = params[CONFIG_ID_NUMBER_OF_SINGLE_EXP];
	data.number_of_exp_suites    = params[CONFIG_ID_NUMBER_OF_EXP_SUITES];
	data.exp_run_length          = params[CONFIG_ID_EXP_RUN_LENGTH];
	data.exp_enable              = params[CONFIG_ID_EXP_ENABLE];
	data.number_of_exp_enabled   = params[CONFIG_ID_NUMBER_OF_EXP_ENABLED];

	tm_send_config_report(data);
}
