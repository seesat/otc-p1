/*
 * @file experiment_fixture.c
 *
 * @brief Fixture to run eFuse experiments
 *
 * @author Stefan Wertheimer
 * @date 2023-12-29
 */

#include "experiment_fixture.h"
#include "timer.h"
#include "adc.h"


/* ##### PUBLIC VARIABLES ################################################# */
static volatile _Bool experiment_tm_enabled = true;

/** @brief Look-up table of permissible experiments. First index is DUT, second index is experiment. */
static const exp_enable_t exp_lut[4][4] = {
	{            0,            0,            0,            0 },
	{ DUT_1_OVP_01,            0, DUT_1_OCP_01, DUT_1_OCP_02 },
	{            0,            0,            0,            0 },
	{            0,            0, DUT_3_OCP_01, DUT_3_OCP_02 }
};

/* ##### PRIVATE VARIABLES ################################################ */

I2C_HandleTypeDef exp_hi2c;

mp28167* exp_ssc;

uint8_t* exp_buffer;

cb_handle_t exp_circular_buffer;

uint8_t* exp_run_buffer;

cb_handle_t exp_run_circular_buffer;

_Bool is_part_of_run;

uint8_t* exp_suite_buffer;

cb_handle_t exp_suite_circular_buffer;

_Bool is_part_of_suite;

_Bool experiment_running;

/* ##### PRIVATE FUNCTIONS ################################################ */

void store_results(exp_data_t data)
{
	circular_buffer_put(exp_circular_buffer, &data.ts_start, sizeof(data.ts_start));
	circular_buffer_put(exp_circular_buffer, &data.experiment, sizeof(data.experiment));
	circular_buffer_put(exp_circular_buffer, &data.dut_select, sizeof(data.dut_select));
	circular_buffer_put(exp_circular_buffer, &data.load_config, sizeof(data.load_config));
	circular_buffer_put(exp_circular_buffer, &data.rep_cnt, sizeof(data.rep_cnt));
	circular_buffer_put(exp_circular_buffer, &data.dut_fault, sizeof(data.dut_fault));
	circular_buffer_put(exp_circular_buffer, &data.dut_v_in, sizeof(data.dut_v_in));
	circular_buffer_put(exp_circular_buffer, &data.t_trip, sizeof(data.t_trip));
	circular_buffer_put(exp_circular_buffer, &data.dut_v_out_trip, sizeof(data.dut_v_out_trip));
	circular_buffer_put(exp_circular_buffer, &data.dut_i_sense_trip, sizeof(data.dut_i_sense_trip));

	if (is_part_of_run)
	{
		circular_buffer_put(exp_run_circular_buffer, &data.ts_start, sizeof(data.ts_start));
		circular_buffer_put(exp_run_circular_buffer, &data.experiment, sizeof(data.experiment));
		circular_buffer_put(exp_run_circular_buffer, &data.dut_select, sizeof(data.dut_select));
		circular_buffer_put(exp_run_circular_buffer, &data.load_config, sizeof(data.load_config));
		circular_buffer_put(exp_run_circular_buffer, &data.rep_cnt, sizeof(data.rep_cnt));
		circular_buffer_put(exp_run_circular_buffer, &data.dut_fault, sizeof(data.dut_fault));
		circular_buffer_put(exp_run_circular_buffer, &data.dut_v_in, sizeof(data.dut_v_in));
		circular_buffer_put(exp_run_circular_buffer, &data.t_trip, sizeof(data.t_trip));
		circular_buffer_put(exp_run_circular_buffer, &data.dut_v_out_trip, sizeof(data.dut_v_out_trip));
		circular_buffer_put(exp_run_circular_buffer, &data.dut_i_sense_trip, sizeof(data.dut_i_sense_trip));
	}

	if (is_part_of_suite)
	{
		circular_buffer_put(exp_suite_circular_buffer, &data.ts_start, sizeof(data.ts_start));
		circular_buffer_put(exp_suite_circular_buffer, &data.experiment, sizeof(data.experiment));
		circular_buffer_put(exp_suite_circular_buffer, &data.dut_select, sizeof(data.dut_select));
		circular_buffer_put(exp_suite_circular_buffer, &data.load_config, sizeof(data.load_config));
		circular_buffer_put(exp_suite_circular_buffer, &data.rep_cnt, sizeof(data.rep_cnt));
		circular_buffer_put(exp_suite_circular_buffer, &data.dut_fault, sizeof(data.dut_fault));
		circular_buffer_put(exp_suite_circular_buffer, &data.dut_v_in, sizeof(data.dut_v_in));
		circular_buffer_put(exp_suite_circular_buffer, &data.t_trip, sizeof(data.t_trip));
		circular_buffer_put(exp_suite_circular_buffer, &data.dut_v_out_trip, sizeof(data.dut_v_out_trip));
		circular_buffer_put(exp_suite_circular_buffer, &data.dut_i_sense_trip, sizeof(data.dut_i_sense_trip));
	}
}

/**
 * @brief Conduct under-voltage protection experiment.
 *
 * @param  dut      DUT to conduct experiment on
 * @param  rep_dnt  Repetition counter for multiple execution
 */
void conduct_uvp_experiment_rep(dut_t dut, uint8_t rep_cnt)
{
	return;
}

/**
 * @brief Conduct under-voltage protection experiment.
 *
 * @param  dut  DUT to conduct experiment on
 */
void conduct_uvp_experiment(dut_t dut)
{
	conduct_uvp_experiment_rep(dut, 0);
}

/**
 * @brief Conduct over-voltage protection experiment.
 *
 * @param  dut      DUT to conduct experiment on
 * @param  rep_dnt  Repetition counter for multiple execution
 */
void conduct_ovp_experiment_rep(dut_t dut, uint8_t rep_cnt)
{
	if (dut != DUT_1)
		return;

	exp_data_t data = {0};
	data.ts_start = utc_ts_pps;
	data.experiment = EXP_OVP;
	data.dut_select = DUT_1;
	data.load_config = 0;
	data.rep_cnt = rep_cnt;

	// enable VCC
	mp28167_set_enable(exp_ssc, true);

	// calculate VCC vref values
	uint16_t vref_bottom = mp28167_calculate_vref(820.0, 100.0, 9000.0);
	uint16_t vref_top = mp28167_calculate_vref(820.0, 100.0, 14500.0);
	mp28167_set_vref(exp_ssc, vref_bottom);

	// connect and enable DUT
	HAL_GPIO_WritePin(DUT1_SEL_GPIO_Port, DUT1_SEL_Pin, 1);
	HAL_GPIO_WritePin(DUT1_EN_GPIO_Port, DUT1_EN_Pin, 1);

	// enable load simulator
	HAL_GPIO_WritePin(LS3_SEL_GPIO_Port, LS3_SEL_Pin, data.load_config & 1);

	// init ADC
	adc_init_one_slow(ADC_DUT_V_OUT_SENSE, ADC_DUT_V_OUT_SENSE_CHANNEL);

	// wait for VCC soft start
	HAL_Delay(10);

	// iterate over DUT input voltages
	uint16_t vref = vref_bottom;
	bool fuse_tripped = false;
	while ((vref < vref_top) && !fuse_tripped)
	{
		mp28167_set_vref(exp_ssc, vref);
		HAL_Delay(4);

		adc_start_dma(ADC_DUT_V_OUT_SENSE);
		HAL_Delay(6);
		uint32_t vout_val = adc_get_single_value_dma(ADC_DUT_V_OUT_SENSE);
		if (vout_val <= 1550) // (DUT_V_OUT_SENSE < 1.25V)
		{
			fuse_tripped = true;

			adc_init_one_slow(ADC_VCC_SENSE, ADC_VCC_SENSE_CHANNEL);
			adc_start_dma(ADC_VCC_SENSE);
			HAL_Delay(6);
			data.dut_v_in = adc_get_single_value_dma(ADC_VCC_SENSE);
			data.t_trip = vref;

			adc_init_one_slow(ADC_DUT_V_OUT_SENSE, ADC_DUT_V_OUT_SENSE_CHANNEL);
			adc_init_one_slow(ADC_DUT1_I_SENSE, ADC_DUT1_I_SENSE_CHANNEL);
			adc_start_single_dma(ADC_DUT_V_OUT_SENSE);
			adc_start_single_dma(ADC_DUT1_I_SENSE);
			HAL_Delay(6);
			data.dut_v_out_trip = adc_get_single_value_dma(ADC_DUT_V_OUT_SENSE);
			data.dut_i_sense_trip = adc_get_single_value_dma(ADC_DUT1_I_SENSE);
		}
		vref += 20;
	}
	vref -= 10;

	// disable load simulator
	HAL_GPIO_WritePin(LS3_SEL_GPIO_Port, LS3_SEL_Pin, 0);

	// disconnect and disable DUT
	HAL_GPIO_WritePin(DUT1_SEL_GPIO_Port, DUT1_SEL_Pin, 0);
	HAL_GPIO_WritePin(DUT1_EN_GPIO_Port, DUT1_EN_Pin, 0);

	// reset and disable VCC
	vref = mp28167_calculate_vref(820.0, 100.0, 5000.0);
	mp28167_set_vref(exp_ssc, vref);
	mp28167_set_enable(exp_ssc, false);

	if (experiment_tm_enabled)
		tm_send_exp_report(data);

	// store test result in circular buffer
	store_results(data);
}

/**
 * @brief Conduct over-voltage protection experiment.
 *
 * @param  dut  DUT to conduct experiment on
 */
void conduct_ovp_experiment(dut_t dut)
{
	conduct_ovp_experiment_rep(dut, 0);
}

/**
 * @brief Conduct over-current protection experiment.
 *
 * @param  dut  DUT to conduct experiment on
 * @param  five_volts  True for 5V, false for 3.3V
 * @param  rep_dnt  Repetition counter for multiple execution
 */
void conduct_ocp_experiment_rep(dut_t dut, _Bool five_volts, uint8_t rep_cnt)
{
	if ((dut != DUT_1) && (dut != DUT_3))
		return;

	uint16_t new_vref;
	uint32_t ts_start = utc_ts_pps;
	exp_data_t data[8] = {{0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}}; // :TODO: remove array

	// set VCC to 3.3V or 5V
	mp28167_set_enable(exp_ssc, true);
	if (five_volts)
		new_vref = mp28167_calculate_vref(820.0, 100.0, 5000.0);
	else
		new_vref = mp28167_calculate_vref(820.0, 100.0, 3300.0);
	mp28167_set_vref(exp_ssc, new_vref);

	// connect (and enable) DUT
	if (dut == DUT_1)
	{
		HAL_GPIO_WritePin(DUT1_SEL_GPIO_Port, DUT1_SEL_Pin, 1);
		HAL_GPIO_WritePin(DUT1_EN_GPIO_Port, DUT1_EN_Pin, 1);
	}
	else if (dut == DUT_3)
	{
		HAL_GPIO_WritePin(DUT3_SEL_GPIO_Port, DUT3_SEL_Pin, 1);
	}
	HAL_Delay(10);

	// iterate over load simulator configs
	bool fuse_tripped = false;
	for (int i = 0; (i < 8) && !fuse_tripped; ++i)
	{
		HAL_GPIO_WritePin(LS1_SEL_GPIO_Port, LS1_SEL_Pin, (i >> 2) & 1);
		HAL_GPIO_WritePin(LS2_SEL_GPIO_Port, LS2_SEL_Pin, (i >> 1) & 1);
		HAL_GPIO_WritePin(LS3_SEL_GPIO_Port, LS3_SEL_Pin, (i >> 0) & 1);

		if (dut == DUT_1)
		{
			// get start micros
			uint64_t timer_start = get_micros();
			uint64_t timer_delta = 0;

			// initialize comparator
			comp3_init();
			comp3_start();

			while((get_last_micros() < timer_start) && (timer_delta < 5000))
				timer_delta = get_micros() - timer_start;

			uint64_t timer_stop = get_last_micros();
			fuse_tripped = timer_stop > timer_start;

			if (fuse_tripped)
			{
				data[0].ts_start = ts_start;
				data[0].experiment = five_volts ? EXP_OCP_5_0 : EXP_OCP_3_3;
				data[0].dut_select = DUT_1;
				data[0].load_config = i;
				data[0].rep_cnt = rep_cnt;
				data[0].dut_fault = false;
				data[0].t_trip = timer_stop - timer_start;

				adc_init_one_slow(ADC_DUT_V_OUT_SENSE, ADC_DUT_V_OUT_SENSE_CHANNEL);
				adc_init_one_slow(ADC_DUT1_I_SENSE, ADC_DUT1_I_SENSE_CHANNEL);
				adc_start_single_dma(ADC_DUT_V_OUT_SENSE);
				adc_start_single_dma(ADC_DUT1_I_SENSE);
				HAL_Delay(6);
				data[0].dut_v_out_trip = adc_get_single_value_dma(ADC_DUT_V_OUT_SENSE);
				data[0].dut_i_sense_trip = adc_get_single_value_dma(ADC_DUT1_I_SENSE);

				adc_init_one_slow(ADC_VCC_SENSE, ADC_VCC_SENSE_CHANNEL);
				adc_start_dma(ADC_VCC_SENSE);
				HAL_Delay(6);
				data[0].dut_v_in = adc_get_single_value_dma(ADC_VCC_SENSE);

				if (experiment_tm_enabled)
					tm_send_exp_report(data[0]);

				// store result data in circular buffer
				store_results(data[0]);
			}
		}
		else if (dut == DUT_3)
		{
			HAL_Delay(4);

			data[i].ts_start = ts_start;
			data[i].experiment = five_volts ? EXP_OCP_5_0 : EXP_OCP_3_3;
			data[i].dut_select = DUT_3;
			data[i].load_config = i;
			data[i].rep_cnt = rep_cnt;
			data[i].dut_fault = false;
			data[i].t_trip = 0;

			adc_init_one_slow(ADC_DUT_V_OUT_SENSE, ADC_DUT_V_OUT_SENSE_CHANNEL);
			adc_init_one_slow(ADC_DUT3_I_SENSE, ADC_DUT3_I_SENSE_CHANNEL);
			adc_start_single_dma(ADC_DUT_V_OUT_SENSE);
			adc_start_single_dma(ADC_RAD_I_SENSE);
			HAL_Delay(6);
			data[i].dut_v_out_trip = adc_get_single_value_dma(ADC_DUT_V_OUT_SENSE);
			data[i].dut_i_sense_trip = adc_get_single_value_dma(ADC_DUT3_I_SENSE);

			adc_init_one_slow(ADC_VCC_SENSE, ADC_VCC_SENSE_CHANNEL);
			adc_start_dma(ADC_VCC_SENSE);
			HAL_Delay(6);
			data[0].dut_v_in = adc_get_single_value_dma(ADC_VCC_SENSE);

			if (experiment_tm_enabled)
				tm_send_exp_report(data[i]);

			// store result data in circular buffer
			store_results(data[i]);
		}
	}

	if (!fuse_tripped && (dut == DUT_1))
	{
		data[0].ts_start = ts_start;
		data[0].experiment = five_volts ? EXP_OCP_5_0 : EXP_OCP_3_3;
		data[0].dut_select = DUT_1;
		data[0].load_config = 0;
		data[0].rep_cnt = rep_cnt;
		data[0].dut_fault = true;
		data[0].t_trip = UINT16_MAX;
		data[0].dut_v_out_trip = 0;
		data[0].dut_i_sense_trip = 0;

		adc_init_one_slow(ADC_VCC_SENSE, ADC_VCC_SENSE_CHANNEL);
		adc_start_dma(ADC_VCC_SENSE);
		HAL_Delay(6);
		data[0].dut_v_in = adc_get_single_value_dma(ADC_VCC_SENSE);

		if (experiment_tm_enabled)
			tm_send_exp_report(data[0]);

		// store result data in circular buffer
		store_results(data[0]);
	}

	// disable load simulator
	HAL_GPIO_WritePin(LS1_SEL_GPIO_Port, LS1_SEL_Pin, 0);
	HAL_GPIO_WritePin(LS2_SEL_GPIO_Port, LS2_SEL_Pin, 0);
	HAL_GPIO_WritePin(LS3_SEL_GPIO_Port, LS3_SEL_Pin, 0);

	// disconnect (and disable) DUT
	if (dut == DUT_1)
	{
		HAL_GPIO_WritePin(DUT1_SEL_GPIO_Port, DUT1_SEL_Pin, 0);
		HAL_GPIO_WritePin(DUT1_EN_GPIO_Port, DUT1_EN_Pin, 0);
	}
	else if (dut == DUT_3)
	{
		HAL_GPIO_WritePin(DUT3_SEL_GPIO_Port, DUT3_SEL_Pin, 0);
	}

	// reset and disable VCC
	if (!five_volts)
	{
		new_vref = mp28167_calculate_vref(820.0, 100.0, 5000.0);
		mp28167_set_vref(exp_ssc, new_vref);
	}
	mp28167_set_enable(exp_ssc, false);
}

/**
 * @brief Conduct over-current protection experiment.
 *
 * @param  dut  DUT to conduct experiment on
 * @param  five_volts  True for 5V, false for 3.3V
 */
void conduct_ocp_experiment(dut_t dut, _Bool five_volts)
{
	conduct_ocp_experiment_rep(dut, five_volts, 0);
}

/**
 * @brief Conduct the given experiment.
 *
 * @param  dut  DUT to conduct experiment on
 * @param  exp  Experiment to conduct
 * @param  rep_dnt  Repetition counter for multiple execution
 */
void conduct_experiment_rep(dut_t dut, experiment_t exp, uint8_t rep_cnt)
{
	if (!(config_get_value(CONFIG_ID_EXP_ENABLE) & exp_lut[dut][exp]))
		return;

	experiment_running = true; // indicate running experiment

	switch (exp)
	{
		case EXP_UVP:
			conduct_uvp_experiment_rep(dut, rep_cnt);
			break;
		case EXP_OVP:
			conduct_ovp_experiment_rep(dut, rep_cnt);
			break;
		case EXP_OCP_3_3:
			conduct_ocp_experiment_rep(dut, false, rep_cnt);
			break;
		case EXP_OCP_5_0:
			conduct_ocp_experiment_rep(dut, true, rep_cnt);
			break;
	}

	experiment_running = false; // reset run indicator
}

/**
 * @brief Conduct the given experiment.
 *
 * @param  dut  DUT to conduct experiment on
 * @param  exp  Experiment to conduct
 */
void conduct_experiment(dut_t dut, experiment_t exp)
{
	conduct_experiment_rep(dut, exp, 0);
}

/**
 * @brief Execute a run (i.e. consecutive repetition) of the given experiment.
 *
 * @param  dut  DUT to execute experiment run on
 * @param  exp  Experiment to execute experiment run on
 */
void execute_run(dut_t dut, experiment_t exp)
{
	if (!(config_get_value(CONFIG_ID_EXP_ENABLE) & exp_lut[dut][exp]))
		return;

	for (int i = 0; i < config_get_value(CONFIG_ID_EXP_RUN_LENGTH); i++)
	{
		conduct_experiment_rep(dut, exp, i);
	}
}

/* ##### PUBLIC FUNCTIONS ################################################# */

/* ------------------------------------------------------------------------ */
void experiment_fixture_init(I2C_HandleTypeDef hi2c, mp28167* ssc)
{
	// initialize circular buffer for single experiment data
	uint16_t exp_buffer_size = exp_data_real_size * config_get_value(CONFIG_ID_NUMBER_OF_SINGLE_EXP);
	exp_buffer = malloc(exp_buffer_size);
	exp_circular_buffer = circular_buffer_init(exp_buffer, exp_buffer_size);

	// initialize circular buffer for experiment runs
	uint16_t exp_run_buffer_size = run_data_real_size() * config_get_value(CONFIG_ID_NUMBER_OF_EXP_RUNS);
	exp_run_buffer = malloc(exp_run_buffer_size);
	exp_run_circular_buffer = circular_buffer_init(exp_run_buffer, exp_run_buffer_size);

	// initialize circular buffer for experiment suites
	uint16_t exp_suite_buffer_size = suite_data_real_size() * config_get_value(CONFIG_ID_NUMBER_OF_EXP_SUITES);
	exp_suite_buffer = malloc(exp_suite_buffer_size);
	exp_suite_circular_buffer = circular_buffer_init(exp_suite_buffer, exp_suite_buffer_size);

	// initialize communication with sensors
	exp_hi2c = hi2c;

	// initialize reference to SSC
	exp_ssc = ssc;

	// initialize all other private variables
	experiment_running = false;
	is_part_of_run = false;
	is_part_of_suite = false;
}

/* ------------------------------------------------------------------------ */
void experiment_fixture_free()
{
	free(exp_suite_circular_buffer);
	free(exp_suite_buffer);
	free(exp_run_circular_buffer);
	free(exp_run_buffer);
	free(exp_circular_buffer);
	free(exp_buffer);
}

/* ------------------------------------------------------------------------ */
void experiment_fixture_start(void)
{
	is_part_of_run = true;
	uint32_t configured_exps = config_get_value(CONFIG_ID_EXP_ENABLE);

	if (configured_exps & DUT_1_UVP_01)
		execute_run(DUT_1, EXP_UVP);
	if (configured_exps & DUT_1_OVP_01)
		execute_run(DUT_1, EXP_OVP);
	if (configured_exps & DUT_1_OCP_01)
		execute_run(DUT_1, EXP_OCP_3_3);
	if (configured_exps & DUT_1_OCP_02)
		execute_run(DUT_1, EXP_OCP_5_0);
	if (configured_exps & DUT_2_UVP_01)
		execute_run(DUT_2, EXP_UVP);
	if (configured_exps & DUT_2_OVP_01)
		execute_run(DUT_2, EXP_OVP);
	if (configured_exps & DUT_2_OCP_01)
		execute_run(DUT_2, EXP_OCP_3_3);
	if (configured_exps & DUT_2_OCP_02)
		execute_run(DUT_2, EXP_OCP_5_0);
	if (configured_exps & DUT_3_OCP_01)
		execute_run(DUT_3, EXP_OCP_3_3);
	if (configured_exps & DUT_3_OCP_02)
		execute_run(DUT_3, EXP_OCP_5_0);

	is_part_of_run = false;
}

/* ------------------------------------------------------------------------ */
void experiment_fixture_one_shot(void)
{
	is_part_of_suite = true;
	uint32_t configured_exps = config_get_value(CONFIG_ID_EXP_ENABLE);

	if (configured_exps & DUT_1_UVP_01)
		conduct_experiment(DUT_1, EXP_UVP);
	if (configured_exps & DUT_1_OVP_01)
		conduct_experiment(DUT_1, EXP_OVP);
	if (configured_exps & DUT_1_OCP_01)
		conduct_experiment(DUT_1, EXP_OCP_3_3);
	if (configured_exps & DUT_1_OCP_02)
		conduct_experiment(DUT_1, EXP_OCP_5_0);
	if (configured_exps & DUT_2_UVP_01)
		conduct_experiment(DUT_2, EXP_UVP);
	if (configured_exps & DUT_2_OVP_01)
		conduct_experiment(DUT_2, EXP_OVP);
	if (configured_exps & DUT_2_OCP_01)
		conduct_experiment(DUT_2, EXP_OCP_3_3);
	if (configured_exps & DUT_2_OCP_02)
		conduct_experiment(DUT_2, EXP_OCP_5_0);
	if (configured_exps & DUT_3_OCP_01)
		conduct_experiment(DUT_3, EXP_OCP_3_3);
	if (configured_exps & DUT_3_OCP_02)
		conduct_experiment(DUT_3, EXP_OCP_5_0);

	is_part_of_suite = false;
}

/* ------------------------------------------------------------------------ */
_Bool experiment_fixture_get_fault_state(dut_t dut)
{
	// :TODO: implement
	return false;
}

/* ------------------------------------------------------------------------ */
_Bool experiment_fixture_is_running()
{
	return experiment_running;
}

/* ------------------------------------------------------------------------ */
void experiment_fixture_get_exp_data(exp_storage_t storage, storage_section_t section)
{
	exp_data_t data;
	size_t     index;

	uint16_t    exp_data_blocks;
	size_t      steps_back;
	cb_handle_t buffer;

	switch(storage)
	{
		case SINGLE:
			exp_data_blocks = 1;
			steps_back = section * exp_data_real_size;
			buffer     = exp_circular_buffer;
			break;
		case SUITE:
			exp_data_blocks = config_get_value(CONFIG_ID_NUMBER_OF_EXP_ENABLED);
			steps_back = section * suite_data_real_size();
			buffer     = exp_suite_circular_buffer;
			break;
		case RUN:
			exp_data_blocks = config_get_value(CONFIG_ID_NUMBER_OF_EXP_ENABLED) * config_get_value(CONFIG_ID_EXP_RUN_LENGTH);
			steps_back = section * run_data_real_size();
			buffer     = exp_run_circular_buffer;
			break;
	}

	if (steps_back <= circular_buffer_head(buffer))
	{
		index = circular_buffer_head(buffer) - steps_back;
	}
	else
	{
		// handle wrapping
		steps_back = steps_back - circular_buffer_head(buffer);
		index = circular_buffer_capacity(buffer) - steps_back;
	}

	for (int i = 0; i < exp_data_blocks; i++)
	{
		// get all data from circular buffer
		circular_buffer_peek_at(buffer, &data.ts_start, sizeof(data.ts_start), index);
		index += sizeof(data.ts_start);
		circular_buffer_peek_at(buffer, &data.experiment, sizeof(data.experiment), index);
		index += sizeof(data.experiment);
		circular_buffer_peek_at(buffer, &data.dut_select, sizeof(data.dut_select), index);
		index += sizeof(data.dut_select);
		circular_buffer_peek_at(buffer, &data.load_config, sizeof(data.load_config), index);
		index += sizeof(data.load_config);
		circular_buffer_peek_at(buffer, &data.rep_cnt, sizeof(data.rep_cnt), index);
		index += sizeof(data.rep_cnt);
		circular_buffer_peek_at(buffer, &data.dut_fault, sizeof(data.dut_fault), index);
		index += sizeof(data.dut_fault);
		circular_buffer_peek_at(buffer, &data.dut_v_in, sizeof(data.dut_v_in), index);
		index += sizeof(data.dut_v_in);
		circular_buffer_peek_at(buffer, &data.t_trip, sizeof(data.t_trip), index);
		index += sizeof(data.t_trip);
		circular_buffer_peek_at(buffer, &data.dut_v_out_trip, sizeof(data.dut_v_out_trip), index);
		index += sizeof(data.dut_v_out_trip);
		circular_buffer_peek_at(buffer, &data.dut_i_sense_trip, sizeof(data.dut_i_sense_trip), index);
		index += sizeof(data.dut_i_sense_trip);

		// send TM packet
		tm_send_exp_report(data);
	}
}

/* ------------------------------------------------------------------------ */
_Bool experiment_fixture_start_single_experiment(dut_t dut, experiment_t exp)
{
	_Bool rc = true;
	if (!(config_get_value(CONFIG_ID_EXP_ENABLE) & exp_lut[dut][exp]))
		rc = false;
	else
		conduct_experiment(dut, exp);
	return rc;
}

