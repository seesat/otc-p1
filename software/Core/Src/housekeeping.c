/**
 * @file housekeeping.c
 *
 * @brief Gather data for housekeeping
 *
 * @author Stefan Wertheimer
 * @date 2024-07-17
 */

#include "housekeeping.h"

/* ##### PUBLIC VARIABLES ################################################ */

static volatile _Bool housekeeping_tm_enabled = true;

/* ##### PRIVATE VARIABLES ################################################ */

static hk_data_t current_data;

/* ##### PUBLIC FUNCTIONS ################################################# */

/* ------------------------------------------------------------------------ */
void housekeeping_set_ts_boot(uint32_t ts)
{
	current_data.ts_boot = ts;
}

/* ------------------------------------------------------------------------ */
void housekeeping_prepare_report()
{
	if (!housekeeping_tm_enabled)
		return;

	am_data_t am_data;

	// get the last received timestamp
	current_data.ts_now = utc_ts_pps;

	current_data.v_in      = adc_get_value_dma(VCC_SENSE);
	current_data.temp_uc   = 0; // :TODO: implement
	current_data.v_ref_int = 0; // :TODO: implement
	current_data.v_ref_adc = adc_get_vref();

	am_data = ambient_monitoring_get_last_data();
	current_data.rad0_v_last  = am_data.rad0_v;
	current_data.rad0_i_last  = am_data.rad0_i;
	current_data.rad1_v_last  = am_data.rad1_v;
	current_data.rad1_i_last  = am_data.rad1_i;
	current_data.rad2_v_last  = am_data.rad2_v;
	current_data.rad2_i_last  = am_data.rad2_i;
	current_data.rad12_v_last = am_data.rad12_v;
	current_data.rad12_i_last = am_data.rad12_i;
	current_data.temp_1_last  = am_data.temp_1;
	current_data.temp_2_last  = am_data.temp_2;

	current_data.dut_1_fault = experiment_fixture_get_fault_state(DUT_1);
	current_data.dut_2_fault = experiment_fixture_get_fault_state(DUT_2);
	current_data.dut_3_fault = experiment_fixture_get_fault_state(DUT_3);
	current_data.experiment  = experiment_fixture_is_running();

	tm_send_hk_report(current_data);
}
