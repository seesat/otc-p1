/**
 * @file messages.c
 *
 * @brief Declaration of TC and TM messages
 *
 * @author Stefan Wertheimer
 * @date 2024-04-04
 */

#include "config.h"
#include "messages.h"

/* ------------------------------------------------------------------------ */
uint16_t run_data_real_size()
{
	return exp_data_real_size * config_get_value(CONFIG_ID_NUMBER_OF_EXP_ENABLED) * config_get_value(CONFIG_ID_EXP_RUN_LENGTH);
}

/* ------------------------------------------------------------------------ */
uint16_t suite_data_real_size()
{
	return exp_data_real_size * config_get_value(CONFIG_ID_NUMBER_OF_EXP_ENABLED);
}
