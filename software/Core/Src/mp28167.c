/**
 * @file mp28167.h
 *
 * @brief Control of MP28167-A Integrated Buck-Boost Converter with I2C interface
 *
 * @author Marian Keller
 * @date 2024-04-28
 */
 
 #include "mp28167.h"

void mp28167_set_enable(mp28167 *ref, bool state)
{
	HAL_GPIO_WritePin(ref->gpio_x, ref->gpio_pin, state);
}


enum error_code mp28167_set_vref(mp28167 *ref, uint16_t vref)
{
	I2C_HandleTypeDef hi2c = *ref->i2c_handle;
	enum error_code result = OK;
	uint16_t address = MP28167_ADDR << 1;
	uint8_t data[3];

	// calculate: vref_reg_val = vref / 0.8
	uint16_t vref_reg_val = (vref * 320) >> 8;

	uint8_t vref_ref_val_l = vref_reg_val & 0x07;
	uint8_t vref_ref_val_h = (vref_reg_val & 0x7F8) >> 3;


	// set vref registers
	data[0] = MP28167_REGISTER_VREF_L;
	data[1] = vref_ref_val_l;
	if (HAL_OK != HAL_I2C_Master_Transmit(&hi2c, address, data, 2, HAL_MAX_DELAY))
	{
		result = I2C_TX_ERROR;
	}

	data[0] = MP28167_REGISTER_VREF_H;
	data[1] = vref_ref_val_h;
	if (HAL_OK != HAL_I2C_Master_Transmit(&hi2c, address, data, 2, HAL_MAX_DELAY))
	{
		result = I2C_TX_ERROR;
	}

	// enable output reference change
	data[0] = MP28167_REGISTER_VREF_GO;
	data[1] = MP28167_VREF_GO_GO_BIT;
	if (HAL_OK != HAL_I2C_Master_Transmit(&hi2c, address, data, 2, HAL_MAX_DELAY))
	{
		result = I2C_TX_ERROR;
	}

	return result;
}

uint16_t mp28167_calculate_vref(float r1, float r2, float vout)
{
	float res = vout/((r1/r2)+1.0);
	return (uint16_t) res;
}
