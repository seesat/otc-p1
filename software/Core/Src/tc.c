/**
 * @file tc.h
 *
 * @brief Handling of Telecommands (TC)
 *
 * @author Stefan Wertheimer
 * @date 2024-03-27
 */

#include "tc.h"

#include "ambient_monitoring.h"
#include "config.h"
#include "experiment_fixture.h"
#include "housekeeping.h"

/* ##### PRIVATE VARIABLES ################################################ */

I2C_HandleTypeDef tc_hi2c;

mp28167* tc_ssc;

/* ##### PRIVATE FUNCTIONS ################################################ */

/**
 * @brief  Handle TC "Set Configuration Parameter"
 *
 * @param  args  Byte array containing the TC arguments
 * @param  len   Length of the byte array
 *
 * @return OK or TC_BAD_PARAMETER.
 */
error_code_t tc_handle_set_config_param(uint8_t* args, size_t len)
{
	if (len != sizeof(uint16_t) + sizeof(uint32_t))
		return TC_BAD_PARAMETER;

	uint16_t param_id = ((uint16_t)args[0] << 8) | args[1];
	uint32_t value = ((uint32_t)args[2] << 24) | ((uint32_t)args[3] << 16) | ((uint32_t)args[4] << 8) | args[5];

	config_set_value(param_id, value);
	return OK;
}

/**
 * @brief  Handle TC "Execute Single Test"
 *
 * @param  args  Byte array containing the TC arguments
 * @param  len   Length of the byte array
 *
 * @return OK or TC_BAD_PARAMETER.
 */
error_code_t tc_handle_execute_single_test(uint8_t* args, size_t len)
{
	if (len != sizeof(uint8_t) + sizeof(uint8_t))
		return TC_BAD_PARAMETER;

	uint8_t dut = args[0];
	uint8_t exp = args[1];

	if (experiment_fixture_start_single_experiment(dut, exp))
	{
		return OK;
	}
	else
	{
		return TC_FAILURE;
	}
}

/**
 * @brief  Handle TC "Re-init"
 */
void tc_handle_reinit()
{
	// skip main loop tasks while re-init in progress
	config_skip_main_loop = true;

	// stop ADCs
	adc_stop_dma(ADC_1);
	adc_stop_dma(ADC_2);

	// restart experiment control & ambient monitoring
	experiment_fixture_free();
	experiment_fixture_init(tc_hi2c, tc_ssc);
	ambient_monitoring_free();
	ambient_monitoring_init(tc_hi2c, tc_ssc);

	// calibrate ADCs
	adc_calibrate(ADC_1);
	adc_calibrate(ADC_2);

	// restart ADCs
	adc_start_dma(ADC_1);
	adc_start_dma(ADC_2);

	// resume main loop tasks
	config_skip_main_loop = false;
}

/* ##### PUBLIC FUNCTIONS ################################################# */

/* ------------------------------------------------------------------------ */
void tc_init(I2C_HandleTypeDef hi2c, mp28167* ssc)
{
	tc_hi2c = hi2c;
	tc_ssc  = ssc;
}

/* ------------------------------------------------------------------------ */
error_code_t tc_execute(uint16_t command_id, uint8_t* parameter, size_t param_len)
{
	error_code_t rc = OK;

	switch (command_id)
	{
		case TC_ID_START_EXP_RUN:
			experiment_fixture_start();
			break;
		case TC_ID_ENABLE_HK_REPORT:
			housekeeping_tm_enabled = true;
			break;
		case TC_ID_DISABLE_HK_REPORT:
			housekeeping_tm_enabled = false;
			break;
		case TC_ID_ENABLE_AM_REPORT:
			ambient_monitoring_tm_enabled = true;
			break;
		case TC_ID_DISABLE_AM_REPORT:
			ambient_monitoring_tm_enabled = false;
			break;
		case TC_ID_ENABLE_EXP_REPORT:
			experiment_tm_enabled = true;
			break;
		case TC_ID_DISABLE_EXP_REPORT:
			experiment_tm_enabled = false;
		case TC_ID_DUMP_AM_DATA_LAST:
			ambient_monitoring_get_all_data(ORBIT_LAST);
			break;
		case TC_ID_DUMP_AM_DATA_PREV:
			ambient_monitoring_get_all_data(ORBIT_PREV);
			break;
		case TC_ID_DUMP_AM_DATA_PENU:
			ambient_monitoring_get_all_data(ORBIT_PENU);
			break;
		case TC_ID_DUMP_EXP_DATA_LAST:
			experiment_fixture_get_exp_data(SINGLE, LAST);
			break;
		case TC_ID_DUMP_EXP_DATA_PREV:
			experiment_fixture_get_exp_data(SINGLE, PREV);
			break;
		case TC_ID_DUMP_EXP_DATA_PENU:
			experiment_fixture_get_exp_data(SINGLE, PENU);
			break;
		case TC_ID_DUMP_SUITE_DATA_LAST:
			experiment_fixture_get_exp_data(SUITE, LAST);
			break;
		case TC_ID_DUMP_SUITE_DATA_PREV:
			experiment_fixture_get_exp_data(SUITE, PREV);
			break;
		case TC_ID_DUMP_SUITE_DATA_PENU:
			experiment_fixture_get_exp_data(SUITE, PENU);
			break;
		case TC_ID_DUMP_RUN_DATA_LAST:
			experiment_fixture_get_exp_data(RUN, LAST);
			break;
		case TC_ID_DUMP_RUN_DATA_PREV:
			experiment_fixture_get_exp_data(RUN, PREV);
			break;
		case TC_ID_DUMP_RUN_DATA_PENU:
			experiment_fixture_get_exp_data(RUN, PENU);
			break;
		case TC_ID_DUMP_DETAILS:
			rc = TC_NOT_IMPLEMENTED; // detailed experiment data is not stored in current implementation
			break;
		case TC_ID_EXE_SINGLE_TEST:
			rc = tc_handle_execute_single_test(parameter, param_len);
			break;
		case TC_ID_SET_CONFIG_PARAM:
			rc = tc_handle_set_config_param(parameter, param_len);
			break;
		case TC_ID_GET_ALL_CONFIG:
			config_create_parameter_report();
			break;
		case TC_ID_REINIT:
			tc_handle_reinit();
			break;
		default:
			rc = TC_BAD_COMMAND; // unknown TC
			break;
	}

	return rc;
}
