/*
 * @file timer.c
 *
 * @brief Additional timer functions.
 *
 * @author Brendan Berg
 * @date 2024-08-04
 */

#include "timer.h"


/* ##### PUBLIC VARIABLES ################################################# */

/* ##### PRIVATE VARIABLES ################################################ */

TIM_HandleTypeDef* timer_us;

uint64_t timer_us_cnt_ovf;

static volatile uint64_t timer_us_last_val;

/* ##### PRIVATE FUNCTIONS ################################################ */



/* ##### PUBLIC FUNCTIONS ################################################# */

/* ------------------------------------------------------------------------ */
void timer_init(TIM_HandleTypeDef* htim)
{
	timer_us = htim;
	timer_us_cnt_ovf = 0;
	timer_us_last_val = 0;
}

uint64_t get_micros()
{
	return (timer_us_cnt_ovf << 32) + (uint64_t)(__HAL_TIM_GET_COUNTER(timer_us));
}

void save_micros()
{
	timer_us_last_val = get_micros();
}

uint64_t get_last_micros()
{
	return timer_us_last_val;
}

void timer_us_overflow()
{
	timer_us_cnt_ovf += 1;
}
