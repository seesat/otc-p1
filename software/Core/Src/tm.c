/**
 * @file tm.c
 *
 * @brief Handling of Telemetry (TM)
 *
 * @author Stefan Wertheimer
 * @date 2024-03-27
 */

#include "tm.h"

/* ------------------------------------------------------------------------ */
enum error_code tm_send_hk_report(hk_data_t data)
{
	seesat_mobefuse_HousekeepingReport_1_0 msg;
	msg.ts_now       = data.ts_now;
	msg.ts_start     = data.ts_boot;
	msg.v_in         = data.v_in;
	msg.temp_uc      = data.temp_uc;
	msg.v_ref_int    = data.v_ref_int;
	msg.v_ref_adc    = data.v_ref_adc;
	msg.rad0_v_last  = data.rad0_v_last;
	msg.rad0_i_last  = data.rad0_i_last;
	msg.rad1_v_last  = data.rad1_v_last;
	msg.rad1_i_last  = data.rad1_i_last;
	msg.rad2_v_last  = data.rad2_v_last;
	msg.rad2_i_last  = data.rad2_i_last;
	msg.rad12_v_last = data.rad12_v_last;
	msg.rad12_i_last = data.rad12_i_last;
	msg.temp_1_last  = data.temp_1_last;
	msg.temp_2_last  = data.temp_2_last;
	msg.dut_1_fault  = data.dut_1_fault;
	msg.dut_2_fault  = data.dut_2_fault;
	msg.dut_3_fault  = data.dut_3_fault;
	msg.experiment   = data.experiment;

	seesat_mobefuse_MOBeFuseTM_1_0 tm;
	tm.hk_report = msg;

	uint8_t buffer[seesat_mobefuse_MOBeFuseTM_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_] = {0};
	size_t  size = sizeof(buffer);

	int8_t ret_val = seesat_mobefuse_MOBeFuseTM_1_0_serialize_(&tm, &buffer[0], &size);
	if (0 != ret_val)
		return CAN_SERIALIZATION_ERROR;

	return can_send(&buffer[0], size, seesat_mobefuse_MOBeFuseTM_1_0_FIXED_PORT_ID_);
}

/* ------------------------------------------------------------------------ */
enum error_code tm_send_am_report(am_data_t data)
{
	seesat_mobefuse_AmbientMonitoringReport_1_0 msg;
	msg.ts      = data.ts;
	msg.temp_1  = data.temp_1;
	msg.temp_2  = data.temp_2;
	msg.rad0_v  = data.rad0_v;
	msg.rad0_i  = data.rad0_i;
	msg.rad1_v  = data.rad1_v;
	msg.rad1_i  = data.rad1_i;
	msg.rad2_v  = data.rad2_v;
	msg.rad2_i  = data.rad2_i;
	msg.rad12_v = data.rad12_v;
	msg.rad12_i = data.rad12_i;

	seesat_mobefuse_MOBeFuseTM_1_0 tm;
	tm.am_report = msg;

	uint8_t buffer[seesat_mobefuse_MOBeFuseTM_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_] = {0};
	size_t  size = sizeof(buffer);

	int8_t ret_val = seesat_mobefuse_MOBeFuseTM_1_0_serialize_(&tm, &buffer[0], &size);
	if (0 != ret_val)
		return CAN_SERIALIZATION_ERROR;

	return can_send(&buffer[0], size, seesat_mobefuse_MOBeFuseTM_1_0_FIXED_PORT_ID_);
}

/* ------------------------------------------------------------------------ */
enum error_code tm_send_exp_report(exp_data_t data)
{
	seesat_mobefuse_SingleExperimentReport_1_0 msg;
	msg.ts_start      = data.ts_start;
	msg.dut_select    = data.dut_select;
	msg.experiment    = data.experiment;
	msg.load_config   = data.load_config;
	msg.rep_cnt       = data.rep_cnt;
	msg.dut_v_in      = data.dut_v_in;
	msg.dut_fault     = data.dut_fault;
	msg.dut_v_out     = data.dut_v_out_trip;
	msg.dut_i_sense   = data.dut_i_sense_trip;
	msg.t_trip        = data.t_trip;

	seesat_mobefuse_MOBeFuseTM_1_0 tm;
	tm.exp_report = msg;

	uint8_t buffer[seesat_mobefuse_MOBeFuseTM_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_] = {0};
	size_t  size = sizeof(buffer);

	int8_t ret_val = seesat_mobefuse_MOBeFuseTM_1_0_serialize_(&tm, &buffer[0], &size);
	if (0 != ret_val)
		return CAN_SERIALIZATION_ERROR;

	return can_send(&buffer[0], size, seesat_mobefuse_MOBeFuseTM_1_0_FIXED_PORT_ID_);
}

/* ------------------------------------------------------------------------ */
enum error_code tm_send_config_report(config_data_t data)
{
	seesat_mobefuse_ConfigurationParameterReport_1_0 msg;
	msg.ts = data.ts;
	msg.exp_interval = data.exp_interval;
	msg.am_interval = data.am_interval;
	msg.hk_interval = data.hk_interval;
	msg.orbit_duration = data.orbit_duration;
	msg.number_of_orbits_for_am = data.number_of_orbits_for_am;
	msg.number_of_exp_runs = data.number_of_exp_runs;
	msg.number_of_single_exp = data.number_of_single_exp;
	msg.number_of_exp_suites = data.number_of_exp_suites;
	msg.exp_run_length = data.exp_run_length;
	msg.exp_enable = data.exp_enable;
	msg.number_of_exp_enabled = data.number_of_exp_enabled;

	seesat_mobefuse_MOBeFuseTM_1_0 tm;
	tm.config_report = msg;

	uint8_t buffer[seesat_mobefuse_MOBeFuseTM_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_] = {0};
	size_t  size = sizeof(buffer);

	int8_t ret_val = seesat_mobefuse_MOBeFuseTM_1_0_serialize_(&tm, &buffer[0], &size);
	if (0 != ret_val)
		return CAN_SERIALIZATION_ERROR;

	return can_send(&buffer[0], size, seesat_mobefuse_MOBeFuseTM_1_0_FIXED_PORT_ID_);
}
