/**
 * @file tmp75bq1_config.h
 *
 * @brief Communication with the TMP75B-Q1 temperature sensor.
 *
 * @author Stefan Wertheimer
 * @date 2024-01-21
 */

#include "tmp75bq1_config.h"

/**
 * @brief Read configuration register of TMP75B-Q1 temperature sensor.
 *
 * @param   address    I2C address of sensor
 * @return Content of configuration register as 16 bit integer (MSB first).
 */
enum error_code tmp75bq1_read_config(I2C_HandleTypeDef hi2c, const uint8_t address, uint16_t* config)
{
	enum error_code result = OK;
	uint8_t data[3] = { 0 };
	data[0] = TMP75BQ1_REGISTER_CONFIG;
	if (HAL_OK == HAL_I2C_Master_Transmit(&hi2c, address, data, 1, HAL_MAX_DELAY))
	{
		if (HAL_OK == HAL_I2C_Master_Receive(&hi2c, address, data, 2, HAL_MAX_DELAY))
		{
			*config = ((uint16_t)data[0] << 8) | data[1];
		}
		else
		{
			result = I2C_RX_ERROR;
		}
	}
	else
	{
		result = I2C_TX_ERROR;
	}
	return result;
}

/* ------------------------------------------------------------------------ */
enum error_code tmp75bq1_read_temperature_raw(I2C_HandleTypeDef hi2c, const uint8_t address, uint16_t* temperature)
{
	enum error_code result = OK;

	uint8_t data[3] = { 0 };
	data[0] = TMP75BQ1_REGISTER_TEMP;

	if (HAL_OK == HAL_I2C_Master_Transmit(&hi2c, address, data, 1, HAL_MAX_DELAY))
	{
		if (HAL_OK == HAL_I2C_Master_Receive(&hi2c, address, data, 2, HAL_MAX_DELAY))
		{
			*temperature = ((uint16_t)data[0] << 4) | (data[1] >> 4);
		}
		else
		{
			result = I2C_TX_ERROR;
		}
	}
	else
	{
		result = I2C_RX_ERROR;
	}
	return result;
}

/* ------------------------------------------------------------------------ */
enum error_code tmp75bq1_read_temperature_float(I2C_HandleTypeDef hi2c, const uint8_t address, float* temperature)
{
	uint16_t value;
	enum error_code result = tmp75bq1_read_temperature_raw(hi2c, address, &value);
	if (OK == result)
	{
		// handle negative values
		if (value > 0x7FF)
		{
			value |= 0xF000;
		}
		*temperature = value * 0.0625;
	}
	return result;
}

/* ------------------------------------------------------------------------ */
enum error_code tmp75bq1_set_low_limit(I2C_HandleTypeDef hi2c, const uint8_t address, const uint16_t limit)
{
	enum error_code result = OK;
	uint8_t data[] = {  TMP75BQ1_REGISTER_T_LOW, (uint8_t)(limit >> 8), (uint8_t)(limit & 0xff) };
	if (HAL_OK != HAL_I2C_Master_Transmit(&hi2c, address, data, 3, HAL_MAX_DELAY))
	{
		result = I2C_TX_ERROR;
	}
	return result;
}

/* ------------------------------------------------------------------------ */
enum error_code tmp75bq1_set_high_limit(I2C_HandleTypeDef hi2c, const uint8_t address, const uint16_t limit)
{
	enum error_code result = OK;
	uint8_t data[] = { TMP75BQ1_REGISTER_T_HIGH, (uint8_t)(limit >> 8), (uint8_t)(limit & 0xff) };
	if (HAL_OK != HAL_I2C_Master_Transmit(&hi2c, address, data, 3, HAL_MAX_DELAY))
	{
		result = I2C_TX_ERROR;
	}
	return result;
}

/* ------------------------------------------------------------------------ */
enum error_code tmp75bq1_set_one_shot_mode(I2C_HandleTypeDef hi2c, const uint8_t address)
{
	uint16_t config = 0;
	enum error_code result = tmp75bq1_read_config(hi2c, address, &config);
	config = config & TMP75BQ1_CONFIG_OS_RESET;
	uint8_t data[] = { address, TMP75BQ1_REGISTER_CONFIG, ((config >> 8) | TMP75BQ1_CONFIG_OS_ON), 0 };
	if (HAL_OK != HAL_I2C_Master_Transmit(&hi2c, address, data, 3, HAL_MAX_DELAY))
	{
		result = I2C_TX_ERROR;
	}
	return result;
}

/* ------------------------------------------------------------------------ */
enum error_code tmp75bq1_set_conversion_rate(I2C_HandleTypeDef hi2c, const uint8_t address, enum tmp75bq1_cr cr)
{
	uint16_t config = 0;
	enum error_code result = tmp75bq1_read_config(hi2c, address, &config);
	config = config & TMP75BQ1_CONFIG_CR_RESET;
	uint8_t data[] = { address, TMP75BQ1_REGISTER_CONFIG, ((config >> 8) | cr), 0 };
	if (HAL_OK != HAL_I2C_Master_Transmit(&hi2c, address, data, 3, HAL_MAX_DELAY))
	{
		result = I2C_TX_ERROR;
	}
	return result;
}

/* ------------------------------------------------------------------------ */
enum error_code tmp75bq1_set_fault_queue(I2C_HandleTypeDef hi2c, const uint8_t address, enum tmp75bq1_fq fq)
{
	uint16_t config = 0;
	enum error_code result = tmp75bq1_read_config(hi2c, address, &config);
	config = config & TMP75BQ1_CONFIG_FQ_RESET;
	uint8_t data[] = { address, TMP75BQ1_REGISTER_CONFIG, ((config >> 8) | fq), 0 };
	if (HAL_OK != HAL_I2C_Master_Transmit(&hi2c, address, data, 3, HAL_MAX_DELAY))
	{
		result = I2C_TX_ERROR;
	}
	return result;
}

/* ------------------------------------------------------------------------ */
enum error_code tmp75bq1_set_alert_polarity(I2C_HandleTypeDef hi2c, const uint8_t address, enum tmp75bq1_pol pol)
{
	uint16_t config = 0;
	enum error_code result = tmp75bq1_read_config(hi2c, address, &config);
	config = config & TMP75BQ1_CONFIG_POL_RESET;
	uint8_t data[] = { address, TMP75BQ1_REGISTER_CONFIG, ((config >> 8) | pol), 0 };
	if (HAL_OK != HAL_I2C_Master_Transmit(&hi2c, address, data, 3, HAL_MAX_DELAY))
	{
		result = I2C_TX_ERROR;
	}
	return result;
}

/* ------------------------------------------------------------------------ */
enum error_code tmp75bq1_set_alert_mode(I2C_HandleTypeDef hi2c, const uint8_t address, enum tmp75bq1_tm tm)
{
	uint16_t config = 0;
	enum error_code result = tmp75bq1_read_config(hi2c, address, &config);
	config = config & TMP75BQ1_CONFIG_TM_RESET;
	uint8_t data[] = { address, TMP75BQ1_REGISTER_CONFIG,  ((config >> 8) | tm), 0 };
	if (HAL_OK != HAL_I2C_Master_Transmit(&hi2c, address, data, 3, HAL_MAX_DELAY))
	{
		result = I2C_TX_ERROR;
	}
	return result;
}

/* ------------------------------------------------------------------------ */
enum error_code tmp75bq1_set_shutdown_mode(I2C_HandleTypeDef hi2c, const uint8_t address, enum tmp75bq1_sd sd)
{
	uint16_t config = 0;
	enum error_code result = tmp75bq1_read_config(hi2c, address, &config);
	config = config & TMP75BQ1_CONFIG_SD_RESET;
	uint8_t data[] = { TMP75BQ1_REGISTER_CONFIG, ((config >> 8) | sd), 0 };
	if (HAL_OK != HAL_I2C_Master_Transmit(&hi2c, address, data, 3, HAL_MAX_DELAY))
	{
		result = I2C_TX_ERROR;
	}
	return result;
}
