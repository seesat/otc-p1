# MOBeFuse Software Interface Control Document

## General

MOBeFuse uses a CAN-FD header ID of 0x321 for all its messages on TX side (TM).

Additionally, it uses two CAN-FD filter IDs 0x321 and 0x7FF on RX side (TC & Heartbeat).

## Telecommands

The commandability of MOBeFuse is implemented through OpenCyphal's standard Node command `ExecuteCommand`. MOBeFuse uses the 1.3 DSDL version.

All telecommand identifier are 16 bits long and have a fixed prefix of 0x4D.

| Command                      | Identifier | Description |
| ---------------------------- | ---------- | ----------- |
| `TC_ID_START_EXP_RUN`        | 0x4D01     | Start experiment run |
| `TC_ID_ENABLE_HK_REPORT`     | 0x4D02     | Enable cyclic report of housekeeping data |
| `TC_ID_DISABLE_HK_REPORT`    | 0x4D03     | Disable cyclic report of housekeeping data |
| `TC_ID_ENABLE_AM_REPORT`     | 0x4D04     | Enable cyclic report of ambient monitoring data |
| `TC_ID_DISABLE_AM_REPORT`    | 0x4D05     | Disable cyclic report of ambient monitoring data |
| `TC_ID_DUMP_AM_DATA_LAST`    | 0x4D15     | Dump ambient monitoring data from last orbit |
| `TC_ID_DUMP_AM_DATA_PREV`    | 0x4D07     | Dump ambient monitoring data from previous orbit |
| `TC_ID_DUMP_AM_DATA_PENU`    | 0x4D08     | Dump ambient monitoring data from penultimate orbit |
| `TC_ID_DUMP_EXP_DATA_LAST`   | 0x4D09     | Dump experiment data from last run |
| `TC_ID_DUMP_EXP_DATA_PREV`   | 0x4D0A     | Dump experiment data from previous run |
| `TC_ID_DUMP_EXP_DATA_PENU`   | 0x4D0B     | Dump experiment data from penultimate run |
| `TC_ID_DUMP_DETAILS`         | 0x4D0C     | Dump detailed experiment data from last run |
| `TC_ID_EXE_SINGLE_TEST`      | 0x4D0D     | Execute a single test case (1) |
| `TC_ID_SET_CONFIG_PARAM`     | 0x4D0E     | Set value of single configuration parameter (2) |
| `TC_ID_GET_ALL_CONFIG`       | 0x4D0F     | Get values of all configuration parameters |
| `TC_ID_DUMP_SUITE_DATA_LAST` | 0x4D10     | Dump experiment data from last suite |
| `TC_ID_DUMP_SUITE_DATA_PREV` | 0x4D11     | Dump experiment data from previous suite |
| `TC_ID_DUMP_SUITE_DATA_PENU` | 0x4D12     | Dump experiment data from penultimate suite |
| `TC_ID_DUMP_RUN_DATA_LAST`   | 0x4D13     | Dump experiment data from last run |
| `TC_ID_DUMP_RUN_DATA_PREV`   | 0x4D14     | Dump experiment data from previous run |
| `TC_ID_DUMP_RUN_DATA_PENU`   | 0x4D15     | Dump experiment data from penultimate run |
| `TC_ID_ENABLE_EXP_REPORT`    | 0x4D16     | Enable report of experiment data on occurrence |
| `TC_ID_DISABLE_EXP_REPORT`   | 0x4D17     | Disable report of experiment data on occurrence |
| `TC_ID_REINIT`               | 0x4DFF     | Re-initialize software |
 
(1) `TC_ID_EXE_SINGLE_TEST` requires two parameters:
 
- DUT Indentifier (`unit8_t`)
- Experiment Indentifier (`unit8_t`)

(2) `TC_ID_SET_CONFIG_PARAM` requires two parameters:
 
- Configuration Parameter Identifier (`uint16_t`) 
- New value of Configuration Parameter (`uint32_t`)

MOBeFuse implements the ExecuteCommand response mechanism as well and uses the following status codes:

- `STATUS_SUCCESS`
- `STATUS_FAILURE`
- `STATUS_BAD_COMMAND`
- `STATUS_BAD_PARAMETER`
- `STATUS_INTERNAL_ERROR`

In case of `STATUS_INTERNAL_ERROR` the data field of the response message contains an internal MOBeFuse software error code as well.

## Telemetry

### Housekeeping Report

MOBeFuse triggers the generation of this report periodically, if it is enabled through the corresponding `TC_ID_ENABLE_HK_REPORT`. Configuration Parameter `CONFIG_ID_HK_INTERVAL` defines the corresponding generation interval.

| Parameter      | Size (bit) | Description |
| -------------- | ---------- | ----------- |
| `ts_now`       | 32         | Current timestamp in UTC |
| `ts_start`     | 32         | Timestamp of system start in UTC; set after reception of first time report broadcast and otherwise 0 |
| `v_in`         | 16         | Supply voltage of MOBeFuse |
| `temp_uc`      | 16         | MCU temperature |
| `v_ref_int`    | 16         | MCU reference voltage |
| `v_ref_adc`    | 32         | ADC reference voltage |
| `rad0_v_last`  | 16         | Last idle voltage read from dosimeter |
| `rad0_i_last`  | 16         | Last idle current read from dosimeter |
| `rad1_v_last`  | 16         | Last voltage of RADFET 1 read from dosimeter |
| `rad1_i_last`  | 16         | Last current of RADFET 1 read from dosimeter |
| `rad2_v_last`  | 16         | Last voltage of RADFET 2 read from dosimeter |
| `rad2_i_last`  | 16         | Last current of RADFET 2 read from dosimeter |
| `rad12_v_last` | 16         | Last combined voltage of RADFETs 1 + 2 read from dosimeter |
| `rad12_i_last` | 16         | Last combined current of RADFETs 1 + 2 read from dosimeter |
| `temp_1_last`  | 16         | Last value read from temperature sensor 1 |
| `temp_2_last`  | 16         | Last value read from temperature sensor 2 |
| `dut_1_fault`  | 1          | Flag indicating whether or not DUT 1 (TPS25947) is in fault mode |
| `dut_2_fault`  | 1          | Flag indicating whether or not DUT 2 (NIS5150) is in fault mode |
| `dut_3_fault`  | 1          | Flag indicating whether or not DUT 3 (SOURCE LCL) is in fault mode |
| `experiment`   | 1          | Flag indicating whether or not an experiment is running |

Housekeeping Report messages have a fixed Port ID of 111.

### Ambient Monitoring Report

MOBeFuse triggers the generation of this report periodically, if it is enabled through the corresponding `TC_ID_ENABLE_AM_REPORT`. Configuration Parameter `CONFIG_ID_AM_INTERVAL` defines the corresponding generation interval.

Alternatively, the software generates this report multiple times, if the ambient monitoring data shall be downlinked for a complete orbit through one of the corresponding TCs.

| Parameter | Size (bit) | Description |
| --------- | ---------- | ----------- |
| `ts`      | 32         | Timestamp of measurement in UTC |
| `rad0_v`  | 16         | Current idle voltage read from dosimeter |
| `rad0_i`  | 16         | Current idle current read from dosimeter |
| `rad1_v`  | 16         | Current voltage of RADFET 1 read from dosimeter |
| `rad1_i`  | 16         | Current current of RADFET 1 read from dosimeter |
| `rad2_v`  | 16         | Current voltage of RADFET 2 read from dosimeter |
| `rad2_i`  | 16         | Current current of RADFET 2 read from dosimeter |
| `rad12_v` | 16         | Current combined voltage of RADFETs 1 + 2 read from dosimeter |
| `rad12_i` | 16         | Current combined current of RADFETs 1 + 2 read from dosimeter |
| `temp_1`  | 16         | Value read from temperature sensor 1 |
| `temp_2`  | 16         | Value read from temperature sensor 2 |

Ambient Monitoring Report messages have a fixed Port ID of 110.

### Single Experiment Report

MOBeFuse triggers the generation of this report every time it completes an experiment. 

Alternatively, the software generates this report multiple times, if the result data shall be downlinked for a complete experiment run through one of the corresponding TCs.

| Parameter      | Size (bit) | Description |
| -------------- | ---------- | ----------- |
| `ts_start`     | 32         | Timestamp at experiment start in UTC |
| `dut_select`   | 2          | Selected DUT; can be "_TPS25947_", "_NIS5150_" or "_SOURCE LCL_" |
| `experiment`   | 2          | Selected experiment; can be "_OVP_", "_UVP_", "_OCP 3.3V_" or "_OCP 5V_" |
| `load_config`  | 3          | Configuration of load simulator; bits indicate active resistors at tripping of DUT |
| `rep_cnt`      | 4          | Repetition counter; each experiment will be repeated 10 times |
| `dut_v_in`     | 16         | Input voltage to DUT at tripping |
| `dut_fault`    | 1          | Fault state of selected DUT |
| `dut_v_out`    | 16         | Output voltage out of DUT after tripping |
| `dut_i_sense`  | 16         | Current measured by selected DUT after tripping |
| `t_trip`       | 10         | Tripping time of DUT in ms indicated by fault signal of DUT (fixed-point with one fractional digit) |

Experiment Report messages have a fixed Port ID of 112.

### Configuration Report

MOBeFuse triggers the generation of this report as response to `TC_ID_GET_ALL_CONFIG`.

| Parameter                 | Size (bit) | Description |
| ------------------------- | ---------- | ----------- |
| `ts`                      | 32         | Timestamp of report generation |
| `exp_interval`            | 32         | Exeution interval for experiment suites |
| `am_interval`             | 32         | Sampling interval for ambient monitoring data |
| `hk_interval`             | 32         | Sampling interval for housekeeping data |
| `orbit_duration`          | 32         | Orbit duration in seconds |
| `number_of_orbits_for_am` | 32         | Number of ambient measurements per orbit |
| `number_of_exp_runs`      | 32         | Number of storable complete experiment runs |
| `number_of_single_exp`    | 32         | Number of storable single experiments |
| `number_of_exp_suites`    | 32         | Number of storable experiment suites |
| `exp_run_length`          | 32         | Length of experiment runs |
| `exp_enable`              | 32         | Bit mask of enabled experiments |
| `number_of_exp_enabled`   | 32         | Number of enabled experiments |

Configuration Report messages have a fixed Port ID of 113.

## Configuration Parameters

The behavior of MOBeFuse can be configured through the following parameters. Each parameter has the on-board datatype `uint32_t` and can be accessed through an unique configuration parameter identifier. The mentioned default values will be restored by each power cycle.

All parameter identifier are 16 bits long and have a fixed prefix of 0x53.

| Parameter                           | Identifier | Data type(1) | Default | Description |
| ----------------------------------- | ---------- | ------------ | ------- | ----------- |
| `CONFIG_ID_EXP_INTERVAL`            | 0x5301     | `integer32`  | 4       | Interval for experiment suite execution in seconds |
| `CONFIG_ID_AM_INTERVAL`             | 0x5302     | `integer32`  | 16      | Interval for ambient monitoring/sensor readout in seconds |
| `CONFIG_ID_HK_INTERVAL`             | 0x5303     | `integer32`  | 60      | Interval for housekeeping report generation in seconds |
| `CONFIG_ID_ORBIT_DURATION`          | 0x5304     | `integer32`  | 5400    | Orbit duration in seconds |
| `CONFIG_ID_NUMBER_OF_ORBITS_FOR_AM` | 0x5305     | `integer32`  | 3       | Number of orbits to store ambient monitoring data for |
| `CONFIG_ID_NUMBER_OF_EXP_RUNS`      | 0x5306     | `integer32`  | 3       | Number of experiment runs to store |
| `CONFIG_ID_NUMBER_OF_SINGLE_EXP`    | 0x5307     | `integer32`  | 3       | Number of single experiments to store |
| `CONFIG_ID_NUMBER_OF_EXP_SUITES`    | 0x5308     | `integer32`  | 3       | Number of experiment suites |
| `CONFIG_ID_EXP_RUN_LENGTH`          | 0x5309     | `integer32`  | 10      | Length of an experiment run (number of repetitions) |
| `CONFIG_ID_EXP_ENABLE`              | 0x530A     | `integer32`  | 0x0703  | Bit field for enabling/disabling experiments in experiment suite  (only experiments set to '1' will run); see (2) for details |
| `CONFIG_ID_NUMBER_OF_EXP_ENABLED`   | 0x530B     | `integer32`  | 19 (3)  | Number of enabled experiments |

(1) OpenCyphal/UAVCAN data type  
(2) Experiment positions in bit field:

| Position | Bit layout       | Hexadecimal  | Experiment                      |
| -------- | -----------------| ------------ | ------------------------------- |
| 11 (MSB) | 0b1000 0000 0000 | 0x0800       | DUT-1 UVP-01 (TPS25947, UVP)    |
| 10       | 0b0110 0000 0000 | 0x0400       | DUT-1 OVP-01 (TPS25947, OVP)    |
| 9        | 0b0010 0000 0000 | 0x0200       | DUT-1 OCP-01 (TPS25947, 3.3V)   |
| 8        | 0b0001 0000 0000 | 0x0100       | DUT-1 OCP-02 (TPS25947, 5.0V)   |
| 7        | 0b0000 1000 0000 | 0x0080       | DUT-2 UVP-01 (NIS6150, UVP)     |
| 6        | 0b0000 0100 0000 | 0x0040       | DUT-2 OVP-01 (NIS6150, OVP)     |
| 5        | 0b0000 0010 0000 | 0x0020       | DUT-2 OCP-01 (NIS6150, 3.3V)    |
| 4        | 0b0000 0001 0000 | 0x0010       | DUT-2 OCP-02 (NIS6150, 5.0V)    |
| 3        | 0b0000 0000 1000 | 0x0008       | DUT-3 UVP-01 (SOURCE LCL UVP    |
| 2        | 0b0000 0000 0100 | 0x0004       | DUT-3 OVP-01 (SOURCE LCL OVP)   |
| 1        | 0b0000 0000 0010 | 0x0002       | DUT-3 OCP-01 (SOURCE LCL, 3.3V) |
| 0 (LSB)  | 0b0000 0000 0001 | 0x0001       | DUT-3 OCP-02 (SOURCE LCL, 5.0V) |

(3) Currently implemented experiments: DUT-1 OVP-01 (1 experiment report), DUT-1 OCP-01 (1 experiment report), DUT-1 OCP-02 (1 experiment report), DUT-3 OCP-01 (8 experiment reports) and DUT-3 OCP-02 (8 experiment reports)

## DUT and Experiment Identifiers

### DUT Identifiers

| Identifier Name | Value | Description             |
| --------------- | ----- | ----------------------- |
| `DUT_1`         | 0x01  | DUT-1 (TI TPS25947)     |
| `DUT_2`         | 0x02  | DUT-2 (ONS NIS6150)     |
| `DUT_3`         | 0x03  | DUT-3 (KSat SOURCE LCL) |

### Experiment Identifiers

| Identifier Name | Value | Description                                |
| --------------- | ----- | ------------------------------------------ |
| `EXP_OVP`       | 0x01  | Over-Voltage Protection Experiment         |
| `EXP_UVP`       | 0x02  | Under-Voltage Protection Experiment        |
| `EXP_OCP_3_3`   | 0x03  | Over-Current Protection at 3.3V Experiment |
| `EXP_OCP_5_0`   | 0x04  | Over-Current Protection at 5.0V Experiment |

