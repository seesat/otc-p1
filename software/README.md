# AHAB – The experiment control firmware for MOBeFuse

## General

AHAB ist written in **C** and based on the Hardware Abstraction Layer (HAL) provided by STMicroelectronics for the STM32G4 family used in the MOBeFuse project.

Addiionally, AHAB requires support of the [**OpenCyphal**](https://opencyphal.org) framework for external communication with OTC-P1 based on CAN and Cyphal's protocol abstraction.
