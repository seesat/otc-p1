#!/bin/sh

# generate public regulated UAVCAN base data types
nnvg --target-language c -O Core/Inc/cyphal/public_regulated_data_types  --enable-serialization-asserts --lookup-dir Config/cyphal/public_regulated_data_types/uavcan Config/cyphal/public_regulated_data_types/uavcan

# generate MOBeFuse custom data types (private unregulated)
nnvg --target-language c -O Core/Inc/cyphal/custom_data_types --enable-serialization-asserts --lookup-dir Config/cyphal/public_regulated_data_types/uavcan --allow-unregulated-fixed-port-id Config/cyphal/custom_data_types/seesat 
